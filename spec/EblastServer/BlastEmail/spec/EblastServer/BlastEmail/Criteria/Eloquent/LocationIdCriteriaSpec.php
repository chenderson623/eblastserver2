<?php

namespace spec\EblastServer\BlastEmail\Criteria\Eloquent;

use EblastServer\BlastEmail\Criteria\Eloquent\LocationIdCriteria;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LocationIdCriteriaSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(LocationIdCriteria::class);
    }
}
