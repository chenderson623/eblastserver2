<?php

namespace spec\EblastServer\BlastEmail;

use EblastServer\BlastEmail\BlastEmailType;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BlastEmailTypeSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(BlastEmailType::class);
    }
}
