var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
    //mix.phpUnit(['tests/UnitTests/Blasts/BlastsFactoryTest.php','EblastServer/**/*.php']);
    mix.phpUnit(['tests/**/*Test.php','EblastServer/**/*.php']);
});

//Note: for phpunit test + watch, run `gulp tdd`