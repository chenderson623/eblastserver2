<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmtpProviderTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('smtp_providers', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('name', 50);
            $table->boolean('active')->default(1);

            $table->timestamps();
		});

        Schema::create('smtp_provider_accounts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('smtp_provider_id', false, false)->index();
            $table->string('name', 50);
            $table->string('domain', 50);
            $table->string('username', 255);
            $table->string('password', 255);
            $table->string('account_id', 255);
            $table->string('api_key', 255);

            $table->boolean('active')->default(1);

            $table->timestamps();
        });

        Schema::create('smtp_provider_accounts_locations', function(Blueprint $table)
        {
            $table->integer('smtp_provider_account_id', false, false)->index();
            $table->integer('location_id', false, false)->index();

            $table->timestamps();
        });

        // Insert some defaults:

        $smtp_providers = [
            [
            'id' => 1,
            'name' => 'Sendgrid Smtp'
            ],
            [
            'id' => 2,
            'name' => 'Mailgun'
            ],
        ];
        DB::table('smtp_providers')->insert( $smtp_providers );

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('smtp_providers');
        Schema::drop('smtp_provider_accounts');
        Schema::drop('smtp_provider_accounts_locations');
	}
}