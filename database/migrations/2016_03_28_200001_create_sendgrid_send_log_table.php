<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSendgridSendLogTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('sendgrid_send_log', function (Blueprint $table) {
            $table->increments('id');

            $table->string('blast_type');
            $table->integer('blast_id')->index();
            $table->date('blast_date');
            $table->integer('location_id')->index();
            $table->string('smtp_username');
            $table->mediumInteger('send_count');
            $table->smallInteger('batch');

            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('sendgrid_send_log');

    }
}