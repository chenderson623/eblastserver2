<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribersTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subscriber_histories', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('subscriber_session_id');
            $table->integer('subscriber_id')->index();
            $table->integer('location_id')->index();
            $table->string('event', 25);
            $table->tinyInteger('event_code', false,false);
            $table->datetime('event_datetime');
            $table->text('description');
            $table->string('remote_code', 10);
            $table->text('data_json');
            $table->text('undo_json');
            $table->boolean('undone');
            $table->integer('parent_history_id', false, false);

            $table->timestamps();
		});

        Schema::create('subscriber_profiles', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('email', 255)->index();
            $table->string('domain', 50);
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('telephone', 15);
            $table->boolean('active')->default(1);
            $table->date('date_added');
            $table->date('date_removed');
            $table->string('hash', 32);
            $table->boolean('remote_checked');
            $table->boolean('remote_valid');

            $table->timestamps();
        });

        Schema::create('subscriber_sessions', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('session_type', 25);
            $table->integer('subscriber_id', false, false)->index();
            $table->string('user', 35);
            $table->string('ip_address', 15);
            $table->string('user_agent', 100);
            $table->string('script', 100);
            $table->string('web_session', 50);
            $table->string('referer', 255);

            $table->timestamps();
        });

        Schema::create('subscriber_subscriptions', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('subscriber_id', false, false)->index();
            $table->integer('location_id', false, false)->index();
            $table->string('hash', 32);
            $table->boolean('active')->default(1);
            $table->boolean('ad_blast');
            $table->boolean('eblast');
            $table->boolean('newsletters');
            $table->boolean('recipes');
            $table->boolean('store_blasts');
            $table->boolean('store_management');
            $table->date('date_added');
            $table->date('date_removed');

            $table->timestamps();
        });


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subscriber_histories');
        Schema::drop('subscriber_profiles');
        Schema::drop('subscriber_sessions');
        Schema::drop('subscriber_subscriptions');
	}
}