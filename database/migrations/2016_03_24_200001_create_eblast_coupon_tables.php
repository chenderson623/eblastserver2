<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEblastCouponTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('eblast_coupons', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('title');
            $table->date('blast_date')->index();
            $table->smallInteger('eblast_coupon_template_id')->default(500);
            $table->string('slug');
            $table->string('email_subject');
            $table->text('email_plain_text');
            $table->text('blast_template_values');
            $table->integer('request_id');

            $table->timestamps();
		});

        Schema::create('eblast_coupon_coupons', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('eblast_coupon_id')->index();
            $table->integer('coupon_id')->index();

            $table->date('display_start_date')->index();
            $table->date('display_end_date')->index();
            $table->date('offer_start_date');
            $table->date('offer_end_date');

            $table->timestamps();
        });

        Schema::create('eblast_coupon_locations_groups', function(Blueprint $table)
        {
            $table->integer('eblast_coupon_id')->index();
            $table->integer('location_id')->index();
            $table->integer('group_id')->nullable()->index();
        });

        Schema::create('eblast_coupon_templates', function (Blueprint $table) {
            $table->increments('id');

            $table->string('template_name');
            $table->string('template_desc');
            $table->string('template_source');
            $table->string('slug');
            $table->string('blast_type');
            $table->string('email_subject');
            $table->text('email_plain_text');
            $table->text('blast_template_values');
            $table->string('image_dir');
            $table->string('thumbnail_filename');

            $table->timestamps();

        });

        $eblast_coupon_templates = array(
            array(
                'id' => '500',
                'template_name' => 'default template',
                'template_desc' => '',
                'template_source' => 'EblastCouponTemplateSource_Default',
                'slug' => 'eBlast',
                'blast_type' => 'eBlast',
                'email_subject' => '--|site_loc_name|-- Special Savings for --|eblast_subject_date|--',
                'email_plain_text' => 'As requested, here are our Special Savings, just for you!

To redeem simply:

(1) Visit this page:
--|blast_url|--

(2) Print page that opens

(3) Bring printed page to store

Thanks for being our customer!

Forward this to a Friend:
--|blast_url|--

Visit our Website:
--|homepage_url|--&l=pt

--|site_loc_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list:
--|unsubscribe_url|--

Feel that you\'ve received this email in error?
Please contact us here:
--|contact_url|--',
                'image_dir' => '',
                'thumbnail_filename' => '',
                'blast_template_values' => '{"coupon_type":"EBlast Coupon"}')
);

        DB::table('eblast_coupon_templates')->insert($eblast_coupon_templates);

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('eblast_coupons');
        Schema::drop('eblast_coupon_coupons');
        Schema::drop('eblast_coupon_templates');
        Schema::drop('eblast_coupon_locations_groups');
	}
}