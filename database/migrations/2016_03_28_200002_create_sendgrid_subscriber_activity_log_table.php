<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSendgridSubscriberActivityLogTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('sendgrid_subscriber_activity_log', function (Blueprint $table) {
            $table->increments('id');

            $table->string('user_id');
            $table->dateTime('log_datetime');
            $table->string('type');
            $table->string('email');
            $table->string('reason');
            $table->string('status');
            $table->text('notes');
            $table->integer('subscriber_id')->index();
            $table->integer('location_id')->index();
            $table->string('blast_type');
            $table->integer('blast_id')->index();
            $table->boolean('sendgrid_deleted')->unsigned()->default(0);
            $table->boolean('cleaner_scanned')->unsigned()->default(0);
            $table->boolean('unsubscribed')->unsigned()->default(0);

            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('sendgrid_subscriber_activity_log');

    }
}