<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlastTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('blast_emails', function(Blueprint $table) {
		    $table->increments('id');

		    $table->string('email_type', 25)                    -> comment = "BlastEmailType enum";
		    $table->date('send_date')->index()                  -> comment = "Date we WANT the mail to send";
		    $table->time('send_time')                           -> comment = "Time to send. Some time (hopefully soon) after this time";
		    $table->integer('location_id')->unsigned()->index() -> comment = "Location id for the blast";
		    $table->string('blast_type', 35)                    -> comment = "BlastType enum";
		    $table->integer('blast_id')->unsigned();

		    $table->tinyInteger('auto_send')->unsigned()->default(0)  -> comment = "flag: set to 1 if auto mailer should pick this up";
		    $table->tinyInteger('production')->unsigned()->default(0) -> comment = "flag: if 1, no other email for blast_id and location_id will be created";

		    $table->tinyInteger('sent')->unsigned()->default(0)->index()          -> comment = "set to 1 AFTER all batches of smtp send completed";
		    $table->tinyInteger('failed')->unsigned()->default(0)                 -> comment = "set to 1 if smtp send fails";
		    $table->mediumInteger('total_sent')->unsigned()->default(0)           -> comment = "updated after each smtp send";
		    $table->mediumInteger('total_opened')->unsigned()->default(0)         -> comment = "rolled up from blast_tracking_log";
		    $table->mediumInteger('total_unique_opened')->unsigned()->default(0)  -> comment = "rolled up from blast_tracking_log";
		    $table->mediumInteger('total_clicked')->unsigned()->default(0)        -> comment = "rolled up from blast_tracking_log";
		    $table->mediumInteger('total_unique_clicked')->unsigned()->default(0) -> comment = "rolled up from blast_tracking_log";
		    $table->mediumInteger('total_failed')->unsigned()->default(0)         -> comment = "rolled up from smtp fail log";

		    $table->text('recipient_strategy') -> comment = "class name for how recipients are chosen";
		    $table->text('send_strategy')      -> comment = "class name for how smtp_sends are created";
		    $table->text('mailer_overrides')   -> comment = "json. any overrides for the mailer factory";
		    $table->text('last_message')       -> comment = "last message from mailer factory";

		    $table->timestamps();

		    $table->index(['blast_type', 'blast_id']);
		});

		Schema::create('blast_email_recipients', function(Blueprint $table) {
		    $table->increments('id');

		    $table->integer('blast_email_id')->unsigned()->index();
		    $table->integer('blast_email_recipient_type_id')->unsigned();
		    $table->integer('subscriber_id')->unsigned()->index();
		    $table->string('email_address', 100);

		    $table->timestamps();
		});

		Schema::create('blast_email_recipient_types', function(Blueprint $table) {
		    $table->increments('id');
		    $table->string('name', 25);
		});

		// Insert some blast email recipient types
		$blast_recipient_types = [
			['id' => '1', 'name' => 'Subscriber'],
			['id' => '2', 'name' => 'QA Recipient'],
			['id' => '3', 'name' => 'Manual Recipient'],
			['id' => '4', 'name' => 'Location Contact']
		];
        DB::table('blast_email_recipient_types')->insert( $blast_recipient_types );

	    Schema::create('blast_tracking_log', function (Blueprint $table) {
	        $table->increments('id');

	        $table->integer('blast_email_recipient_id')->unsigned()->index();
	        $table->string('event', 25);
	        $table->string('link', 25);
	        $table->string('user_agent', 255);
	        $table->string('ip_address', 25);
	        $table->string('other', 255);

	        $table->timestamp('date_time');
	    });

		Schema::create('blast_smtp_batches', function(Blueprint $table) {
			$table->increments('id');

			$table->string('batch_name', 50);
			$table->mediumInteger('sends')->unsigned()->default(0);
			$table->dateTime('sent_datetime');

            $table->timestamps();

		});

		Schema::create('blast_smtp_sends', function(Blueprint $table) {
			$table->increments('id');

			$table->integer('blast_smtp_batch_id')->unsigned()->index();
			$table->integer('blast_email_id')->unsigned()->index();
			$table->mediumInteger('recipients')->unsigned()->default(0);
			$table->dateTime('sent_datetime');

            $table->timestamps();

		});

		Schema::create('blast_smtp_sends2recipients', function(Blueprint $table) {

			$table->integer('blast_smtp_send_id')->unsigned()->index();
			$table->integer('blast_recipient_id')->unsigned()->index();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blast_emails');
		Schema::drop('blast_email_recipients');
		Schema::drop('blast_tracking_log');
		Schema::drop('blast_smtp_batches');
		Schema::drop('blast_smtp_sends');
		Schema::drop('blast_smtp_sends2recipients');
		Schema::drop('blast_email_recipient_types');

	}
}

