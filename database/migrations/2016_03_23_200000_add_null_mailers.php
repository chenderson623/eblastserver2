<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullMailers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        $smtp_provider = [
            'id' => 3,
            'name' => 'Null Mailer'
        ];
        DB::table('smtp_providers')->insert( $smtp_provider );

        $smtp_provider_account = [
            'id' => 1,
            'smtp_provider_id' => 3,
            'name' => 'Null Mailer'
        ];
        DB::table('smtp_provider_accounts')->insert( $smtp_provider_account );

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}
}