<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomEblastTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('custom_eblasts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('custom_eblast_template_id');
            $table->string('name');
            $table->string('slug');
            $table->string('email_subject');
            $table->text('email_plain_text');
            $table->text('blast_template_values');
            $table->string('image_dir');
            $table->date('blast_date')->index();
            $table->date('display_start_date');
            $table->date('display_end_date');
            $table->date('offer_start_date');
            $table->date('offer_end_date');

            $table->timestamps();
        });

        Schema::create('custom_eblast_templates', function (Blueprint $table) {
            $table->increments('id');

            $table->string('template_name');
            $table->string('template_desc');
            $table->string('template_source');
            $table->string('slug');
            $table->string('blast_type');
            $table->string('email_subject');
            $table->text('email_plain_text');
            $table->text('blast_template_values');
            $table->string('image_dir');
            $table->string('thumbnail_filename');

            $table->timestamps();

        });

        Schema::create('custom_eblast_locations_groups', function (Blueprint $table) {
            $table->integer('custom_eblast_id')->index();
            $table->integer('location_id')->nullable()->index();
            $table->integer('group_id')->nullable()->index();
        });

        $custom_eblast_templates = array(
            array('id' => '506', 'template_name' => 'coupons.com announcement', 'template_desc' => '', 'template_source' => 'CustomEblastTemplateSource_CouponsCom', 'slug' => 'Coupons.Com', 'blast_type' => 'special_offers', 'email_subject' => '--|site_loc_name|-- - Start Saving Now with Coupons.com', 'email_plain_text' => 'Announcing a new feature from --|site_loc_name|--

Start Saving Now With Our Online Savings Center

Print at Home, Shop Local, Save Better!

Counpons.com - Get Free printable coupons and more from our Coupons.com Online Savings Center.

To Access Our Online Savings Center:
1. Visit our Website
2. Click on Ads & Coupons
3. Select Coupons.com
4. Select the Coupons You Want to Print
5. Click Print Coupons and Start Saving!

Forward this to a friend: --|web_url|--?mid=-track_code-&recid=-blast_recipient-&l=pt

Visit our website: --|site_url|--?mid=-track_code-&recid=-blast_recipient-&l=pt
--|store_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list: --|site_url|--/subscribe/opt.php?mid=-track_code-&recid=-blast_recipient-&l=pt', 'blast_template_values' => '{
    "main_image_alt_text":"Enable images to view",
    "main_image_title_text":"--|site_loc_name|-- - Coupons.com",
    "main_image_link_url":"--|web_url|--"
        }', 'image_dir' => '', 'thumbnail_filename' => ''),
            array('id' => '528', 'template_name' => 'With Coupon', 'template_desc' => '', 'template_source' => 'CustomEblastTemplateSource_WithCoupon', 'slug' => 'SpecialCoupon', 'blast_type' => 'special_offers', 'email_subject' => '[fill in offer here]', 'email_plain_text' => '--|site_loc_name|--

Here is a special coupon for you.

Offer available from --|offer_start_date_formatted|-- through --|offer_end_date_formatted|--

Go here for more info: --|blast_url|--&l=pt

Visit our website: --|homepage_url|--&l=pt

--|store_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list:
--|unsubscribe_url|--&l=pt', 'blast_template_values' => '{
    "main_image_alt_text":"Enable images to view",
    "main_image_title_text":"Special Offers",
    "main_image_link_url":"--|blast_url|--&l=main",
    "has_coupons": "true"
}', 'image_dir' => '', 'thumbnail_filename' => ''),
            array('id' => '624', 'template_name' => 'Special Deal-ivery', 'template_desc' => '', 'template_source' => 'CustomEblastTemplateSource_SpecialDealivery', 'slug' => 'SpecialDealivery', 'blast_type' => 'special_offers', 'email_subject' => 'Special Deal-ivery', 'email_plain_text' => '--|site_loc_name|--

Free [CHANGE TO ITEM] with $25.00 purchase.

Offer available from --|offer_start_date_formatted|-- through --|offer_end_date_formatted|--

Go here for more info: --|blast_url|--&l=pt

Visit our website: --|homepage_url|--&l=pt

--|store_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list:
--|unsubscribe_url|--&l=pt', 'blast_template_values' => '{
    "main_image_alt_text":"Enable images to view",
    "main_image_title_text":"Special Offers",
    "main_image_link_url":"--|blast_url|--&l=main",
    "has_coupons": "true"
        }', 'image_dir' => '', 'thumbnail_filename' => ''),
            array('id' => '625', 'template_name' => 'Standard Custom Eblast', 'template_desc' => '', 'template_source' => 'CustomEblastTemplateSource_CenteredLogo', 'slug' => '[replace with something meaningful]', 'blast_type' => 'special_offers', 'email_subject' => 'A Special Offer From --|site_loc_name|--', 'email_plain_text' => '--|site_loc_name|--

A Special offer for you

Offer available from --|offer_start_date_formatted|-- through --|offer_end_date_formatted|--

Go here for more info: --|blast_url|--&l=pt

Visit our website: --|homepage_url|--&l=pt

--|store_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list:
--|unsubscribe_url|--&l=pt', 'blast_template_values' => '{
    "main_image_alt_text":"Enable images to view",
    "main_image_title_text":"Special Offers",
    "main_image_link_url":"--|blast_url|--&l=main",
    "has_print_view": "true"
        }', 'image_dir' => '', 'thumbnail_filename' => ''),
            array('id' => '626', 'template_name' => '3 Day Sale Animated', 'template_desc' => '', 'template_source' => 'CustomEblastTemplateSource_3DaySaleAnimated', 'slug' => '[replace with something meaningful]', 'blast_type' => 'special_offers', 'email_subject' => 'A Special Offer From --|site_loc_name|--', 'email_plain_text' => '--|site_loc_name|--

A Special offer for you

Offer available from --|offer_start_date_formatted|-- through --|offer_end_date_formatted|--

Go here for more info: --|blast_url|--&l=pt

Visit our website: --|homepage_url|--&l=pt

--|store_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list:
--|unsubscribe_url|--&l=pt', 'blast_template_values' => '{
    "banner_image_alt_text":"Enable images to view",
    "banner_image_title_text":"3-Day Sale",
    "main_image_alt_text":"Enable images to view",
    "main_image_title_text":"Special Offers",
    "main_image_link_url":"--|blast_url|--&l=main",
    "has_print_view": "true"
        }', 'image_dir' => '', 'thumbnail_filename' => ''),
            array('id' => '627', 'template_name' => 'Animated Banner', 'template_desc' => '', 'template_source' => 'CustomEblastTemplateSource_AnimatedBanner', 'slug' => '[replace with something meaningful]', 'blast_type' => 'special_offers', 'email_subject' => 'A Special Offer From --|site_loc_name|--', 'email_plain_text' => '--|site_loc_name|--

A Special offer for you

Offer available from --|offer_start_date_formatted|-- through --|offer_end_date_formatted|--

Go here for more info: --|blast_url|--&l=pt

Visit our website: --|homepage_url|--&l=pt

--|store_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list:
--|unsubscribe_url|--&l=pt', 'blast_template_values' => '{
    "banner_image_alt_text":"Enable images to view",
    "banner_image_title_text":"3-Day Sale",
    "main_image_alt_text":"Enable images to view",
    "main_image_title_text":"Special Offers",
    "main_image_link_url":"--|blast_url|--&l=main",
    "has_print_view": "true",
    "banner_image_filename": "3-DAY-SALE-ANI-BANNER.gif"
        }', 'image_dir' => '', 'thumbnail_filename' => ''),
            array('id' => '628', 'template_name' => 'Buche Foods - Custom', 'template_desc' => '', 'template_source' => 'CustomEblastTemplateSource_BucheChooseStore', 'slug' => '[replace with something meaningful]', 'blast_type' => 'special_offers', 'email_subject' => 'Sign Up For Savings At Buche Foods', 'email_plain_text' => '--|site_loc_name|--

Update your info today to continue to get our weekly ad exclusive offers and more

--|store_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list:
--|unsubscribe_url|--&l=pt', 'blast_template_values' => '{
    "banner_image_alt_text":"Enable images to view",
    "banner_image_title_text":"Sign Up For Savings",
    "main_image_alt_text":"Enable images to view",
    "main_image_title_text":"Sign Up For Savings",
    "main_image_link_url":"--|blast_url|--&l=main",
    "has_print_view": "true"
        }', 'image_dir' => '', 'thumbnail_filename' => ''),
            array('id' => '629', 'template_name' => 'Maynards', 'template_desc' => '', 'template_source' => 'CustomEblastTemplateSource_Maynards', 'slug' => '[replace with something meaningful]', 'blast_type' => 'special_offers', 'email_subject' => '--|email_subject|--', 'email_plain_text' => '--|site_loc_name|--

--|email_plain_text|--

--|store_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list:
--|unsubscribe_url|--&l=pt', 'blast_template_values' => '{
    "has_print_view": "false"
}', 'image_dir' => '', 'thumbnail_filename' => ''),
        );

        DB::table('custom_eblast_templates')->insert($custom_eblast_templates);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('custom_eblasts');
        Schema::drop('custom_eblast_templates');
        Schema::drop('custom_eblast_locations_groups');

    }
}