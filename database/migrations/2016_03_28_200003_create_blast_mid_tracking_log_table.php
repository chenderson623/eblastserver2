<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlastMidTrackingLogTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('blast_mid_tracking_log', function (Blueprint $table) {
            $table->increments('id');

            $table->string('blast_type')->index();
            $table->integer('blast_id')->index();
            $table->integer('location_id')->index();
            $table->string('subscriber_id')->index();
            $table->string('tracking_type');
            $table->string('user_agent');

            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('blast_mid_tracking_log');

    }
}