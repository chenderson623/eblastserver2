<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWholesalerBlastTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('wholesaler_blasts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('wholesaler_blast_template_id');
            $table->integer('store_affiliation_id');
            $table->string('name');
            $table->string('slug');
            $table->string('email_subject');
            $table->text('email_plain_text');
            $table->text('blast_template_values');
            $table->string('image_dir');
            $table->date('opt_in_date');
            $table->date('blast_date')->index();
            $table->date('display_start_date');
            $table->date('display_end_date');
            $table->date('offer_start_date');
            $table->date('offer_end_date');

            $table->timestamps();
        });
        Schema::create('wholesaler_blast_locations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('wholesaler_blast_id')->index();
            $table->integer('location_id')->index();
            $table->string('checksum');
            $table->boolean('opt_in');
            $table->boolean('opt_out');
            $table->date('blast_date');

            $table->timestamps();

        });
        Schema::create('wholesaler_blast_templates', function (Blueprint $table) {
            $table->increments('id');

            $table->string('template_name');
            $table->string('template_desc');
            $table->string('template_source');
            $table->string('opt_in_template_source');
            $table->string('opt_in_smtp_provider');
            $table->string('opt_in_smtp_username');
            $table->string('opt_in_email_from_name');
            $table->string('slug');
            $table->string('blast_type');
            $table->string('email_subject');
            $table->text('email_plain_text');
            $table->text('blast_template_values');
            $table->string('image_dir');
            $table->string('thumbnail_filename');

            $table->timestamps();

        });
        Schema::create('wholesaler_blast_transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('object_name');
            $table->string('object_id');
            $table->string('action');
            $table->smallInteger('action_level');
            $table->string('description');
            $table->string('user');
            $table->string('ip_address');
            $table->string('user_agent');
            $table->string('script');
            $table->string('referer');
            $table->integer('parent_transaction_id');

            $table->timestamps();

        });

        $wholesaler_blast_templates = array(
            array(
                'id'                     => '250',
                'template_name'          => 'e-Bonus [CHANGE TO MONTH]',
                'template_desc'          => 'AFM e-Bonus Program',
                'template_source'        => 'EBonusCouponTemplateSource_CenteredLogo',
                'opt_in_template_source' => 'EBonusCouponTemplateSource_StoreNotify',
                'opt_in_smtp_provider'   => 'SendGrid',
                'opt_in_smtp_username'   => 'afmconnect',
                'opt_in_email_from_name' => 'AFM Connect',
                'slug'                   => '[CHANGE TO ITEM]',
                'blast_type'             => 'ebonus_vip_coupons',
                'email_subject'          => '[CHANGE TO MONTH] e-Bonus VIP Item - [CHANGE TO ITEM]',
                'email_plain_text'       => '--|site_loc_name|--

[CHANGE TO ITEM] free with $20 grocery purchase

e-BONUS COUPON GOOD --|offer_start_date_formatted|-- - --|offer_end_date_formatted|--

View this e-Bonus VIP Item coupon at: --|blast_url|--&l=pt

Visit our website: --|homepage_url|--&l-pt

--|site_loc_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list: --|unsubscribe_url|--&l=pt',
                'blast_template_values'  => '{"main_image_alt_text":"Enable images to view","main_image_title_text":"--|site_loc_name|-- - e-Bonus VIP Item","main_image_link_url":"--|blast_url|--"}',
                'image_dir'              => '',
                'thumbnail_filename'     => ''),

            array('id' => '251', 'template_name' => 'e-Bonus [CHANGE TO MONTH]', 'template_desc' => 'LGC Big eBlastDeals', 'template_source' => 'EBonusCouponTemplateSource_PrintPage', 'opt_in_template_source' => 'EBonusCouponTemplateSource_StoreNotify_LGC', 'opt_in_smtp_provider' => 'SendGrid', 'opt_in_smtp_username' => 'lgcconnect', 'opt_in_email_from_name' => 'Laurel Grocery Company', 'slug' => 'BigEblastDeals', 'blast_type' => 'big_eblast_deals', 'email_subject' => 'Big e-Blast Deals', 'email_plain_text' => '--|site_loc_name|--

Prices Effective --|offer_start_date_formatted|-- - --|offer_end_date_formatted|--

View these special deals: --|blast_url|--&l=pt

Visit our website: --|homepage_url|--&l-pt

--|site_loc_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list: --|unsubscribe_url|--&l=pt', 'blast_template_values' => '{"main_image_alt_text":"Enable images to view","main_image_title_text":"e-Blast Deals","main_image_link_url":"--|blast_url|--"}', 'image_dir' => '', 'thumbnail_filename' => ''),
            array('id' => '252', 'template_name' => 'IGA Only Version', 'template_desc' => 'LGC Big eBlastDeals', 'template_source' => 'EBonusCouponTemplateSource_PrintPage', 'opt_in_template_source' => 'EBonusCouponTemplateSource_StoreNotify_LGC_IGA', 'opt_in_smtp_provider' => 'SendGrid_Manual', 'opt_in_smtp_username' => 'lgcconnect', 'opt_in_email_from_name' => 'Laurel Grocery Company', 'slug' => 'BigEblastDeals', 'blast_type' => 'big_eblast_deals', 'email_subject' => 'Big e-Blast Deals', 'email_plain_text' => '--|site_loc_name|--

Prices Effective --|offer_start_date_formatted|-- - --|offer_end_date_formatted|--

View these special deals: --|blast_url|--&l=pt

Visit our website: --|homepage_url|--&l-pt

--|site_loc_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list: --|unsubscribe_url|--&l=pt', 'blast_template_values' => '{"main_image_alt_text":"Enable images to view","main_image_title_text":"e-Blast Deals","main_image_link_url":"--|blast_url|--"}', 'image_dir' => '', 'thumbnail_filename' => ''),
            array('id' => '253', 'template_name' => 'Shurfine Only Version', 'template_desc' => 'LGC Big eBlastDeals', 'template_source' => 'EBonusCouponTemplateSource_PrintPage', 'opt_in_template_source' => 'EBonusCouponTemplateSource_StoreNotify_LGC_Shurfin', 'opt_in_smtp_provider' => 'SendGrid_Manual', 'opt_in_smtp_username' => 'lgcconnect', 'opt_in_email_from_name' => 'Laurel Grocery Company', 'slug' => 'BigEblastDeals', 'blast_type' => 'big_eblast_deals', 'email_subject' => 'Big e-Blast Deals', 'email_plain_text' => '--|site_loc_name|--

Prices Effective --|offer_start_date_formatted|-- - --|offer_end_date_formatted|--

View these special deals: --|blast_url|--&l=pt

Visit our website: --|homepage_url|--&l-pt

--|site_loc_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list: --|unsubscribe_url|--&l=pt', 'blast_template_values' => '{"main_image_alt_text":"Enable images to view","main_image_title_text":"e-Blast Deals","main_image_link_url":"--|blast_url|--"}', 'image_dir' => '', 'thumbnail_filename' => ''),
            array('id' => '254', 'template_name' => 'AFM IGA Real Deal eBlasts', 'template_desc' => 'AFM IGA Real Deal eBlasts', 'template_source' => 'EBonusCouponTemplateSource_AFM_IGA', 'opt_in_template_source' => 'EBonusCouponTemplateSource_StoreNotify_AFM_IGA', 'opt_in_smtp_provider' => 'SendGrid_Manual', 'opt_in_smtp_username' => 'afmconnect', 'opt_in_email_from_name' => 'AFM IGA Real Deals', 'slug' => 'IGARealDeal', 'blast_type' => 'iga_real_deal', 'email_subject' => 'IGA Real Deal e-Blast', 'email_plain_text' => '--|site_loc_name|--

Prices Effective --|offer_start_date_formatted|-- - --|offer_end_date_formatted|--

View these special deals: --|blast_url|--&l=pt

Visit our website: --|homepage_url|--&l-pt

--|site_loc_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list: --|unsubscribe_url|--&l=pt', 'blast_template_values' => '{"main_image_alt_text":"Enable images to view","main_image_title_text":"IGA Real Deal","main_image_link_url":"--|blast_url|--"}', 'image_dir' => '', 'thumbnail_filename' => ''),
        );

        DB::table('wholesaler_blast_templates')->insert($wholesaler_blast_templates);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        Schema::drop('wholesaler_blasts');
        Schema::drop('wholesaler_blast_locations');
        Schema::drop('wholesaler_blast_templates');
        Schema::drop('wholesaler_blast_transactions');

    }
}