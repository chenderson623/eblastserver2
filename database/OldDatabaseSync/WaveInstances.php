<?php

namespace OldDatabaseSync;

use DB;

/**
 * This class contains definitions to the wave instances
 */
class WaveInstances {

    //singleton:
    private static $instance;
    // These are the wholesaler instances that will be synced
    // To shorten a test sync, you can sync only the wholeslaers you want
    public static $selected_wholesalers = array(2, 1, 4, 5, 6, 7, 8, 9, 12, 13, 14, 15, 16, 17); // 2 (afm) first for it's default content
    // Wholesaler definitions: id's come from Service Desk
    public static $wave_instances_data = array(
        array(
            "wholesaler_id" => 2,
            "url"           => "afmsdc.mediasolutionscorp.com",
            "database"      => "afm",
            "name"          => "AFM",
            "display_name"  => 'Affiliated Foods Midwest',
            "demo_site_ids" => array(9, 145, 250, 511)
        ),
        array(
            "wholesaler_id" => 1,
            "url"           => "lgcsdc.mediasolutionscorp.com",
            "database"      => "lgc",
            "name"          => "LGC",
            "display_name"  => "Laurel Grocery",
            "demo_site_ids" => array(1, 9)
        ),
        array(
            "wholesaler_id" => 4,
            "url"           => "owgsdc.mediasolutionscorp.com",
            "database"      => "owg",
            "name"          => "Olean",
            "demo_site_ids" => array(9,430, 431, 432)
        ),
        array(
            "wholesaler_id" => 5,
            "url"           => "mscsdc.mediasolutionscorp.com",
            "database"      => "demo_msc",
            "name"          => "MSC Demos",
            "demo_site_ids" => array(9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21)
        ),
        array(
            "wholesaler_id" => 6,
            "url"           => "sdc.unifieddemosite.com",
            "database"      => "unified",
            "name"          => "Unified",
            "demo_site_ids" => array(9, 10, 11)
        ),
        array(
            "wholesaler_id" => 7,
            "url"           => "sdc.agsdemosite.com",
            "database"      => "ags",
            "name"          => "AGS",
            "demo_site_ids" => array(9, 10, 11)
        ),
        array(
            "wholesaler_id" => 8,
            "url"           => "sdc.afidemosite.com",
            "database"      => "afi",
            "name"          => "AFI",
            "demo_site_ids" => array(9, 10, 11)
        ),
        array(
            "wholesaler_id" => 9,
            "url"           => "storessdc.mediasolutionscorp.com",
            "database"      => "standalone_stores",
            "name"          => "Standalone",
            "demo_site_ids" => array()
        ),
        array(
            "wholesaler_id" => 12,
            "url"           => "sdc.gsdemosite.com",
            "database"      => "grocerssupply",
            "name"          => "Grocers Supply",
            "demo_site_ids" => array(9, 11)
        ),
        array(
            "wholesaler_id" => 13,
            "url"           => "sdc.ngademosite.com",
            "database"      => "nga",
            "name"          => "NGA",
            "demo_site_ids" => array(9, 11)
        ),
        array(
            "wholesaler_id" => 14,
            "url"           => "sdc.afsdemosite.com",
            "database"      => "afs",
            "name"          => "AFS",
            "demo_site_ids" => array(9, 10, 11)
        ),
        array(
            "wholesaler_id" => 15,
            "url"           => "sdc.cgdemosite.com",
            "database"      => "centralgrocers",
            "name"          => "Central Grocers",
            "demo_site_ids" => array(9, 10, 11)
        ),
        array(
            "wholesaler_id" => 16,
            "url"           => "sdc.rofdademosite.com",
            "database"      => "rofda",
            "name"          => "ROFDA",
            "demo_site_ids" => array(9, 10, 11)
        ),
        array(
            "wholesaler_id" => 17,
            "url"           => "sdc.urmdemosite.com",
            "database"      => "urm",
            "name"          => "URM",
            "demo_site_ids" => array(9, 10, 11)
        ),
    );

    public static $wave_instances;

    protected function __construct() {

    }

    public static function getInstance() {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected static function getWaveInstances() {
        if(!isset(self::$wave_instances)) {
            self::$wave_instances = array();
            foreach(self::$wave_instances_data as $wave_instance_data) {
                $wholesaler_instance = self::createWaveInstance($wave_instance_data);
                self::$wave_instances[$wholesaler_instance->getWholesalerId()] = $wholesaler_instance;
            }
        }
        return self::$wave_instances;
    }

    public static function getInstanceDataByWholesalerId($wholesaler_id) {
        foreach (self::$wave_instances_data as $wave_instance_data) {
            if ((int) $wave_instance_data['wholesaler_id'] === (int) $wholesaler_id) {
                return $wave_instance_data;
            }
        }
        return false;
    }

    protected static function getWholesalerDataIsAvailable($wave_instance_data) {
        return self::getWholesalerIdIsAvailable($wave_instance_data['wholesaler_id']);
    }

    public static function getWholesalerIdIsAvailable($wholesaler_id) {
        return in_array($wholesaler_id, self::$selected_wholesalers);
    }

    //
    // Iterators
    //--------------------------------------------------------------------------------------------

    public static function wholesalerIdIterator() {
        $iterator = new \CallbackFilterIterator(new \ArrayIterator(self::$selected_wholesalers), '\OldDatabaseSync\WaveInstances::getInstanceDataByWholesalerId');
        return $iterator;
    }

    public static function instanceDataIterator() {
        $iterator = new \CallbackFilterIterator(new \ArrayIterator(self::$wave_instances_data), '\OldDatabaseSync\WaveInstances::getWholesalerDataIsAvailable');
        return $iterator;
    }

    /**
     * Only returns the instances that are selected AND have a valid connection
     * @return \CallbackFilterIterator
     */
    public static function instanceIterator() {
        $iterator = new \CallbackFilterIterator(new \ArrayIterator(self::getWaveInstances()), function($wave_instance) {
            if(!self::getWholesalerIdIsAvailable($wave_instance->getWholesalerId())) {
                return false;
            }

            // Check if database exists
            try {
                DB::select("SHOW TABLES FROM " . $wave_instance->getDatabaseName());
                return true;
            } catch (\Exception $e) {
                return false;
            }

        });
        return $iterator;
    }

    //
    // WaveInstance
    //--------------------------------------------------------------------------------------------

    public static function createWaveInstance($wave_instance_data) {
        return new WaveInstance($wave_instance_data);
    }

}
