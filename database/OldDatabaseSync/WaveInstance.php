<?php

namespace OldDatabaseSync;

use OldDatabaseSync\WaveModels\LandingSites as WaveLandingSites;
use OldDatabaseSync\Eblasts\LandingSiteGroups;

class WaveInstance {

    protected $wave_instance_data;
    protected $wholesaler_group;
    protected $wholesaler_id;

    public function __construct($wave_instance_data) {
        $this->wave_instance_data = $wave_instance_data;
        $this->wholesaler_id = (int) $wave_instance_data['wholesaler_id'];
    }

    public function getWholesalerId() {
        return $this->wholesaler_id;
    }

    public function getDatabaseName() {
        return $this->wave_instance_data['database'];
    }

    public function calculateLocationId($old_site_id) {
        return SolutionsCenter::calculateLocationId($this->wholesaler_id, $old_site_id);
    }

    public function getClientIdClientId($old_site_id) {
        return SolutionsCenter::calculateLocationId($this->wholesaler_id, $old_site_id);
    }

    public function calculateLandingGroupId($old_landing_id) {
        return SolutionsCenter::calculateLandingGroupId($this->wholesaler_id, $old_landing_id);
    }

    public function calculateMaxLandingGroupId() {
        return SolutionsCenter::calculateMaxLandingGroupId($this->wholesaler_id);
    }

    public function calculateMultiplierId($old_id, $multiplier) {
        return ($this->wholesaler_id * $multiplier) + (int) $old_id;
    }

    public function getName() {
        return $this->wave_instance_data['name'];
    }

    public function getDemoSiteIds() {
        return $this->wave_instance_data['demo_site_ids'];
    }

    public function isDemoSite($site_id) { //site_id will be the OLD site_id - same as $wholesaler['demo_site_ids']
        if(in_array($site_id, $this->getDemoSiteIds())) {
            return true;
        }
        return false;
    }

    public function hasDemoSites($site_ids) {
        foreach($site_ids as $site_id) {
            if($this->isDemoSite($site_id)) {
                return true;
            }
        }
        return false;
    }


}
