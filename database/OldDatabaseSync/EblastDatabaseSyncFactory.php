<?php namespace OldDatabaseSync;

use Illuminate\Events\Dispatcher;

class EblastDatabaseSyncFactory {

    protected $log_name= 'EblastDatabase';  //log with the other wave database tables
    protected $database;

    /**
     * @var \OldDatabaseSync\SyncSession
     */
    protected $sync_session;

    /**
     * @var \OldDatabaseSync\WaveInstance
     */
    protected $wave_instance;

    public function __construct(SyncSession $sync_session, WaveInstance $wave_instance) {
        $this->sync_session = $sync_session;
        $this->wave_instance     = $wave_instance;
        $this->database          = $this->wave_instance->getName();
    }

    public static function create(SyncSession $sync_session, WaveInstance $wave_instance) {
        return new EblastDatabaseSyncFactory($sync_session, $wave_instance);
    }

    /**
     * @var \Illuminate\Events\Dispatcher
     */
    protected $event_dispatcher;

    /**
     * @return \Illuminate\Events\Dispatcher
     */
    protected function getEventDispatcher() {
        if (!isset($this->event_dispatcher)) {
            $this->event_dispatcher = new Dispatcher();
            $this->event_dispatcher->subscribe($this->getRecordCounterHandler());
            $this->event_dispatcher->subscribe($this->getRecordMessageHandler());
        }
        return $this->event_dispatcher;
    }

    /**
     * @var RecordCounterHandler
     */
    protected $record_counter;

    /**
     * @return RecordCounterHandler
     */
    protected function getRecordCounterHandler() {
        if (!isset($this->record_counter)) {
            $this->record_counter = new Events\RecordCounterHandler($this->log_name, $this->database, $this->sync_session);
        }
        return $this->record_counter;
    }

    /**
     * @var RecordMessageHandler
     */
    protected $record_messages;

    /**
     * @return RecordMessageHandler
     */
    protected function getRecordMessageHandler() {
        if (!isset($this->record_messages)) {
            $this->record_messages = new Events\RecordMessageHandler($this->getRecordCounterHandler(), $this->sync_session);
        }
        return $this->record_messages;
    }

    protected function setLastRecordId($sync_query) {
        $last_record_id = $this->sync_session->getRunStatus()->getLastRecordIdCopied($this->log_name, $this->database, $sync_query->getSyncName());
        if (!empty($last_record_id)) {
            //override the date that has already been set
            $sync_query->setLastRecordId($last_record_id);
        }
    }

    protected function printHeader() {
        $this->sync_session->writeln("");
        $this->sync_session->writeln("");
        $this->sync_session->writeln("=========================================================================================");
        $this->sync_session->writeln("Syncing {$this->wave_instance->getName()} Eblasts and subscribers");
        $this->sync_session->writeln("=========================================================================================");
        $this->sync_session->writeln("");
    }

    public function copy(array $sync_queries) {
        $this->printHeader();
        $this->getEventDispatcher()->fire('start', []);
        foreach ($sync_queries as $sync_query) {
            $this->getRecordMessageHandler()->setNewQuery($sync_query);
            $this->getRecordCounterHandler()->setNewQuery($sync_query);

            $sync_query->setEventDispatcher($this->getEventDispatcher());
            $this->setLastRecordId($sync_query);

            $this->getEventDispatcher()->fire('copy_start', []);
            $sync_query->copy();
            $this->getEventDispatcher()->fire('copy_done', []);
        }
        $this->getEventDispatcher()->fire('done', []);
    }

    public function update(array $sync_queries) {
        $this->printHeader();
        $this->getEventDispatcher()->fire('start', []);
        foreach ($sync_queries as $sync_query) {
            $this->getRecordMessageHandler()->setNewQuery($sync_query);
            $this->getRecordCounterHandler()->setNewQuery($sync_query);

            $sync_query->setEventDispatcher($this->getEventDispatcher());
            //$this->setLastRecordId($sync_query);

            $this->getEventDispatcher()->fire('update_start', []);
            $sync_query->update();
            $this->getEventDispatcher()->fire('update_done', []);
        }
        $this->getEventDispatcher()->fire('done', []);
    }

    /**
     * Delete all previous records from other runs. Used to ensure that the data
     * being synced is accurate.
     *
     */
    public static function deletePreviousRecords(SyncSession $sync_session) {

        $tables = [
            'blast_email_recipients',
            'blast_email_tracking_events',
            'blast_emails',
            'blast_mid_tracking_log',
            'blast_smtp_batches',
            'blast_smtp_sends',
            'blast_smtp_sends2recipients',
            'eblast_coupon_coupons',
            'eblast_coupon_locations_groups',
            'eblast_coupon_templates',
            'eblast_coupons',
            'migrations',
            'password_resets',
            'sendgrid_send_log',
            'sendgrid_subscriber_activity_log',
            'smtp_provider_accounts',
            'smtp_provider_accounts_locations',
            'smtp_providers',
            'subscriber_histories',
            'subscriber_profiles',
            'subscriber_sessions',
            'subscriber_subscriptions',
            'users',
            'webblast_locations_groups',
            'webblast_templates',
            'webblasts',
            'wholesaler_blast_locations',
            'wholesaler_blast_templates',
            'wholesaler_blast_transactions',
            'wholesaler_blasts',
        ];


        foreach($tables as $table) {
            \DB::delete("TRUNCATE $table");
        }

    }

}
