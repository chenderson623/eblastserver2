<?php namespace OldDatabaseSync\EblastsDatabase\SyncQueries;

use DB;
use OldDatabaseSync\BaseSyncQuery;
use OldDatabaseSync\EblastsDatabase\RecordSyncs\EblastCoupon;

class EblastCoupons extends BaseSyncQuery {

    protected $multiplier = 10000;
    public $table_name    = 'eblast_coupons';
    public $id_key        = 'eblast_id';
    public $site_ids_key  = 'eblast_site_ids';
    protected $from_date;

    protected function setup() {
    }

    //
    // Copy
    //-----------------------------------------------------------------------------------

    public function copy() {
        $today = new \DateTime();
        DB::table($this->wave_instance->getDatabaseName() . '.' . $this->table_name)
            ->where($this->id_key, '>', $this->last_record_id)
            ->where(function ($query) use ($today) {
                $query->where('eblast_end_date', '>', $today)
                    ->orWhere(function ($query) {
                        $query->where('eblast_end_date', '>', $this->from_date->format('Y-m-d'));
                    });
            })
            ->chunk(200, array($this, 'copyRecords'));
    }

    public function copyRecord($record) {
        $record = (array) $record;
        $this->fire('copy_record_start', array($record, $record[$this->id_key]));

        $record_sync = new EblastCoupon($record, $this);
        $record_sync->setEventDispatcher($this->event_dispatcher);
        $record_sync->copy();

        $this->fire('copy_record_done', array($record_sync));

        return $record_sync;
    }

    //
    // Update
    //-----------------------------------------------------------------------------------

    public function update() {
        $today = new \DateTime();
        DB::table($this->wave_instance->getDatabaseName() . '.' . $this->table_name)
            ->where(function ($query) use ($today) {
                $query->where('eblast_end_date', '>', $today)
                    ->orWhere(function ($query) {
                        $query->where('eblast_end_date', '>', $this->from_date->format('Y-m-d'));
                    });
            })
            ->chunk(200, array($this, 'updateRecords'));
    }

    public function updateRecord($record) {
        $record = (array) $record;
        $this->fire('update_record_start', array($record, $record[$this->id_key]));

        $record_sync = new EblastCoupon($record, $this);
        $record_sync->setEventDispatcher($this->event_dispatcher);
        $record_sync->update();

        $this->fire('update_record_done', array($record_sync));

        return $record_sync;
    }
}
