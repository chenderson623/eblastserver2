<?php namespace OldDatabaseSync\EblastsDatabase\SyncQueries;

use DB;
use OldDatabaseSync\BaseSyncQuery;
use OldDatabaseSync\EblastsDatabase\RecordSyncs\BlastMidTrackingLog;

class BlastMidTrackingLogs extends BaseSyncQuery {

    protected $multiplier = 100000000;
    public $table_name    = 'promo_tracking';
    public $id_key        = 'id';

    protected function setup() {

    }

    //
    // Copy
    //-----------------------------------------------------------------------------------

    public function copy() {
        $dtTm = new \DateTime('-1 MONTH');

        DB::table($this->wave_instance->getDatabaseName() . '.' . $this->table_name)
                ->where($this->id_key, '>', $this->last_record_id)
                ->where('access_timestamp', '>', $dtTm->format('Y-m-d'))
                ->chunk(200, array($this, 'copyRecords'));
    }

    public function copyRecord($record) {
        $record         = (array) $record;
        $this->fire('copy_record_start', array($record, $record[$this->id_key]));
        $record_sync = new BlastMidTrackingLog($record, $this);
        $record_sync->setEventDispatcher($this->event_dispatcher);
        $record_sync->copy();

        $this->fire('copy_record_done', array($record_sync));

        return $record_sync;
    }

    //
    // Update
    //-----------------------------------------------------------------------------------

    public function update() {
        DB::table($this->wave_instance->getDatabaseName() . '.' . $this->table_name)
                ->chunk(200, array($this, 'updateRecords'));
    }

    public function updateRecord($record) {
        $record = (array) $record;
        $this->fire('update_record_start', array($record, $record[$this->id_key]));

        $record_sync = new BlastMidTrackingLog($record, $this);
        $record_sync->setEventDispatcher($this->event_dispatcher);
        $record_sync->update();

        $this->fire('update_record_done', array($record_sync));

        return $record_sync;
    }

}
