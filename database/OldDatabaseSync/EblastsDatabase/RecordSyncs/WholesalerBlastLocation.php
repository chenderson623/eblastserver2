<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;

class WholesalerBlastLocation extends BaseRecordSync {

    public static $db_mappings;
    protected $eblast_model = '\App\Models\WholesalerBlastLocation';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(

                    'wholesaler_blast_id' => 'ebonus_coupon_id',
                    'location_id' => 'site_id',
                    'checksum' => 'checksum',
                    'opt_in' => 'opt_in',
                    'opt_out' => 'opt_out',
                    'blast_date' => 'blast_date',

                ),
                'convert' => array(
                //old-fieldname => function
                )
            );
        }
    }


    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return \App\Models\WholesalerBlastLocation::find($find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        $transform_array['location_id'] = $this->getLocationId($transform_array['location_id']);

        if ($transform_array['wholesaler_blast_id'] > 0) {
            $wholesaler_blasts                      = new WholesalerBlasts($this->getWaveInstance());
            $transform_array['wholesaler_blast_id'] = $wholesaler_blasts->calculateMultiplierId($transform_array['wholesaler_blast_id']);
        }

        return $this->excludeArray($transform_array, $exclude);
    }

}
