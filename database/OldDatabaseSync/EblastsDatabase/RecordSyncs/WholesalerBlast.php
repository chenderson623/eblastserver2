<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;

class WholesalerBlast extends BaseRecordSync {

    public static $db_mappings;
    protected $eblast_model = '\App\Models\WholesalerBlast';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(
                    'wholesaler_blast_template_id' => 'blast_template_id',
                    'store_affiliation_id'  => 'store_affiliation_id',
                    'title'                 => 'blast_name',
                    'slug'                  => 'web_slug',
                    'email_subject'         => 'email_subject',
                    'email_plain_text'      => 'email_plain_text',
                    'blast_template_values' => 'blast_template_values',
                    'image_dir'             => 'image_dir',
                    'opt_in_date'           => 'opt_in_date',
                    'blast_date'            => 'blast_date',
                    'display_start_date'    => 'display_start_date',
                    'display_end_date'      => 'display_end_date',
                    'offer_start_date'      => 'offer_start_date',
                    'offer_end_date'        => 'offer_end_date',
                ),
                'convert' => array(
                //old-fieldname => function
                )
            );
        }
    }

    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return \App\Models\WholesalerBlast::find($find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        return $this->excludeArray($transform_array, $exclude);
    }

}
