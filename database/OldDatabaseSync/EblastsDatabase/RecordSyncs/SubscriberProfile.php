<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;

class SubscriberProfile extends BaseRecordSync {

    public static $db_mappings;
    protected $new_model_classpath = 'EblastServer\Subscribers\Models\Eloquent\SubscriberProfile';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(
                    'email'          => 'email',
                    'domain'         => 'domain',
                    'first_name'     => 'first_name',
                    'last_name'      => 'last_name',
                    'telephone'      => 'telephone',
                    'active'         => 'active',
                    'date_added'     => 'date_added',
                    'date_removed'   => 'date_removed',
                    'hash'           => 'hash',
                    'remote_checked' => 'remote_checked',
                    'remote_valid'   => 'remote_valid',
                ),
                'convert' => array(
                //old-fieldname => function
                )
            );
        }
    }

    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return call_user_func([$this->new_model_classpath, 'find'], $find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        return $this->excludeArray($transform_array, $exclude);
    }

}
