<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;
use OldDatabaseSync\EblastsDatabase\SyncQueries\SubscriberProfiles;

class SubscriberSubscription extends BaseRecordSync {

    public static $db_mappings;
    protected $new_model_classpath = 'EblastServer\Subscribers\Models\Eloquent\SubscriberSubscription';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(
                    'subscriber_id'    => 'subscriber_id',
                    'location_id'      => 'site_id',
                    'hash'             => 'hash',
                    'active'           => 'active',
                    'ad_blast'         => 'ad_blast',
                    'eblast'           => 'eblast',
                    'newsletters'      => 'newsletters',
                    'recipes'          => 'recipes',
                    'store_blasts'     => 'store_blasts',
                    'store_management' => 'store_management',
                    'date_added'       => 'date_added',
                    'date_removed'     => 'date_removed',
                ),
                'convert' => array(
                //old-fieldname => function
                )
            );
        }
    }

    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return call_user_func([$this->new_model_classpath, 'find'], $find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        $transform_array['location_id'] = $this->getLocationId($transform_array['location_id']);

        if ($transform_array['subscriber_id'] > 0) {
            $profiles                         = new SubscriberProfiles($this->getWaveInstance());
            $transform_array['subscriber_id'] = $profiles->calculateMultiplierId($transform_array['subscriber_id']);
        }

        return $this->excludeArray($transform_array, $exclude);
    }

}
