<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;
use OldDatabaseSync\EblastsDatabase\SyncQueries\SubscriberProfiles;
use OldDatabaseSync\EblastsDatabase\SyncQueries\SubscriberHistories;

class SubscriberHistoryItem extends BaseRecordSync {
    public static $db_mappings;
    protected $new_model_classpath = '\EblastServer\Subscribers\Models\Eloquent\SubscriberHistoryItem';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(
                    'subscriber_session_id' => 'subscriber_session_id',
                    'subscriber_id'         => 'subscriber_id',
                    'location_id'           => 'site_id',
                    'event'                 => 'event',
                    'event_code'            => 'event_code',
                    'event_datetime'        => 'event_datetime',
                    'description'           => 'description',
                    'remote_code'           => 'remote_code',
                    'data_json'             => 'data_json',
                    'undo_json'             => 'undo_json',
                    'undone'                => 'undone',
                    'parent_history_id'     => 'parent_history_id',
                    'created_at'            => 'created_datetime',
                ),
                'convert' => array(
                //old-fieldname => function
                )
            );
        }
    }

    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return call_user_func([$this->new_model_classpath, 'find'], $find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        $transform_array['location_id'] = $this->getLocationId($transform_array['location_id']);

        if ($transform_array['subscriber_id'] > 0) {
            $profiles                         = new SubscriberProfiles($this->getWaveInstance());
            $transform_array['subscriber_id'] = $profiles->calculateMultiplierId($transform_array['subscriber_id']);
        }

        if ($transform_array['subscriber_session_id'] > 0) {
            $profiles                                 = new SubscriberHistories($this->getWaveInstance());
            $transform_array['subscriber_session_id'] = $profiles->calculateMultiplierId($transform_array['subscriber_session_id']);
        }

        return $this->excludeArray($transform_array, $exclude);
    }

}
