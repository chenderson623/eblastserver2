<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;

class BlastMidTrackingLog extends BaseRecordSync {

    public static $db_mappings;
    protected $eblast_model = '\App\Models\BlastMidTrackingLog';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(

                    'blast_type'    => 'promo_type',
                    'blast_id'      => 'promo_id',
                    'location_id'   => 'site_id',
                    'subscriber_id' => 'user_id',
                    'tracking_type' => 'tracking_type',
                    'user_agent'    => 'user_agent',

                ),
                'convert' => array(
                //old-fieldname => function
                )
            );
        }
    }

    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return \App\Models\BlastMidTrackingLog::find($find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        $transform_array['created_at'] = $this->wave_record['access_timestamp'];

        return $this->excludeArray($transform_array, $exclude);
    }

}
