<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;

class EblastCoupon extends BaseRecordSync {

    public static $db_mappings;
    protected $eblast_model = '\App\Models\EblastCoupon';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(
                    'title'      => 'eblast_name_1',
                    'blast_date' => 'eblast_blast_date',
                ),
                'convert' => array(
                ),
            );
        }
    }

    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return \App\Models\EblastCoupon::find($find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        if (!empty($this->wave_record['eblast_name_2'])) {
            $transform_array['title'] = $transform_array['title'] . ' & ' . $this->wave_record['eblast_name_2'];
        }

        if($transform_array['blast_date'] === '1000-01-01') {
            $transform_array['blast_date'] = $this->wave_record['eblast_start_date'];
        }

        return $this->excludeArray($transform_array, $exclude);
    }

    protected function mapEblastCouponData($record_array) {
        $import_data = array();

        $mappings = array(
            'display_start_date' => 'eblast_start_date',
            'display_end_date'   => 'eblast_end_date',
            'offer_start_date'   => 'eblast_start_date',
            'offer_end_date'     => 'eblast_end_date',
        );

        foreach ($mappings as $new_field => $old_field) {
            $value                   = $this->convertMappingValue($old_field, $record_array[$old_field]);
            $import_data[$new_field] = $value;
        }

        return $import_data;
    }

    protected function mapCouponData($record_array, $coupon_number) {
        $import_data = array();

        $mappings = array(
            'title'                  => 'eblast_name_' . $coupon_number,
            'price'                  => 'eblast_price_' . $coupon_number,
            'description'            => 'eblast_description_' . $coupon_number,
            'disclaimer'             => 'eblast_disclaimer_' . $coupon_number,
            'plu'                    => 'eblast_plu_' . $coupon_number,
            'redemption_value'       => 'eblast_rv_' . $coupon_number,
            'product_image_filename' => 'eblast_product_image_file_' . $coupon_number,
            'beauty_image_filename'  => 'eblast_beauty_shot_' . $coupon_number,
            'upc_image_file'         => 'eblast_upc_image_file_' . $coupon_number,
        );

        foreach ($mappings as $new_field => $old_field) {
            $value                   = $this->convertMappingValue($old_field, $record_array[$old_field]);
            $import_data[$new_field] = $value;
        }

        $import_data['dir_name'] = $this->getDirectoryName();

        return $import_data;
    }

    public function copy() {
        $eblast = parent::copy();

        if (!empty($eblast)) {
            // Create coupon 1
            $eblast_coupon_data = $this->mapEblastCouponData($this->wave_record);
            $eblast_coupon      = \App\Models\EblastCouponCoupon::create($eblast_coupon_data);

            $coupon_data = $this->mapCouponData($this->wave_record, 1);
            $coupon      = \App\Models\WaveWebsites\Coupon::create($coupon_data);

            $eblast_coupon->eblast_coupon_id = $eblast->id;
            $eblast_coupon->coupon_id        = $coupon->id;
            $eblast_coupon->save();

            if (!empty($this->wave_record['eblast_name_2'])) {
                // Create coupon 2
                $eblast_coupon_data = $this->mapEblastCouponData($this->wave_record);
                $eblast_coupon      = \App\Models\EblastCouponCoupon::create($eblast_coupon_data);

                $coupon_data = $this->mapCouponData($this->wave_record, 2);
                $coupon      = \App\Models\WaveWebsites\Coupon::create($coupon_data);

                $eblast_coupon->eblast_coupon_id = $eblast->id;
                $eblast_coupon->coupon_id        = $coupon->id;
                $eblast_coupon->save();
            }
        }

        return $eblast;
    }

    public function update() {
        $record = parent::update();

        if (!empty($record)) {
        }

        return $record;
    }

}
