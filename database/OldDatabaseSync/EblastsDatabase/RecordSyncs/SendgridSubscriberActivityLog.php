<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;

class SendgridSubscriberActivityLog extends BaseRecordSync {

    public static $db_mappings;
    protected $eblast_model = '\App\Models\SendgridSubscriberActivityLog';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(

                    'user_id'          => 'user_id',
                    'log_datetime'     => 'datetime',
                    'type'             => 'type',
                    'email'            => 'email',
                    'reason'           => 'reason',
                    'status'           => 'status',
                    'notes'            => 'notes',
                    'subscriber_id'    => 'subscriber_id',
                    'location_id'      => 'site_id',
                    'blast_type'       => 'promo_type',
                    'blast_id'         => 'promo_id',
                    'sendgrid_deleted' => 'sendgrid_deleted',
                    'cleaner_scanned'  => 'cleaner_scanned',
                    'unsubscribed'     => 'unsubscribed',

                ),
                'convert' => array(
                //old-fieldname => function
                )
            );
        }
    }

    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return \App\Models\SendgridSubscriberActivityLog::find($find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        $transform_array['created_at'] = $this->wave_record['date_gathered'];

        return $this->excludeArray($transform_array, $exclude);
    }

}
