<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;

class SendgridCredential extends BaseRecordSync {

    public static $db_mappings;
    protected $eblast_model = '\App\Models\SmtpProviderAccount';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(
                    'username' => 'username',
                    'password' => 'password',
                    'name'     => 'url',
                    'domain'   => 'url'
                ),
                'convert' => array(
                //old-fieldname => function
                )
            );
        }
    }

    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return \App\Models\SmtpProviderAccount::find($find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        $transform_array['smtp_provider_id'] = 1; // All are SendGrid Smtp

        return $this->excludeArray($transform_array, $exclude);
    }

    protected function lookUpSiteByUrl($url) {
        $sites = \DB::table($this->getWaveInstance()->getDatabaseName() . '.sites')
                ->where('site_url', '=', $url)
                ->get();
        if(count($sites) > 0) {
            return $sites[0];
        }
        return null;
    }

    protected function createSiteContentRecord() {
        $model      = $this->eblast_model;
        call_user_func([$model, 'unguard']);
        $new_record = new $model($this->getTransformArray());

        //lookup site from url
        $url  = $this->wave_record['url'];
        $site = $this->lookUpSiteByUrl($url);
        if($site) {
            $new_record->save();
            $location_id = $this->getLocationId($site->site_id);
            $new_record->attachLocation($location_id);
        }

        return $new_record;
    }

    protected function updateSiteContentRecord($record) {
        // Not going to worry too much about updates
        $record->fill($this->getTransformArray());
        if ($record->isDirty()) {
            $record->save();
            $this->updated = true; // Event flag set here
        }
        return $record;
    }
}
