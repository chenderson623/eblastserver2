<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;
use OldDatabaseSync\EblastsDatabase\SyncQueries\SubscriberProfiles;

class SubscriberSession extends BaseRecordSync {

    public static $db_mappings;
    protected $new_model_classpath = '\EblastServer\Subscribers\Models\Eloquent\SubscriberSession';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(
                    'session_type'     => 'session_type',
                    'subscriber_id'    => 'subscriber_id',
                    'user'             => 'user',
                    'ip_address'       => 'ip_address',
                    'user_agent'       => 'user_agent',
                    'script'           => 'script',
                    'web_session'      => 'web_session',
                    'referer'          => 'referer',
                    'created_at'       => 'created_datetime',
                ),
                'convert' => array(
                //old-fieldname => function
                )
            );
        }
    }

    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return call_user_func([$this->new_model_classpath, 'find'], $find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        if ($transform_array['subscriber_id'] > 0) {
            $profiles                         = new SubscriberProfiles($this->getWaveInstance());
            $transform_array['subscriber_id'] = $profiles->calculateMultiplierId($transform_array['subscriber_id']);
        }

        return $this->excludeArray($transform_array, $exclude);
    }

}
