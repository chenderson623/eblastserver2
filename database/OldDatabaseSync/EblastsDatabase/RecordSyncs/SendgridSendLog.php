<?php namespace OldDatabaseSync\EblastsDatabase\RecordSyncs;

use OldDatabaseSync\BaseRecordSync;

class SendgridSendLog extends BaseRecordSync {

    public static $db_mappings;
    protected $eblast_model = '\App\Models\SendgridSendLog';

    public function setup() {
        //need to set this up here because of the anonymous functions
        if (!isset(self::$db_mappings)) {
            self::$db_mappings = array(
                'fields'  => array(
                    'blast_type'    => 'promo_type',
                    'blast_id'      => 'promo_id',
                    'blast_date'    => 'date',
                    'location_id'   => 'site_id',
                    'smtp_username' => 'smtp_username',
                    'send_count'    => 'send_count',
                    'batch'         => 'batch',
                ),
                'convert' => array(
                //old-fieldname => function
                )
            );
        }
    }

    public function isDemoContent() {
        return false;
    }

    protected function findSiteContentRecord() {
        $find_id = $this->getSiteContentId();
        return \App\Models\SendgridSendLog::find($find_id);
    }

    protected function getTransformArray($exclude = array()) {
        $transform_array       = $this->mapOldData($this->wave_record);
        $transform_array['id'] = $this->getSiteContentId();

        $transform_array['created_at'] = $this->wave_record['log_entry_created'];

        return $this->excludeArray($transform_array, $exclude);
    }

}
