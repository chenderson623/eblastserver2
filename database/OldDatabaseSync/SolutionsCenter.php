<?php

namespace OldDatabaseSync;

class SolutionsCenter {

    //singleton:
    private static $instance;

    //
    // Singleton
    //---------------------------------------------------------------------------------------------
    protected function __construct() {
    }

    public function __destruct() {
    }

    /**
     * @return OldDatabaseSync\SyncSession
     */
    public static function getInstance() {
        if (null === static ::$instance) {
            static ::$instance = new static ();
        }

        return static ::$instance;
    }


    //
    // id Calculators
    //---------------------------------------------------------------------------------------------
    public static function calculateLocationId($servicedesk_wholesaler_id, $wave_site_id) {
        return $servicedesk_wholesaler_id * 10000 + (int) $wave_site_id;
    }

    public static function calculateWholesalerGroupId($servicedesk_wholesaler_id) {
        return 200 + (int) $servicedesk_wholesaler_id;
    }

    public static function calculateWholesalerClientId($servicedesk_wholesaler_id) {
        return 100 + (int) $servicedesk_wholesaler_id;
    }



}
