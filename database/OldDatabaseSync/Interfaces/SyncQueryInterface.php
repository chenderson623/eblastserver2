<?php

namespace OldDatabaseSync\Interfaces;

interface SyncQueryInterface {
    public function getSyncName();
    public function copy();
}