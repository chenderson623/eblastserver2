<?php

namespace OldDatabaseSync\Interfaces;

interface RecordSyncInterface {

    public function created();
    public function updated();

}