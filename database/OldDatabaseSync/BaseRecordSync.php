<?php namespace OldDatabaseSync;

use OldDatabaseSync\Interfaces\RecordSyncInterface;

abstract class BaseRecordSync implements RecordSyncInterface {

    /**
     * @var BaseQuert
     */
    protected $sync_query;

    /**
     * @var \Illuminate\Events\Dispatcher
     */
    protected $event_dispatcher;
    protected $wave_record;
    protected $new_model_classpath;
    protected $created = false;
    protected $updated = false;

    public function __construct($wave_record, BaseSyncQuery $sync_query) {
        $this->wave_record     = $wave_record;
        $this->sync_query = $sync_query;
        $this->setup();
    }

    abstract public function setup();

    public function created() {
        return $this->created;
    }

    public function updated() {
        return $this->updated;
    }

    public function fire($event_string, $args) {
        if (isset($this->event_dispatcher)) {
            $this->event_dispatcher->fire($event_string, $args);
        }
    }

    public function listen($event_string, $handler) {
        if (isset($this->event_dispatcher)) {
            $this->event_dispatcher->listen($event_string, $handler);
        }
    }

    public function setEventDispatcher(\Illuminate\Events\Dispatcher $event_dispatcher = null) {
        $this->event_dispatcher = $event_dispatcher;
    }

    public function getWaveInstance() {
        return $this->sync_query->getWaveInstance();
    }

    public function getDemoContent() {
        return $this->sync_query->getDemoContent();
    }

    public function canCreateDemoContent() {
        return !$this->getDemoContent()->hasRunOnce();
    }

    public function isDemoContent() {
        return $this->getWaveInstance()->hasDemoSites($this->getSiteIds());
    }

    public function isDemoSite($site_id) {
        return $this->getWaveInstance()->isDemoSite($site_id);
    }

    public function saveDemoContent($new_record) {
        if ($this->canCreateDemoContent() && $this->isDemoContent()) {
            $this->getDemoContent()->addDemoContent($new_record);
        }
    }

    abstract protected function getTransformArray($exclude = array());

    protected function getSiteIds() {
        if(empty($this->sync_query->site_ids_key)) {
            return [];
        }
        return $this->parseSiteIds($this->wave_record[$this->sync_query->site_ids_key]);
    }

    protected function getSiteContentId() {
        return $this->sync_query->calculateMultiplierId($this->getOldId());
    }

    protected function getLocationId($old_site_id) {
        return $this->sync_query->calculateLocationId($old_site_id);
    }

    public function getOldId() {
        return $this->wave_record[$this->sync_query->id_key];
    }

    protected function getDirectoryName() {
        return "00-" . substr("0" . $this->getWaveInstance()->getWholesalerId(), -2);
    }

    public function copy() {
        if ($this->isDemoContent() && !$this->canCreateDemoContent()) {
            return null;
        }
        // existing record
        $record = $this->findSiteContentRecord();
        if (empty($record)) {
            $record        = $this->createSiteContentRecord();
            $this->created = true;
        }

        if ($this->isDemoContent()) {
            $this->saveDemoContent($record);
        }

        return $record;
    }

    public function update() {
        if ($this->isDemoContent() && !$this->canCreateDemoContent()) {
            return null;
        }

        // existing record
        $record = $this->findSiteContentRecord();
        if (empty($record)) {
            return $this->copy();
        }
        $this->updateSiteContentRecord($record);

        if ($this->isDemoContent()) {
            $this->saveDemoContent($record);
        }

        return $record;
    }

    protected function createSiteContentRecord() {
        $model      = $this->new_model_classpath;
        call_user_func([$model, 'unguard']);
        $new_record = new $model($this->getTransformArray());
        $this->attachToLocations($new_record, $this->getSiteIds(), true);
        $new_record->save();
        return $new_record;
    }

    protected function updateSiteContentRecord($record) {
        $record->fill($this->getTransformArray());
        if ($record->isDirty()) {
            //var_dump($record->getDirty());
            //var_dump($record);
            $record->save();
            $this->updated = true; // Event flag set here
        }
        $this->attachToLocations($record, $this->getSiteIds(), true);
        return $record;
    }

    protected function excludeArray($array, $exclude = array()) {
        foreach ($exclude as $key) {
            if (isset($array[$key])) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    protected function parseSiteIds($site_ids_string) {
        $site_ids = array_filter(explode('||', $site_ids_string));
        return $site_ids;
    }

    protected function mapOldData($record_array) {
        $import_data = array();
        foreach (static::$db_mappings['fields'] as $new_field => $old_field) {
            $value                   = $this->convertMappingValue($old_field, (isset($record_array[$old_field])) ? $record_array[$old_field] : '' );
            $import_data[$new_field] = $value;
        }
        return $import_data;
    }

    protected function convertMappingValue($old_field, $value) {
        if (!isset(static::$db_mappings['convert'][$old_field])) {
            return $value;
        }
        $function = static::$db_mappings['convert'][$old_field];
        return $function($value);
    }

    public function attachToLocations($new_record, $site_ids, $skip_demo_sites = false) {

        if (is_null($new_record)) {
            return;
        }

        if (empty($site_ids)) {
            return; //some record types don't have sites
        }

        foreach ($site_ids as $site_id) {
            if ($skip_demo_sites && $this->isDemoSite($site_id)) {
                continue;
            }
            $new_location_id = $this->getLocationId($site_id);
            $new_record->attachLocation($new_location_id);
        }
    }

}
