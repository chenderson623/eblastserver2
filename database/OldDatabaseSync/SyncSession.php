<?php
namespace OldDatabaseSync;

use Illuminate\Support\Facades\Event;

/**
 * Singleton class to globalize session instances
 */
class SyncSession {

    //singleton:
    private static $instance;

    protected function __construct() {
        $this->getRunStatus()->setLastRun(strftime('%F %T'));
        $this->setupNotifiers();

        $this->writeln("\n\n");
    }

    public function __destruct() {
        $this->getRunStatus()->saveFile();
    }

    /**
     * @return OldDatabaseSync\SyncSession
     */
    public static function getInstance() {
        if (null === static ::$instance) {
            static ::$instance = new static ();
        }

        return static ::$instance;
    }

    public function write($str) {
        echo $str;
    }

    public function writeln($str = '') {
        echo $str . "\n";
    }

    protected function setupNotifiers() {
        Event::listen('sync.group.created', function($group){ echo "Group Created: {$group->name}\n";});
        Event::listen('sync.client.created', function($client){ echo "Client Created: {$client->business_name}\n";});
        Event::listen('sync.groups.loaded', function(){ echo "Groups Loaded.\n\n"; });
    }

    /**
     * @var RunStatus
     */
    protected $run_status;

    /**
     * @return RunStatus
     */
    public function getRunStatus() {
        if (!isset($this->run_status)) {
            $this->run_status = new RunStatus();
            $this->run_status->loadFile();
        }
        return $this->run_status;
    }

    public function clearRunStatus() {
        $this->getRunStatus()->clearAll();
        $this->getRunStatus()->setLastRun(strftime('%F %T'));
    }

    public function saveRunStatus() {
        $this->getRunStatus()->saveFile();
    }

    public function setLastRecordIdCopied($factory, $database, $sync_name, $recordid) {
        $this->getRunStatus()->setLastRecordIdCopied($factory, $database, $sync_name, $recordid);
    }
    public function setLastDateCreated($factory, $database, $sync_name, $datetime) {
        $this->getRunStatus()->setLastDateCreated($factory, $database, $sync_name, $datetime);
    }
    public function setLastDateUpdated($factory, $database, $sync_name, $datetime) {
        $this->getRunStatus()->setLastDateUpdated($factory, $database, $sync_name, $datetime);
    }
}