<?php

namespace OldDatabaseSync\Events;

/**
 * Handles tracking records and writing to RunStatus
 */
class RecordCounterHandler {

    /**
     * @var \OldDatabaseSync\SyncSession
     */
    protected $sync_session;

    // RunStatus keys:
    protected $factory;
    protected $database;
    protected $sync_name;

    protected $last_record_id = 0;
    protected $last_date_created = '0000-00-00';
    protected $last_date_updated = '0000-00-00';

    protected $records_processed = 0;
    protected $records_created = 0;
    protected $records_updated = 0;

    public function __construct($factory, $database, \OldDatabaseSync\SyncSession $sync_session) {
        $this->factory = $factory;
        $this->database = $database;
        $this->sync_session = $sync_session;
    }

    protected function setLastRecordId($record, $record_id = null) {

        if(empty($record_id)) {
            $record_id = $record->id;
        }

        // special case for requests. want to track requestable_id, if it exists
        if(!empty($record->requestable_id)) {
            $record_id = $record->requestable_id;
        }

        if($record_id > $this->last_record_id) {
            $this->last_record_id = $record_id;
        }
    }

    protected function setLastDateCreated($record) {
        if(!is_object($record)) {
            return;
        }
        $datetime = (string) $record->created_at;
        if($datetime > $this->last_date_created) {
            $this->last_date_created = $datetime;
        }
    }

    protected function setLastDateUpdated($record) {
        if(!is_object($record)) {
            return;
        }
        $datetime = (string) $record->updated_at;
        if($datetime > $this->last_date_updated) {
            $this->last_date_updated = $datetime;
        }
    }

    public function getStats() {
        return [
            'last_record_id'    => $this->last_record_id,
            'last_date_created' => $this->last_date_created,
            'last_date_updated' => $this->last_date_updated,
            'records_created'   => $this->records_created,
            'records_updated'   => $this->records_updated,
            'records_processed' => $this->records_processed
        ];
    }

    protected function reset() {
        $this->last_record_id = 0;
        $this->last_date_created = '0000-00-00';
        $this->last_date_updated = '0000-00-00';

        $this->records_processed = 0;
        $this->records_created = 0;
        $this->records_updated = 0;
    }

    public function setNewQuery(\OldDatabaseSync\Interfaces\SyncQueryInterface $query) {
        $this->sync_name = $query->getSyncName();
        $this->reset();
    }

    //
    // Event Handlers
    //---------------------------------------------------------------------
    public function onCopyDone() {
        if($this->records_processed > 0) {
            //if nothing was done, don't overwrite the values in RunStatus
            $this->sync_session->setLastRecordIdCopied($this->factory, $this->database, $this->sync_name, $this->last_record_id);
            $this->sync_session->setLastDateCreated($this->factory, $this->database, $this->sync_name, $this->last_date_created);
            $this->sync_session->setLastDateUpdated($this->factory, $this->database, $this->sync_name, $this->last_date_updated);
        }
    }
    public function onUpdateDone() {
        if($this->records_processed > 0) {
            //if nothing was done, don't overwrite the values in RunStatus
            $this->sync_session->setLastRecordIdCopied($this->factory, $this->database, $this->sync_name, $this->last_record_id);
            $this->sync_session->setLastDateCreated($this->factory, $this->database, $this->sync_name, $this->last_date_created);
            $this->sync_session->setLastDateUpdated($this->factory, $this->database, $this->sync_name, $this->last_date_updated);
        }
    }
    public function onCopyBatchStart($records) {

    }
    public function updateRunStatus($records) {
        $this->onCopyDone();
        $this->sync_session->saveRunStatus();
        gc_collect_cycles();
    }
    public function onCopyRecordStart($record, $record_id = null) {
        $this->records_processed++;
        $this->setLastRecordId($record, $record_id);
        $this->setLastDateCreated($record);
        $this->setLastDateUpdated($record);
    }
    public function onUpdateRecordStart($record, $record_id = null) {
        $this->records_processed++;

        // if it's updated, it's also copied
        $this->setLastRecordId($record, $record_id);
        $this->setLastDateCreated($record);

        $this->setLastDateUpdated($record);
    }

    public function countSyncEvents(\OldDatabaseSync\Interfaces\RecordSyncInterface $sync_obj = null) {
        if(!$sync_obj) {
            return;
        }
        if($sync_obj->updated()) {
            $this->records_updated++;
        }
        if($sync_obj->created()) {
            $this->records_created++;
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events) {
        //$events->listen('copy_start', [$this, 'onCopyStart']);
        $events->listen('copy_done', [$this, 'onCopyDone']);
        $events->listen('update_done', [$this, 'onUpdateDone']);

        //$events->listen('copy_batch_start', [$this, 'onCopyBatchStart']);
        $events->listen('copy_batch_done', [$this, 'updateRunStatus']);
        $events->listen('update_batch_done', [$this, 'updateRunStatus']);

        $events->listen('copy_record_start', [$this, 'onCopyRecordStart']);
        $events->listen('copy_record_done', [$this, 'countSyncEvents']);

        $events->listen('update_record_start', [$this, 'onUpdateRecordStart']);
        $events->listen('update_record_done', [$this, 'countSyncEvents']);
    }

}