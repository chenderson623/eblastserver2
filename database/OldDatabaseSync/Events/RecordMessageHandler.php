<?php

namespace OldDatabaseSync\Events;

class RecordMessageHandler {

    /**
     * @var RecordCounterHandler
     */
    protected $record_counter;

    /**
     * @var \OldDatabaseSync\SyncSession
     */
    protected $sync_session;

    protected $start;

    public function __construct(RecordCounterHandler $record_counter, \OldDatabaseSync\SyncSession $sync_session) {
        $this->record_counter = $record_counter;
        $this->sync_session = $sync_session;
    }

    protected function getBaseClassName($obj) {
        $classname = get_class($obj);
        $parts = explode('\\', $classname);
        return array_pop($parts);
    }

    public function setNewQuery($query) {
        $this->sync_session->write($this->getBaseClassName($query) . ': ');
    }

    //
    // Event Handlers
    //---------------------------------------------------------------------
    public function onStart() {
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $this->start = $time;
    }
    public function onCopyDone() {
        $this->sync_session->writeln(' ' . $this->record_counter->getStats()['records_created'] . ' Copied');
    }
    public function onUpdateDone() {
        $this->sync_session->writeln(' ' . $this->record_counter->getStats()['records_created'] . ' Copied, ' . $this->record_counter->getStats()['records_updated'] . ' Updated');
    }
    public function tickBatch() {
        $this->sync_session->write('.');
    }
    public function onDone() {
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;
        $total_time = round(($finish - $this->start), 4);

        $this->sync_session->writeln(' ');
        $this->sync_session->writeln("Done in $total_time seconds");
        $this->sync_session->writeln(' ');
    }
    public function onNotice($message) {
        $this->sync_session->write(' ' . $message . ' ');
    }
    public function onWarning($message) {
        $this->sync_session->write(" \033[31m{$message}\033[0m ");
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     * @return array
     */
    public function subscribe($events) {
        $events->listen('start', [$this, 'onStart']);

        //$events->listen('copy_start', [$this, 'onCopyStart']);
        $events->listen('copy_done', [$this, 'onCopyDone']);
        $events->listen('update_done', [$this, 'onUpdateDone']);

        $events->listen('copy_batch_start', [$this, 'tickBatch']);
        $events->listen('update_batch_start', [$this, 'tickBatch']);
        //$events->listen('copy_batch_done', [$this, 'onCopyBatchDone']);

        //$events->listen('copy_record_start', [$this, 'onCopyRecordStart']);
        //$events->listen('copy_record_done', [$this, 'onCopyRecordDone']);

        $events->listen('done', [$this, 'onDone']);

        $events->listen('notice', [$this, 'onNotice']);
        $events->listen('warning', [$this, 'onWarning']);
    }

}
