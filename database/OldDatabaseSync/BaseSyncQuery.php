<?php namespace OldDatabaseSync;

use OldDatabaseSync\Interfaces\SyncQueryInterface;
use OldDatabaseSync\WaveInstance;

abstract class BaseSyncQuery implements SyncQueryInterface {

    protected $last_record_id = 0;

    /**
     * @var \Illuminate\Events\Dispatcher
     */
    protected $event_dispatcher;

    /**
     * @var \OldDatabaseSync\WaveInstance
     */
    protected $wave_instance;
    protected $multiplier = 1;
    protected $demo_content;

    public function __construct(WaveInstance $wave_instance, \DateTime $from_date = null) {
        $this->wave_instance = $wave_instance;
        $this->from_date     = empty($from_date) ? new \DateTime('0000-00-00') : $from_date;
        $this->setup();
    }

    abstract protected function setup();

    public function getSyncName() {
        return $this->table_name;
    }

    public function setEventDispatcher(\Illuminate\Events\Dispatcher $event_dispatcher) {
        $this->event_dispatcher = $event_dispatcher;
    }

    /**
     * @return \OldDatabaseSync\WaveInstance
     */
    public function getWaveInstance() {
        return $this->wave_instance;
    }

    public function calculateMultiplierId($old_id) {
        return $this->getWaveInstance()->calculateMultiplierId($old_id, $this->multiplier);
    }

    public function calculateLocationId($old_site_id) {
        return $this->getWaveInstance()->calculateLocationId($old_site_id);
    }

    public function getDemoContent() {
        if (!isset($this->demo_content)) {
            throw new \Exception("Need to set up demo content in setup");
        }
        return $this->demo_content;
    }

    public function fire($event_string, $args) {
        if (isset($this->event_dispatcher)) {
            $this->event_dispatcher->fire($event_string, $args);
        }
    }

    public function listen($event_string, $handler) {
        if (isset($this->event_dispatcher)) {
            $this->event_dispatcher->listen($event_string, $handler);
        }
    }

    abstract public function copy();
    abstract public function copyRecord($record);

    public function setLastRecordId($last_record_id) {
        $this->last_record_id = $last_record_id;
    }

    public function copyRecords($records) {
        if (count($records) > 0) {
            $this->fire('copy_batch_start', array($records, $this));
        }
        foreach ($records as $record) {
            $this->copyRecord($record);
        }
        if (count($records) > 0) {
            $this->fire('copy_batch_done', array($records, $this));
        }
    }

    abstract public function update();
    abstract public function updateRecord($record);

    public function updateRecords($records) {
        if (count($records) > 0) {
            $this->fire('update_batch_start', array($records));
        }
        foreach ($records as $record) {
            $this->updateRecord($record);
        }
        if (count($records) > 0) {
            $this->fire('update_batch_done', array($records));
        }
    }

}
