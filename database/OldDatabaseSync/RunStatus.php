<?php

namespace OldDatabaseSync;

/**
 * Loads and saves sync data to an array file.
 * File located at app/storage/old_database_sync/run_status
 * Saves last ids and timestamps
 */
class RunStatus implements \ArrayAccess{

    protected $data = [];
    protected $save_file_dir = 'old_database_sync';
    protected $file_name     = 'run_status';

    public function __construct() {

    }

    //
    // Internal getters
    //---------------------------------------------------------------------------------------------
    protected function &getFactory($factory) {
        // this is just a top-level array key
        if(!isset($this->data[$factory])) {
            $this->data[$factory] = [];
        }
        return $this->data[$factory];
    }

    protected function &getDatabase($factory, $database) {
        $factory = &$this->getFactory($factory);
        if(!isset($factory[$database])) {
            $factory[$database] = [];
        }
        return $factory[$database];
    }

    protected function &getMigration($factory, $database, $sync_name) {
        if(!is_scalar($sync_name) || empty($sync_name)) {
            throw new \Exception("illegal offset");
        }
        $database = &$this->getDatabase($factory, $database);
        if(!isset($database[$sync_name])) {
            $database[$sync_name] = [];
        }
        return $database[$sync_name];
    }

    //
    // Public getters / setters
    //---------------------------------------------------------------------------------------------

    public function setLastRun($datetime) {
        $this->data['last_run'] = $datetime;
    }

    public function setLastRecordIdCopied($factory, $database, $sync_name, $recordid) {
        $sync_log_item = &$this->getMigration($factory, $database, $sync_name);
        $sync_log_item['last_copied'] = $recordid;
    }

    public function getLastRecordIdCopied($factory, $database, $sync_name) {
        $sync_log_item = &$this->getMigration($factory, $database, $sync_name);
        if(!isset($sync_log_item['last_copied'])) {
            $sync_log_item['last_copied'] = 0;
        }
        return $sync_log_item['last_copied'];
    }

    public function setLastDateCreated($factory, $database, $sync_name, $datetime) {
        $sync_log_item = &$this->getMigration($factory, $database, $sync_name);
        $sync_log_item['last_date_created'] = $datetime;
    }

    public function getLastDateCreated($factory, $database, $sync_name) {
        $sync_log_item = &$this->getMigration($factory, $database, $sync_name);
        if(!isset($sync_log_item['last_date_created'])) {
            $sync_log_item['last_date_created'] = '0000-00-00 00:00:00';
            return null;
        }
        return $sync_log_item['last_date_created'];
    }

    public function setLastDateUpdated($factory, $database, $sync_name, $datetime) {
        $sync_log_item = &$this->getMigration($factory, $database, $sync_name);
        $sync_log_item['last_date_updated'] = $datetime;
    }

    public function getLastDateUpdated($factory, $database, $sync_name) {
        $sync_log_item = &$this->getMigration($factory, $database, $sync_name);
        if(!isset($sync_log_item['last_date_updated'])) {
            $sync_log_item['last_date_updated'] = '0000-00-00 00:00:00';
            return null;
        }
        return $sync_log_item['last_date_updated'];
    }

    public function clearAll() {
        $this->data = array();
    }

    //
    // Array Access methods
    //---------------------------------------------------------------------------------------------

    /**
     * Get an item from the collection by key.
     *
     * @param  mixed  $key
     * @param  mixed  $default
     * @return mixed
     */
    public function get($key, $default = null) {
        if ($this->offsetExists($key)) {
            return $this->items[$key];
        }

        return value($default);
    }

    public function set($key, $value) {
        $this->data[$key] = $value;
    }

    /**
     * Get the keys of the collection items.
     *
     * @return array
     */
    public function keys() {
        return array_keys($this->data);
    }

    /**
     * Get the collection of items as a plain array.
     *
     * @return array
     */
    public function toArray() {
        return array_map(function($value) {
            return $value instanceof ArrayableInterface ? $value->toArray() : $value;
        }, $this->data);
    }

    /**
     * Determine if an item exists at an offset.
     *
     * @param  mixed  $key
     * @return bool
     */
    public function offsetExists($key) {
        return array_key_exists($key, $this->data);
    }

    /**
     * Get an item at a given offset.
     *
     * @param  mixed  $key
     * @return mixed
     */
    public function offsetGet($key) {
        return $this->data[$key];
    }

    /**
     * Set the item at a given offset.
     *
     * @param  mixed  $key
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($key, $value) {
        if (is_null($key)) {
            $this->data[] = $value;
        } else {
            $this->data[$key] = $value;
        }
    }

    /**
     * Unset the item at a given offset.
     *
     * @param  string  $key
     * @return void
     */
    public function offsetUnset($key) {
        unset($this->data[$key]);
    }

    //
    // File Handling
    //------------------------------------------------------------------------------

    public function saveFilePath() {
        $dir_path = storage_path() . '/' . $this->save_file_dir;
        if (!file_exists($dir_path)) {
            mkdir($dir_path, 0777, true);
        }
        $path = $dir_path . '/' . $this->file_name;
        return $path;
    }

    public function loadFile() {
        if (file_exists($this->saveFilePath())) {
            $data = include $this->saveFilePath();
            if(is_array($data)) {
                $this->data = $data;
            }
            return true;
        }
        return false;
    }

    public function saveFile() {
        file_put_contents($this->saveFilePath(), "<?php\n\nreturn " . var_export($this->toArray(), true) . ";");
    }

}
