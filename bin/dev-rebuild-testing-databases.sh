#!/bin/bash

export APP_ENV=testing

./artisan migrate:reset --env=testing --database=site_content_database
./artisan migrate --env=testing --database=site_content_database --path=tests/_migrations/site_content

./artisan migrate:reset --env=testing --database=solutions_center_database
./artisan migrate --env=testing --database=solutions_center_database --path=tests/_migrations/SolutionsCenter

./artisan migrate:refresh --env=testing
./artisan db:seed --env=testing --class=TestDatabaseSeeder
