<?php

namespace SolutionsCenterConnector\Models;

class LocationType extends \Eloquent {

    protected $connection = 'solutions_center_database';

    protected $table = 'location_types';

}
