<?php

namespace SolutionsCenterConnector\Models;

class Location extends \Eloquent {

    protected $connection = 'solutions_center_database';

    protected $table = 'locations';

    public function locationType() {
        return $this->belongsTo(LocationType::class, 'location_type_id');
    }

}
