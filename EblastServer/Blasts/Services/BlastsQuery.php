<?php

namespace EblastServer\Blasts\Services;

use Illuminate\Foundation\Application;
use EblastServer\Blasts\BlastType;

class BlastsQuery {

    /**
     * @var Illuminate\Foundation\Application
     */
    protected $app;

    // Must pass either date or id. if id is passed, must also pass blast_type
    protected $search_options = [
        'id'             => null, // (int) limit blasts to blast id. requires to also pass blast_type
        'location_id'    => null, // (int) limit blasts to a location id
        'location_group' => null, // (int) limit to a location group
        'date'           => null, // (DateTime|string) limit blasts to a blast_date. Allowed to pass 'today' and 'tomorrow'
        'blast_type'     => null, // (\EblastServer\Blasts\BlastType) limit to blast type
    ];

    protected $blast_types;
    protected $query_builders;

    public function __construct(Application $app, array $search_options) {
        $this->app = $app;
        $this->search_options = [
            'id'             => !empty($search_options['id']) ? (int) $search_options['id'] : null,
            'location_id'    => !empty($search_options['location_id']) ? (int) $search_options['location_id'] : null,
            'location_group' => !empty($search_options['location_group']) ? (int) $search_options['location_group'] : null,
            'date'           => !empty($search_options['date']) ? $search_options['date'] : null,
            'blast_type'     => !empty($search_options['blast_type']) ? $search_options['blast_type'] : null,
        ];
        $this->validate();
    }

    protected function validate() {
        if(!empty($this->search_options['date']) && !($this->search_options['date'] instanceof \DateTime)) {
            throw new InvalidSearchOptionException('date must be instance of DateTime');
        }
        if(!empty($this->search_options['blast_type']) && !($this->search_options['blast_type'] instanceof BlastType)) {
            throw new InvalidSearchOptionException('blast_type must be instance of \EblastServer\Blasts\BlastType');
        }
        if(empty($this->search_options['id']) && empty($this->search_options['date'])) {
            throw new InvalidUserSearchOptionException('Must pass either id or date');
        }
        if(!empty($this->search_options['id']) && empty($this->search_options['blast_type'])) {
            throw new InvalidUserSearchOptionException('If id is passed, must also pass blast_type');
        }
    }

    protected function addBlastDateCriteria($builder) {
        if(empty($this->search_options['date'])) {
            return;
        }
        $builder->addBlastDateCriteria($this->search_options['date']);
    }

    protected function addBlastIdCriteria($builder) {
        if(empty($this->search_options['id'])) {
            return;
        }
        $builder->addBlastIdCriteria($this->search_options['id']);
    }

    protected function addLocationIdCriteria($builder) {
        if(empty($this->search_options['location_id'])) {
            return;
        }
        $builder->addLocationIdCriteria($this->search_options['location_id']);
    }

    protected function addLocationGroupIdCriteria($builder) {
        if(empty($this->search_options['location_group'])) {
            return;
        }
        $builder->addLocationGroupIdCriteria($this->search_options['location_group']);
    }

    public function getBlastTypes() {
        if(isset($this->blast_types)) {
            return $this->blast_types;
        }
        if($this->search_options['blast_type']) {
            $this->blast_types = [$this->search_options['blast_type']->getValue()];
            return $this->blast_types;
        }

        $this->blast_types = $this->app->make('BlastTypeRepositories')->getBlastTypes();
        return $this->blast_types;
    }

    protected function createRepository($blast_type_value) {
        $repository = $this->app->make('BlastTypeRepositories')->repository($blast_type_value);
        return $repository;
    }

    protected function createQueryBuilder($blast_type_value) {
        $query_builder = $this->createRepository($blast_type_value)->getBlastsQueryBuilder();

        $this->addBlastDateCriteria($query_builder);
        $this->addBlastIdCriteria($query_builder);
        $this->addLocationIdCriteria($query_builder);
        $this->addLocationGroupIdCriteria($query_builder);

        return $query_builder;
    }

    public function getQueryBuilders() {
        if(isset($this->query_builders)) {
            return $this->query_builders;
        }
        $this->query_builders = [];
        foreach($this->getBlastTypes() as $blast_type_value) {
            $this->query_builders[] = $this->createQueryBuilder($blast_type_value);
        }
        return $this->query_builders;
    }

    protected function addToCollection($collection, $blasts) {
        return $collection->merge($blasts);
    }

    public function fetchCollection() {
        $collection = new \Illuminate\Support\Collection();
        foreach($this->getQueryBuilders() as $builder) {
            $collection = $this->addToCollection($collection, $builder->get());
        }
        return $collection;
    }

    /**
     * Execute a callback over each item while chunking.
     * @param  callable  $callback
     * @param  integer $count
     * @return null
     */
    public function each($callback, $count = 200) {
        $query_builders = $this->getQueryBuilders();
        foreach($query_builders as $query_builder) {
           $query_builder->each($callback, $count);
        }
    }

    //
    // Modifiers
    //--------------------------------------------------------
    public function addLocationCountField() {
        foreach($this->getQueryBuilders() as $query_builder) {
            $query_builder->addLocationCountField();
        }
    }

    public function explodeByLocationId() {
        foreach($this->getQueryBuilders() as $query_builder) {
            $query_builder->buildLeftJoinLocationsGroups();
        }
    }
}