<?php

namespace EblastServer\Blasts\Services;

use Illuminate\Foundation\Application;
use EblastServer\Blasts\BlastType;

class BlastTypeRepositories {

    /**
     * @var Illuminate\Foundation\Application
     */
    protected $app;

    public $blast_repositories = [
        BlastType::AD            => '\EblastServer\Blasts\Repositories\AdsRepository',
        BlastType::EBLAST_COUPON => '\EblastServer\Blasts\Repositories\EblastCouponsRepository',
        BlastType::CUSTOM_EBLAST => '\EblastServer\Blasts\Repositories\CustomEblastsRepository'
    ];

    public function __construct(Application $app) {
        $this->app = $app;
    }

    public function repository($blast_type_value) {
        if(!isset($this->blast_repositories[$blast_type_value])) {
            throw new \Exception('There is no repository set up for blast_type ' . $blast_type_value);
        }
        $repository = $this->app->make($this->blast_repositories[$blast_type_value]);

        return $repository;
    }

    public function getBlastTypes() {
        return array_keys($this->blast_repositories);
    }

}