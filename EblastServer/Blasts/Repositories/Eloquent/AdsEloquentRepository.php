<?php namespace EblastServer\Blasts\Repositories\Eloquent;

use EblastServer\Blasts\Models\Eloquent\Ad;
use EblastServer\Blasts\Repositories\AdsRepository;

class AdsEloquentRepository extends BlastsEloquentRepository implements AdsRepository {

    protected $joins = [
        'locations_groups' => ['ads_locations_groups', 'ads_locations_groups.ad_id', '=', 'ads.id']
    ];

    public function __construct(Ad $model) {
        $this->model = $model;
    }

}