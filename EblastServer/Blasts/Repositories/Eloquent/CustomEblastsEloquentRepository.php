<?php namespace EblastServer\Blasts\Repositories\Eloquent;

use EblastServer\Blasts\Models\Eloquent\CustomEblast;
use EblastServer\Blasts\Repositories\CustomEblastsRepository;

class CustomEblastsEloquentRepository extends BlastsEloquentRepository implements CustomEblastsRepository {

    protected $joins = [
        'locations_groups' => ['custom_eblast_locations_groups', 'custom_eblast_locations_groups.custom_eblast_id', '=', 'custom_eblasts.id']
    ];

    public function __construct(CustomEblast $model) {
        $this->model = $model;
    }

}