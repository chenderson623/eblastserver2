<?php namespace EblastServer\Blasts\Repositories\Eloquent;

use EblastServer\Blasts\Models\Eloquent\EblastCoupon;
use EblastServer\Blasts\Repositories\EblastCouponsRepository;

class EblastCouponsEloquentRepository extends BlastsEloquentRepository implements EblastCouponsRepository {

    protected $joins = [
        'locations_groups' => ['eblast_coupon_locations_groups', 'eblast_coupon_locations_groups.eblast_coupon_id', '=', 'eblast_coupons.id']
    ];

    public function __construct(EblastCoupon $model) {
        $this->model = $model;
    }

}