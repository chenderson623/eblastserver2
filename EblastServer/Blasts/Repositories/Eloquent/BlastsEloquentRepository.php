<?php namespace EblastServer\Blasts\Repositories\Eloquent;

use EblastServer\Blasts\Repositories\BlastsRepository;
use EblastServer\Common\Repositories\Eloquent\EloquentRepository;
use EblastServer\Blasts\QueryBuilders\Eloquent\BlastsEloquentQueryBuilder;

class BlastsEloquentRepository extends EloquentRepository implements BlastsRepository {

    public function getBlastsQueryBuilder() {
        return new BlastsEloquentQueryBuilder($this->model->query(), $this->joins);
    }

}