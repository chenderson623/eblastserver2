<?php namespace EblastServer\Blasts\Repositories;

use EblastServer\Common\Repositories\RepositoryInterface;

interface BlastsRepository extends RepositoryInterface{

    public function getBlastsQueryBuilder();

}