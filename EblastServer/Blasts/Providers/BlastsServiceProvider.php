<?php

namespace EblastServer\Blasts\Providers;

use Illuminate\Support\ServiceProvider;

use EblastServer\Blasts\Services\BlastTypeRepositories;
use EblastServer\Blasts\Services\BlastsQuery;

class BlastsServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        // Repositories:
        $this->app->bind('EblastServer\Blasts\Repositories\AdsRepository', 'EblastServer\Blasts\Repositories\Eloquent\AdsEloquentRepository');
        $this->app->bind('EblastServer\Blasts\Repositories\CustomEblastsRepository', 'EblastServer\Blasts\Repositories\Eloquent\CustomEblastsEloquentRepository');
        $this->app->bind('EblastServer\Blasts\Repositories\EblastCouponsRepository', 'EblastServer\Blasts\Repositories\Eloquent\EblastCouponsEloquentRepository');


        // Services:
        $this->app->bind('BlastTypeRepositories', BlastTypeRepositories::class);
        $this->app->bind('BlastsQuery', function($app, $search_options){
            return new BlastsQuery($app, $search_options);
        });
    }
}