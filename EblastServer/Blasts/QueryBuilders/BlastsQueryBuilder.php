<?php namespace EblastServer\Blasts\QueryBuilders;

use EblastServer\Common\Repositories\Eloquent\QueryBuilder;
//TODO: what's this?
use EblastServer\Blasts\QueryBuilders\BlastsQueryBuilder;

interface BlastsQueryBuilder {

    public function addBlastDateCriteria(\DateTime $date);
    public function addBlastIdCriteria($id);
    public function addLocationIdCriteria($location_id);
}