<?php namespace EblastServer\Blasts\QueryBuilders\Eloquent;

use EblastServer\Common\Repositories\Eloquent\QueryBuilder;
use EblastServer\Blasts\QueryBuilders\BlastsQueryBuilder;

class BlastsEloquentQueryBuilder extends QueryBuilder implements BlastsQueryBuilder {

    protected $joins; // should have join: ['locations_groups' => ['eblast_coupon_locations_groups', 'eblast_coupon_locations_groups.eblast_coupon_id', '=', 'eblast_coupons.id']]

    public function __construct(\Illuminate\Database\Eloquent\Builder $eloquent_builder, array $joins) {
        $this->eloquent_builder          = $eloquent_builder;
        $this->joins                     = $joins;
        $this->solutions_center_database = config('database.connections.solutions_center_database.database');
    }

    public function addBlastDateCriteria(\DateTime $date) {
        $criteria = new \EblastServer\Blasts\Criteria\Eloquent\BlastDateCriteria($date);
        $this->applyCriteria($criteria);
        return $this;
    }
    public function addBlastIdCriteria($id) {
        $criteria = new \EblastServer\Blasts\Criteria\Eloquent\BlastIdCriteria((int) $id);
        $this->applyCriteria($criteria);
        return $this;
    }
    public function addLocationIdCriteria($location_id) {
        $criteria = new \EblastServer\Blasts\Criteria\Eloquent\LocationIdCriteria((int) $location_id);
        $this->buildLeftJoinLocationsGroups();
        $this->applyCriteria($criteria);
        return $this;
    }

    public function buildLeftJoinLocationsGroups() {
        if(!isset($this->joins['locations_groups'])) {
            throw new \Exception('Need to set locations_groups in joins');
        }
        $join = $this->joins['locations_groups'];
        if($this->hasJoin($join[0])) {
            return;
        }
        $this->eloquent_builder->getQuery()->leftJoin($join[0], $join[1], $join[2], $join[3]);
    }

    public function buildLeftJoinSolutionsCenterLocations() {
        if(!isset($this->joins['locations_groups'])) {
            throw new \Exception('Need to set locations_groups in joins');
        }
        $join = $this->joins['locations_groups'];
        if(!$this->hasJoin($join[0])) {
            throw new \Exception('Need to call buildLeftJoinLocationsGroups first');
        }
        $this->eloquent_builder->getQuery()->leftJoin($this->solutions_center_database . '.locations', $join[0] . '.location_id', '=', $this->solutions_center_database . '.locations.id');
    }

    public function buildLeftJoinSolutionsCenterLocationsToGroups() {
        $this->eloquent_builder->getQuery()->leftJoin($this->solutions_center_database . '.groupables', function($join){
            $join->on($this->solutions_center_database . '.groupables.groupable_id', '=', $this->solutions_center_database . '.locations.id');
            $join->on($this->solutions_center_database . '.groupables.groupable_type', '=', \DB::raw('"Location"'));
        });
        $this->eloquent_builder->getQuery()->leftJoin($this->solutions_center_database . '.groups', $this->solutions_center_database . '.groupables.group_id', '=', $this->solutions_center_database . '.groups.id');
    }

    // Querying by "location group" is querying by location id, joined to a group.
    // This is different from querying group_id in locations_groups table
    public function addLocationGroupIdCriteria($group_id) {
        $criteria = new \EblastServer\Blasts\Criteria\Eloquent\LocationGroupIdCriteria((int) $group_id);
        $this->buildLeftJoinLocationsGroups();
        $this->buildLeftJoinSolutionsCenterLocations();
        $this->buildLeftJoinSolutionsCenterLocationsToGroups();
        $this->applyCriteria($criteria);
        return $this;
    }

    public function addLocationCountField() {
        $this->buildLeftJoinLocationsGroups();
        $join = $this->joins['locations_groups'];
        $this->eloquent_builder->selectRaw('*, count(' . $join[1] . ') as locationsCount');
        $this->eloquent_builder->groupBy($join[3]);
    }
}