<?php namespace EblastServer\Blasts\Criteria\Eloquent;

use EblastServer\Common\Repositories\Criteria;

class LocationGroupIdCriteria extends Criteria {

    public function __construct($group_id) {
        $this->group_id = $group_id;
        $this->database = config('database.connections.solutions_center_database.database');
    }

    public function apply( $query_builder ) {
        $query_builder->where($this->database . '.groups.id', '=', $this->group_id);
        return $query_builder;
    }

}
