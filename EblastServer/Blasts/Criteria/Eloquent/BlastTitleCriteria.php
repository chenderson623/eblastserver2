<?php namespace EblastServer\Blasts\Criteria\Eloquent;

use EblastServer\Common\Repositories\Criteria;

class BlastTitleCriteria extends Criteria {

    public function __construct($text) {
        $this->text = $text;
    }

    public function apply( $query_builder ) {
        $query_builder->where('title', 'LIKE', '%' . $this->text . '%');
        return $query_builder;
    }

}
