<?php namespace EblastServer\Blasts\Criteria\Eloquent;

use EblastServer\Common\Repositories\Criteria;
use DateTime;

class BlastDateCriteria extends Criteria {

    public function __construct(DateTime $date) {
        $this->date = $date;
    }

    public function apply( $query_builder ) {
        $query_builder->where('blast_date', '=', $this->date->format('Y-m-d'));
        return $query_builder;
    }

}
