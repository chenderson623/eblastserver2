<?php namespace EblastServer\Blasts;

use MabeEnum\Enum;

class BlastType extends Enum {
    const AD                  = 1;
    const EBLAST_COUPON       = 2;
    const CUSTOM_EBLAST       = 3;

    static public $titles = [
        1 => 'Ad',
        2 => 'eBlast Coupon',
        3 => 'Custom eBlast',
    ];

    public function getTitle() {
        if(!isset(static::$titles[$this->getValue()])) {
            throw new \Exception('No title is set for ' . $this->getName());
        }
        return static::$titles[$this->getValue()];
    }
}