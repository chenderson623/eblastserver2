<?php namespace EblastServer\Blasts\Models\Eloquent;

use EblastServer\Blasts\BlastType;

abstract class BaseBlastModel extends \Eloquent {

    /**
     * @return BlastType
     */
    abstract public function getType();

    public function getTypeTitle() {
        return $this->getType()->getTitle();
    }

    abstract public function locations();
    abstract public function locationsIdList();
}