<?php

namespace EblastServer\Blasts\Models\Eloquent;

use EblastServer\Blasts\BlastType;
use SolutionsCenterConnector\Models\Location as SolutionsCenterLocation;

class Ad extends BaseBlastModel {

    protected $connection = 'site_content_database';

    protected $table    = 'ads';
    protected $fillable = ['title', 'ad_template_id', 'description', 'labels', 'pages', 'blast_date', 'display_start_date', 'display_end_date', 'offer_start_date', 'offer_end_date', 'upload_type', 'dir_name', 'request_id', 'archived'];

    /**
     * @return BlastType
     */
    public function getType() {
        return BlastType::AD();
    }

    public function locations() {
        return $this->belongsToMany(SolutionsCenterLocation::class, $this->getConnection()->getDatabaseName() . '.ads_locations_groups');
    }

    public function locationsIdList() {
        return \DB::connection($this->connection)->table('ads_locations_groups')->where('ad_id', '=', $this->id)->lists('location_id');
    }

}