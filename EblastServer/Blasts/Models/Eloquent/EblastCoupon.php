<?php

namespace EblastServer\Blasts\Models\Eloquent;

use EblastServer\Blasts\BlastType;
use SolutionsCenterConnector\Models\Location as SolutionsCenterLocation;

class EblastCoupon extends BaseBlastModel {

    protected $table    = 'eblast_coupons';
    protected $fillable = ['name', 'blast_date'];

    /**
     * @return BlastType
     */
    public function getType() {
        return BlastType::EBLAST_COUPON();
    }

    public function locations() {
        return $this->belongsToMany(SolutionsCenterLocation::class, $this->getConnection()->getDatabaseName() . '.eblast_coupon_locations_groups');
    }

    public function locationsIdList() {
        return \DB::connection($this->connection)->table('eblast_coupon_locations_groups')->where('ad_id', '=', $this->id)->lists('location_id');
    }

}