<?php

namespace EblastServer\Blasts\Models\Eloquent;

use EblastServer\Blasts\BlastType;
use SolutionsCenterConnector\Models\Location as SolutionsCenterLocation;

class CustomEblast extends BaseBlastModel {

    protected $table    = 'custom_eblasts';
    protected $fillable = ['email_type', 'send_date', 'send_time', 'site_id', 'blast_type', 'blast_id', 'auto_recipients', 'auto_send', 'sent', 'failed', 'resend', 'total_sent', 'total_opened', 'total_unique_opened', 'total_clicked', 'total_unique_clicked', 'total_failed', 'total_resent', 'blast_template', 'smtp_provider_account_id', 'email_from_name'];

    /**
     * @return BlastType
     */
    public function getType() {
        return BlastType::CUSTOM_EBLAST();
    }

    public function locations() {
        return $this->belongsToMany(SolutionsCenterLocation::class, $this->getConnection()->getDatabaseName() . '.custom_eblast_locations_groups');
    }

    public function locationsIdList() {
        return \DB::connection($this->connection)->table('custom_eblast_locations_groups')->where('ad_id', '=', $this->id)->lists('location_id');
    }

}