<?php namespace EblastServer\Subscribers;

use Illuminate\Support\ServiceProvider;

class SubscribersServiceProvider extends ServiceProvider {

    protected $defer = true;

    public function register() {
// TODO: this works:
        $this->app->bind(Repositories\SubscriberProfilesRepository::class, Repositories\Eloquent\SubscriberProfilesEloquentRepository::class);

        $this->app->bind(Repositories\SubscriberProfilesRepositoryXX::class, function($app)
        {
            $repository =  new Repositories\Eloquent\SubscriberProfilesEloquentRepository(
                new Eloquent\SubscriberProfile
            );



/*
            if( $app['config']->get('is_admin', false) == false )
            {
                $article = new CacheDecorator(
                    $article,
                    new LaravelCache($app['cache'], 'articles', 10)
                );
            }
*/
            return $repository;

        });
    }

    public function provides() {
        return [
            Repositories\SubscriberProfiles::class
        ];
    }
}