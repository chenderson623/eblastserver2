<?php

class SubscribersService {


    public function __construct(SubscriberRepositoryInterface $subscriber_reposititory, SubscriberQueryBuilderInterface $query_builder) {

    }

    public function findSubscriptionsForLoction($location) { // no type hinting here - will be taken care of in SubscriptionsForLocation
        $this->query_builder->newQueryBuilder();

        $criteria = $this->query_builder->getCriteria(
            new ActiveSubscriberCriteria(),
            new SubscriptionsForLocationCriteria($location)
        );

        // OR:
        $criteria = $this->query_builder->getCriteria( // No dependencies:
            $app->make('ActiveSunscriberCriteria'),
            $app->make('SubscriptionsForLocationCriteria', [$location])
        );

        // OR:
        $criteria = $query_builder->makeCriteria([  // can be mixed with above
            $query_builder->activeSubscribers(),   // returns Criteria object
            $query_builder->subscriptionsForLocation($location)
        ]);
    }

}