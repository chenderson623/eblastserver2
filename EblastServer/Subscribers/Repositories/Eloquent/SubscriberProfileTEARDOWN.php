<?php namespace App\Models;

use App\Libraries\EmailAddresses\EmailAddress;

class SubscriberProfile extends \Eloquent {

    protected $table    = 'subscriber_profiles';
    protected $fillable = ['email', 'domain', 'first_name', 'last_name', 'telephone', 'active', 'date_added', 'date_removed', 'hash', 'remote_checked', 'remote_valid'];

    protected static $hash_salt = '08a648be28c84adcbbb006859528049d';

    protected $subscriber_subscriptions_set;

    //
    // Finders
    //----------------------------------------------------------------------------

    public static function findByEmail($email) {
        return self::where('email', '=', $email)->first();
    }

    public static function NumberOfSubscribers($active = false) {
        return self::where('active', '=', 1)->count();
    }

    public static function findByEmailAndHash($email, $hash) {
        return self::where('email', '=', $email)->where('hash', '=', $hash)->first();
    }

    public static function findByHash($hash) {
        return self::where('hash', '=', $hash)->first();
    }

    public function findNeedRemoteCheck($limit = 50) {
        return self::where('remote_checked', '=', 0)->take($limit)->get();
    }

    //
    // Creators
    //----------------------------------------------------------------------------

    public static function createNewProfile(EmailAddress $email_address, $data) {
        $backtrace  = debug_backtrace();
        $valid_call = false;
        if (isset($backtrace[1])) {
            if (isset($backtrace[1]['object'])) {
                if ($backtrace[1]['object'] instanceof \App\Commands\AbstractSubscriberCommand) {
                    $valid_call = true;
                }
            }
        }

        if (!$valid_call) {
            throw new \Exception("Must call this method from a command object");
        }

        $data['email'] = $email_address->getEmailAddress();
        $validate      = self::validateData($data);
        if(!$email_address->isValid()) {
            $validate['errors'][] = 'Invalid email address';
        }

        if (count($validate['errors']) > 0) {
            throw new Exception("Data has errors. Call validateData before calling this");
        }

        $data = $validate['data'];

        $email                = $data['email'];
        $data['date_added']   = isset($data['date_added']) ? $data['date_added'] : strftime('%Y-%m-%d');
        $data['date_removed'] = '2099-12-31';
        $data['domain']       = $email_address->getDomain();

        //double check:
        $existing = self::findByEmail($email);

        if (!empty($existing)) {
            throw new Exception("Email $email already exists. Call findProfileArrayByEmail before calling this");
        }

        $new_profile = self::create($data);

        $new_profile->hash = self::getHash($new_profile);
        $new_profile->save();

        return $new_profile;
    }

    //
    // Instance Methods
    //----------------------------------------------------------------------------

    public static function getHash(SubscriberProfile $profile) {
        return md5($profile->email . $profile->id . self::$hash_salt);
    }

    /**
     * @return SubscriberHistorySet
     */
    public function getHistories() {
        if (!isset($this->histories)) {
            $this->histories = SubscriberHistoryItem::getHistoriesForProfile($this);
        }
        return $this->histories;
    }

    /**
     * @return SubscriberSubscriptionsSet
     */
    public function getSubscriptions($active_only = false) {
        if (!isset($this->subscriber_subscriptions_set)) {
            $this->subscriber_subscriptions_set = SubscriberSubscription::findSubscriptionsForProfile($this, $active_only);
        }
        return $this->subscriber_subscriptions_set;
    }

    public function getName() {
        return trim($this->first_name . ' ' . $this->last_name);
    }

    public function getEmailAddress() {
        return $this->email;
    }

    /**
     * @return \App\Libraries\EmailAddress
     */
    public function getEmailAddressObject() {
        $email_address = new \App\Libraries\EmailAddress($this->getEmailAddress());
        $email_address->setName($this->getName());
        return $email_address;
    }

    public function getFullEmail() {
        $name = $this->getName();
        if (!empty($name)) {
            return "$name<{$this->getEmailAddress()}>";
        } else {
            return $this->getEmailAddress();
        }
    }

    public function setEmail($email) {
        $old_email = $this->email;

        if ($email != $old_email) {

            $email_parts = explode('@', $email);

            $this->email = $email;
            $this->domain = $email_parts[1];

            $this->save();

            //create history
            $histories                   = $this->getHistories();
            $history_data['description'] = "Email address changed from $old_email to $email";
            $histories->updateSubscriber($history_data);
            return true;
        }
        return false;
    }

    public function isSubscribedToLocation($location_id) {
        $subscriber_subscriptions = $this->getSubscriptions();
        return $subscriber_subscriptions->findForLocationId($location_id) !== false;
    }

    public function isActive() {
        return $this->active == 1;
    }

    public function deactivate() {
        if ($this->active == 0) {
            //already deactivated
            return;
        }

        $this->active = 0;
        $this->date_removed = strftime('%Y-%m-%d');
    }

    public function activate() {
        if ($this->active == 1) {
            //already active
            return;
        }

        $this->active = 1;
        $this->date_removed = '2099-12-31';
    }

    public function getId() {
        return $this->id;
    }

    public function get($key, $default = null) {
        $value = $this->getAttribute($key);
        if(empty($value)) {
            $value = $default;
        }
        return $value;
    }

}
