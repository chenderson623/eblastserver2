<?php namespace EblastServer\Subscribers\Repositories\Eloquent;

use Validator;

class AbstractSubscribersEloquentRepository {

    public function findById($id) {
        return $this->model->find($id);
    }

    public function findAll() {
        $model_class = $this->model_class;
        return $model_class::orderBy('title', 'asc')->get();
    }

    public function paginate($limit = null) {
        $model_class = $this->model_class;
        return $model_class::paginate($limit);
    }

    public function store($data) {
        $this->validate($data);
        $model_class = $this->model_class;
        return $model_class::create($data);
    }

    public function update($id, $data) {
        $gallery_item = $this->findById($id);
        $gallery_item->fill($data);
        $this->validate($gallery_item->toArray());
        $gallery_item->save();
        return $gallery_item;
    }

    public function destroy($id) {
        $gallery_item = $this->findById($id);
        $gallery_item->delete();
        return true;
    }

    public function validate($data) {
        $model_class = $this->model_class;
        $validator = Validator::make($data, $model_class::$rules);
        if ($validator->fails()) {throw new \ValidationException($validator);
        }

        return true;
    }

    public function instance($data = array()) {
        $model_class = $this->model_class;
        return new $model_class($data);
    }

}