<?php namespace EblastServer\Subscribers\Repositories\Eloquent;

use EblastServer\Subscribers\Models\Eloquent\SubscriberProfile;
use EblastServer\Subscribers\Repositories\SubscriberProfilesRepository;
use EblastServer\Subscribers\Repositories\Eloquent\AbstractSubscribersEloquentRepository;

class SubscriberProfilesEloquentRepository extends AbstractSubscribersEloquentRepository implements SubscriberProfilesRepository {

    public function __construct(SubscriberProfile $model) {
        $this->model = $model;
    }

}