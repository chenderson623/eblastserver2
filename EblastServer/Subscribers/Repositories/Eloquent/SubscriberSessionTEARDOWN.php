<?php namespace App\Models;

use App\Libraries\Reporter;

class SubscriberSession extends \Eloquent {

    protected $table    = 'subscriber_sessions';
    protected $fillable = ['session_type', 'subscriber_id', 'user', 'ip_address', 'user_agent', 'script', 'web_session', 'referer'];

    //
    // Finders
    //------------------------------------------------------------------------------------------

    public static function lookupWebSession($ip_address, $web_session) {
        return self::where('ip_address', '=', $ip_address)->where('web_session','=',$web_session)->orderBy('id', 'DESC')->first();
    }

    //
    // Creators
    //------------------------------------------------------------------------------------------

    public static function createFromReporter(Reporter $reporter) {

        $session_type      = '';
        $existing_session = null;
        switch ($reporter->getUser()) {
            case 'CLI':
                $session_type     = 'cli';
                break;
            case 'web':
                $session_type     = 'web';
                $existing_session = self::lookupWebSession($reporter->getIpAddress(), $reporter->getSessionId());
        }

        if ($existing_session !== null) {
            return $existing_session;
        }

        $insert_array = array(
            "session_type"  => $session_type,
            "subscriber_id" => '',
            "user"          => $reporter->getUser(),
            "ip_address"    => $reporter->getIpAddress(),
            "user_agent"    => $reporter->getUserAgent(),
            "script"        => $reporter->getScript(),
            "web_session"   => $reporter->getSessionId(),
            "referer"       => $reporter->getReferer(),
        );

        $subscriber_session = self::create($insert_array);
        return $subscriber_session;
    }

    //
    // For backward compatability:
    //------------------------------------------------------------------------------------------
    public function getId() {
        return $this->id;
    }

    public function get($key, $default = null) {
        $value = $this->getAttribute($key);
        if(empty($value)) {
            $value = $default;
        }
        return $value;
    }

}
