<?php namespace App\Models;

use App\Models\SolutionsCenter\Location;

class SubscriberSubscription extends \Eloquent {

    protected $table    = 'subscriber_subscriptions';
    protected $fillable = ['subscriber_id', 'location_id', 'hash', 'active', 'ad_blast', 'eblast', 'newsletters', 'recipes', 'store_blasts', 'store_management', 'date_added', 'date_removed'];

    protected static $hash_salt = '08a648be28c84adcbbb006859528049d';

    protected static $mailing_lists_choices = array(
        'ad_blast'    => 'Weekly Ad',
        'eblast'      => 'Eblast',
        'newsletters' => 'Newsletters',
        'recipes'     => 'Recipes',
        'store_blasts'     => 'Store Blasts',
        'store_management' => 'Store Management Notification'
    );

    public function getMailingListsChoices() {
        return self::$mailing_lists_choices;
    }

    public function getMailingListsChoicesString() {
        return implode(', ', $this->getMailingListsChoices());
    }

    public static function getMailingListChoiceFromLabel($label) {
        $key = array_search($label, self::$mailing_lists_choices);
        return $key;
    }


    //
    // Eloquent overrides
    //------------------------------------------------------------------------------------------

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Database\Eloquent\Collection
     */
    //public function newCollection(array $models = [])
    //{
    //    return new Collection($models);
    //}

    //
    // Query Builders
    //--------------------------------------------------------------------

    protected static function buildSiteSubscriptionType($builder, $site_id = null, $type = null, $include_inactive = false) {
        if ($site_id !== null) {
            $builder->where('location_id', '=', $site_id);
        }
        if ($include_inactive === false) {
            $builder->where('subscriber_subscriptions.active', '=', '1');
        }

        if($type !== null) {
            self::buildForEmailType($type, $builder);
        }
        return $builder;
    }

    public static function buildForEmailType($type, $builder) {
        $type_field = $type === null ? null : self::getMailingListChoiceFromLabel($type);

        if ($type_field === false) {
            throw new Exception("Type [$type] is not a know mail type.");
        }

        if ($type_field !== null) {
            $builder->where($type_field, '=', '1');
        }
    }

    public static function buildLeftJoinSubscriberProfile($builder) {
        $builder->leftJoin('subscriber_profiles', 'subscriber_profiles.id', '=', 'subscriber_subscriptions.subscriber_id');
        //$builder->select('subscriber_profiles.*');
    }


    //
    // Finders
    //----------------------------------------------------------------------------

    public static function findForLocations($location_id_array) {
        return self::where('location_id', 'IN', $location_id_array)->get();
    }

    public static function findSubscriberAndLocationId(SubscriberProfile $profile, $location_id) {
        return self::where('subscriber_id','=',$profile->id)->where('location_id','=',$location_id)->where('active','=',1)->first();
    }

    public static function findSubscriptionsForProfile(SubscriberProfile $profile, $active_only = false) {
        $builder = self::where('subscriber_id','=',$profile->id);

        if ($active_only === true) {
            $builder->where('active','=',1);
        }
        $subscriptions = $builder->get();
        return new SubscriberSubscriptionsSet($profile, $subscriptions);
    }

    public static function chunkSubscribersForSiteAndSubscriptionType($site_id, $subscription_type, $include_inactive, $chunk_qty, $callback) {
        $builder = \DB::table('subscriber_subscriptions');
        $builder = self::buildSiteSubscriptionType($builder, $site_id, $subscription_type, $include_inactive);
        self::buildLeftJoinSubscriberProfile($builder);
        $builder->chunk($chunk_qty, $callback);
    }

    //
    // Creators
    //----------------------------------------------------------------------------

    public static function createNewSubscription(SubscriberProfile $profile, Location $location, $data = array()) {
        $existing = self::findSubscriberAndLocationId($profile, $location->id);

        if (!empty($existing)) {
            throw new Exception("Subscription exists for subscriber profile {$profile->id} and location {$location->id}");
        }

        $data = array(
            'subscriber_id'    => $profile->id,
            'location_id'      => $location->id,
            'active'           => 1,
            'ad_blast'         => (isset($data['ad_blast'])) ? $data['ad_blast'] : 1,
            'eblast'           => (isset($data['eblast'])) ? $data['eblast'] : 1,
            'newsletters'      => (isset($data['newsletters'])) ? $data['newsletters'] : 0,
            'recipes'          => (isset($data['recipes'])) ? $data['recipes'] : 0,
            'store_blasts'     => (isset($data['store_blasts'])) ? $data['store_blasts'] : 0,
            'store_management' => (isset($data['store_management'])) ? $data['store_management'] : 0,
            'date_added'       => (isset($data['date_added'])) ? $data['date_added'] : strftime('%Y-%m-%d'),
            'date_removed'     => '2099-12-31'
        );

        $subscription       = self::create($data);
        $subscription->hash = self::getHash($profile, $subscription);
        $subscription->save();

        return $subscription;
    }

    //
    // Instance Methods
    //----------------------------------------------------------------------------
    public static function getHash(SubscriberProfile $profile, SubscriberSubscription $subscription) {
        return md5($profile->id . $subscription->location_id . $subscription->id . self::$hash_salt);
    }

    public function getSubscriberId() {
        return $this->subscriber_id;
    }

    public function getLocationId() {
        return $this->location_id;
    }

    protected $profile;
    public function getProfile() {
        if (!isset($this->profile)) {
            $this->profile = SubscriberProfile::find($this->subscriber_id);
        }
        return $this->profile;
    }

    protected $location;
    public function getLocation() {
        if (!isset($this->location)) {
            // 2015-11-08 changed this: $this->location = \App\Libraries\SolutionsCenter\LocationLoader::LocationOnly($this->location_id);
            $this->location=\App\Models\SolutionsCenter\Location::find($this->location_id);
        }
        return $this->location;
    }

    public function getOptionsArray() {
        $options         = array();
        $options_choices = array('ad_blast', 'eblast', 'newsletters', 'recipes', 'store_blasts', 'store_management');
        foreach ($options_choices as $choice) {
            if ($this->getAttribute($choice) == 1) {
                $options[] = $choice;
            }
        }
        return $options;
    }

    public function getOptionLabelsArray() {
        $options         = array();
        foreach ($this->getOptionsArray() as $choice) {
            $options[] = self::$key_labels[$choice];
        }
        return $options;
    }

    public function hasEblast() {
        return $this->eblast == 1;
    }

    public function hasAdblast() {
        return $this->ad_blast == 1;
    }

    public function hasNewsletters() {
        return $this->newsletters == 1;
    }

    public function hasRecipes() {
        return $this->recipes == 1;
    }

    public function enableEblast() {
        $this->eblast = 1;
    }

    public function disableEblast() {
        $this->eblast = 0;
    }

    public function enableAdblast() {
        $this->ad_blast = 1;
    }

    public function disableAdblast() {
        $this->ad_blast = 0;
    }

    public function enableNewsletters() {
        $this->newsletters = 1;
    }

    public function disableNewsletters() {
        $this->newsletters = 0;
    }

    public function enableRecipes() {
        $this->recipes = 1;
    }

    public function disableRecipes() {
        $this->recipes = 0;
    }

    public function enableAll() {
        $this->enableAdblast();
        $this->enableEblast();
        $this->enableNewsletters();
        $this->enableRecipes();
    }

    public function disableAll() {
        $this->disableAdblast();
        $this->disableEblast();
        $this->disableNewsletters();
        $this->disableRecipes();
    }

    public function deactivate() {
        if ($this->active == 0) {
            //already deactivated
            return;
        }

        $this->active = 0;
        $this->date_removed = strftime('%Y-%m-%d');
    }

    public function activate() {
        if ($this->active == 1) {
            //already active
            return;
        }
        $this->active = 1;
        $this->date_removed = '2099-12-31';
    }

    public function isActive() {
        return $this->active == 1;
    }


    //
    // For backward compatability:
    //------------------------------------------------------------------------------------------
    public function getId() {
        return $this->id;
    }

    public function get($key, $default = null) {
        $value = $this->getAttribute($key);
        if(empty($value)) {
            $value = $default;
        }
        return $value;
    }

}
