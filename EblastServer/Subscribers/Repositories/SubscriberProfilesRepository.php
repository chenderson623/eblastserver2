<?php namespace EblastServer\Subscribers\Repositories;

interface SubscriberProfilesRepository {

    public function findById($id);
    public function findAll();
    public function paginate($limit = null);
    public function instance();

}