<?php namespace EblastServer\Subscribers\Models\Eloquent;

class SubscriberHistoryItem extends \Eloquent {

    protected $table    = 'subscriber_histories';
    protected $fillable = ['subscriber_session_id', 'subscriber_id', 'location_id', 'event', 'event_code', 'event_datetime', 'description', 'remote_code', 'data_json', 'undo_json', 'undone', 'parent_history_id'];


}
