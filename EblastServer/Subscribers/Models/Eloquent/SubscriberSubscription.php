<?php namespace EblastServer\Subscribers\Models\Eloquent;

class SubscriberSubscription extends \Eloquent {

    protected $table    = 'subscriber_subscriptions';
    protected $fillable = ['subscriber_id', 'location_id', 'hash', 'active', 'ad_blast', 'eblast', 'newsletters', 'recipes', 'store_blasts', 'store_management', 'date_added', 'date_removed'];

    protected static $hash_salt = '08a648be28c84adcbbb006859528049d';


}
