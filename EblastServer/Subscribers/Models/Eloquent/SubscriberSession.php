<?php namespace EblastServer\Subscribers\Models\Eloquent;

class SubscriberSession extends \Eloquent {

    protected $table    = 'subscriber_sessions';
    protected $fillable = ['session_type', 'subscriber_id', 'user', 'ip_address', 'user_agent', 'script', 'web_session', 'referer'];

}
