<?php namespace EblastServer\Subscribers\Models\Eloquent;

class SubscriberProfile extends \Eloquent {

    protected $table    = 'subscriber_profiles';
    protected $fillable = ['email', 'domain', 'first_name', 'last_name', 'telephone', 'active', 'date_added', 'date_removed', 'hash', 'remote_checked', 'remote_valid'];
//TODO: put in config
    protected static $hash_salt = '08a648be28c84adcbbb006859528049d';


}
