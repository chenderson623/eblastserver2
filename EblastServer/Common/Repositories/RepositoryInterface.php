<?php namespace EblastServer\Common\Repositories;

interface RepositoryInterface {

    public function find($id);
    public function all();

    //? public function queryCriteria( $array_of_criteria);
    //? public function getQueryBuilder( $array_of_criteria);

}

