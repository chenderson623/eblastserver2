<?php namespace EblastServer\Common\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use EblastServer\Common\Repositories\Criteria;

class QueryBuilder {

    /**
     * @var Illuminate\Database\Eloquent\Builder
     */
    protected $eloquent_builder;

    public function __construct(Builder $eloquent_builder) {
        $this->eloquent_builder = $eloquent_builder;
    }

    /**
     * @param  Criteria $criteria
     * @return QueryBuilder
     */
    public function  applyCriteria(Criteria $criteria) {
        $criteria->apply($this->eloquent_builder);
        return $this->eloquent_builder;
    }

    public function hasJoin($table) {
        $joins = $this->eloquent_builder->getQuery()->joins;
        if(!is_array($joins)) {
            return false;
        }
        foreach($joins as $join) {
            if($join->table === $table) {
                return true;
            }
        }
        return false;
    }

    // Decorator for Illuminate\Database\Eloquent\Builder
    public function __call($method, $args) {
        if (!method_exists($this->eloquent_builder, $method)) {
            throw new \Exception("Undefined method $method attempt in the Eloquent Builder class.");
        }
        return call_user_func_array(array($this->eloquent_builder, $method), $args);
    }
}