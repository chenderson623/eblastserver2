<?php namespace EblastServer\Common\Repositories\Eloquent;

use EblastServer\Common\Repositories\RepositoryInterface;

class EloquentRepository implements RepositoryInterface {

    static protected $query_builder_class = \EblastServer\Common\Repositories\Eloquent\QueryBuilder::class;

    public function find($id) {
        return $this->model->find($id);
    }

    public function fetchCriteria($criteria_array) {
        $builder = $this->getQueryBuilderWithCriteria($criteria_array);
        return $builder->get();
    }

    public function all() {
        $model_class = $this->model_class;
        return $model_class::query()->get();
    }

    public function paginate($limit = null) {
        $model_class = $this->model_class;
        return $model_class::paginate($limit);
    }

    /**
     * @return EblastServer\Common\Repositories\Eloquent\QueryBuilder
     */
    public function getQueryBuilder() {
        return new static::$query_builder_class($this->model->query());
    }

    public function getQueryBuilderWithCriteria($criteria_array) {
        if(!is_array($criteria_array)) {
            $criteria_array = [ $criteria_array ];
        }
        $builder = $this->getQueryBuilder();
        foreach($criteria_array as $criteria) {
            $builder->applyCriteria($criteria);
        }
        return $builder;
    }

}