<?php namespace EblastServer\Common\Repositories;

abstract class Criteria {

    public abstract function apply($query_builder);
}