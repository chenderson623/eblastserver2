<?php namespace EblastServer\BlastEmail\Models\Eloquent;

class BlastEmailTrackingEvent extends \Eloquent {

    protected $table    = 'blast_email_tracking_events';
    protected $fillable = ['blast_recipient_id','event','event_link','opened','clicked','ftf','unsubscribe','spam_report','bounce','failed','description','user_agent','ip_address'];


}
