<?php namespace EblastServer\BlastEmail\Models\Eloquent;

class BlastMidTrackingLog extends \Eloquent {

    const UPDATED_AT = null;

    protected $table    = 'blast_mid_tracking_log';
    protected $fillable = ['blast_type', 'blast_id', 'location_id', 'subscriber_id', 'tracking_type', 'user_agent'];

}