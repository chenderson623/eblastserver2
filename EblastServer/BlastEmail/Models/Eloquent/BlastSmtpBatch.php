<?php namespace EblastServer\BlastEmail\Models\Eloquent;

class BlastSmtpBatch extends \Eloquent {

    protected $table    = 'blast_smtp_batches';
    protected $fillable = ['batch_name','sends','sent_datetime'];


}
