<?php namespace EblastServer\BlastEmail\Models\Eloquent;

class BlastSmtpSend extends \Eloquent {

    protected $table    = 'blast_smtp_sends';
    protected $fillable = ['blast_smtp_batch_id','blast_email_id','recipients','sent_datetime'];


}
