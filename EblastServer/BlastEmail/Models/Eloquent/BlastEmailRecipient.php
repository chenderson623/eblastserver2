<?php namespace EblastServer\BlastEmail\Models\Eloquent;

class BlastEmailRecipient extends \Eloquent {

    protected $table    = 'blast_email_recipients';
    protected $fillable = ['blast_email_id','subscriber_id','email_address','contact_id','recipient_type','sub_vars'];


}
