<?php namespace EblastServer\BlastEmail\Models\Eloquent;

use SolutionsCenterConnector\Models\Location as SolutionsCenterLocation;

class BlastEmail extends \Illuminate\Database\Eloquent\Model {

    protected $table    = 'blast_emails';
    protected $fillable = [
        'email_type',
        'send_date',
        'send_time',
        'location_id',
        'blast_type',
        'blast_id',
        'auto_send',
        'production',
        'sent',
        'failed',
        'total_sent',
        'total_opened',
        'total_unique_opened',
        'total_clicked',
        'total_unique_clicked',
        'total_failed',
        'recipient_strategy',
        'send_strategy',
        'mailer_overrides',
        'last_message'];

    public function location() {
        return $this->belongsTo(SolutionsCenterLocation::class, 'location_id');
    }

}
