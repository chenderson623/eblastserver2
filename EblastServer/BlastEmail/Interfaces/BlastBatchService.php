<?php namespace App\EblastServer\Blasts\Interfaces;

interface BlastBatchService {
    public function config(array $values);
    public function getConfig();
    public function getContainer();
    public function getBlastBatch();
    public function getNextSmtpSend();
}