<?php namespace EblastServer\BlastEmail;

use MabeEnum\Enum;

class BlastEmailType extends Enum {
    const MailSubscribers      = 1;
    const Preview              = 2;
    const MailManualRecipients = 3;
    const FTF                  = 4;
    const LocationOptIn        = 5;

    static public $titles = [
        1 => 'MailSubscribers',
        2 => 'Preview',
        3 => 'MailManualRecipients',
        4 => 'FTF',
        5 => 'LocationOptIn',
    ];

    public function getTitle() {
        if(!isset(static::$titles[$this->getValue()])) {
            throw new \Exception('No title is set for ' . $this->getName());
        }
        return static::$titles[$this->getValue()];
    }
}




