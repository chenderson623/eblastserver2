<?php namespace EblastServer\BlastEmail\Criteria\Eloquent;

use EblastServer\Common\Repositories\Criteria;

class LocationIdCriteria extends Criteria {

    public function __construct($location_id) {
        $this->location_id = $location_id;
    }

    public function apply( $query_builder ) {
        $query_builder->where('location_id', '=', $this->location_id);
        return $query_builder;
    }

}
