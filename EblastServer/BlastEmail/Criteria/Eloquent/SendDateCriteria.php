<?php namespace EblastServer\BlastEmail\Criteria\Eloquent;

use EblastServer\Common\Repositories\Criteria;
use DateTime;

class SendDateCriteria extends Criteria {

    public function __construct(DateTime $date) {
        $this->date = $date;
    }

    public function apply( $query_builder ) {
        $query_builder->where('send_date', '=', $this->date->format('Y-m-d'));
        return $query_builder;
    }

}
