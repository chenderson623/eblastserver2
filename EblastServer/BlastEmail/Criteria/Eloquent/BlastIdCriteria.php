<?php namespace EblastServer\BlastEmail\Criteria\Eloquent;

use EblastServer\Common\Repositories\Criteria;
use DateTime;

class BlastIdCriteria extends Criteria {

    public function __construct($id) {
        $this->id = $id;
    }

    public function apply( $query_builder ) {
        $query_builder->where('blast_id', '=', $this->id);
        return $query_builder;
    }

}
