<?php namespace EblastServer\BlastEmail\Repositories;

use EblastServer\Common\Repositories\RepositoryInterface;

interface BlastEmailsRepository extends RepositoryInterface{

    public function getBlastEmailsQueryBuilder();

}