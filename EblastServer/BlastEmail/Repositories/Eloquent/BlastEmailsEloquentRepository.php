<?php namespace EblastServer\BlastEmail\Repositories\Eloquent;

use EblastServer\BlastEmail\Repositories\BlastEmailsRepository;
use EblastServer\Common\Repositories\Eloquent\EloquentRepository;
use EblastServer\BlastEmail\QueryBuilders\Eloquent\BlastEmailsEloquentQueryBuilder;
use EblastServer\BlastEmail\Models\Eloquent\BlastEmail;

class BlastEmailsEloquentRepository extends EloquentRepository implements BlastEmailsRepository {

    protected $joins = [];

    public function __construct(BlastEmail $model) {
        $this->model = $model;
    }

    public function getBlastEmailsQueryBuilder() {
        return new BlastEmailsEloquentQueryBuilder($this->model->query(), $this->joins);
    }

}