<?php namespace EblastServer\BlastEmail\QueryBuilders;

use EblastServer\Common\Repositories\Eloquent\QueryBuilder;

interface BlastEmailsQueryBuilder {

    public function addSendDateCriteria(\DateTime $date);
    public function addBlastIdCriteria($id);
    public function addLocationIdCriteria($location_id);
}