<?php namespace EblastServer\BlastEmail\QueryBuilders\Eloquent;

use EblastServer\Common\Repositories\Eloquent\QueryBuilder;
use EblastServer\BlastEmail\QueryBuilders\BlastEmailsQueryBuilder;

class BlastEmailsEloquentQueryBuilder extends QueryBuilder implements BlastEmailsQueryBuilder {

    public function __construct(\Illuminate\Database\Eloquent\Builder $eloquent_builder) {
        $this->eloquent_builder          = $eloquent_builder;
    }

    public function addSendDateCriteria(\DateTime $date) {
        $criteria = new \EblastServer\BlastEmail\Criteria\Eloquent\SendDateCriteria($date);
        $this->applyCriteria($criteria);
        return $this;
    }
    public function addBlastIdCriteria($id) {
        $criteria = new \EblastServer\BlastEmail\Criteria\Eloquent\BlastIdCriteria((int) $id);
        $this->applyCriteria($criteria);
        return $this;
    }
    public function addLocationIdCriteria($location_id) {
        $criteria = new \EblastServer\BlastEmail\Criteria\Eloquent\LocationIdCriteria((int) $location_id);
        $this->applyCriteria($criteria);
        return $this;
    }

}