
Blast types should always be an enum, controlled by: /EblastServer/Blasts/BlastType.php


Access Blasts through BlastsFactory:

    search_options is an array with the following properties:
    Must pass either date or id. if id is passed, must also pass blast_type
    $search_options = [
        'id'             => null, // (int) limit blasts to blast id. requires to also pass blast_type
        'location_id'    => null, // (int) limit blasts to a location id
        'location_group' => null, // (int) limit to a location group
        'date'           => null, // (DateTime|string) limit blasts to a blast_date. Allowed to pass 'today' and 'tomorrow'
        'blast_type'     => null, // (\EblastServer\Blasts\BlastType) limit to blast type
    ];

    __construct(search_options)
