<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class BlastsListLocationsOutput
{

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var Command
     */
    protected $output;

    public function __construct(Collection $collection, Command $output)
    {
        $this->collection = $collection;
        $this->output     = $output;
    }

    public function render()
    {
        $count_locations = 0;
        $count_rows      = 0;
        $this->collection->each(function($blast) use(&$count_locations, &$count_rows){
            $this->renderBlast($blast);
            $count_rows++;
            $count_locations+= (int) $blast->locationsCount;
        });

        $this->output->line('<info>Blasts:</info>       ' . $count_rows);
        $this->output->line('<info>Total Emails:</info> ' . $count_locations);
        $this->output->line('');
    }

    protected function renderBlast($blast) {
        $this->output->line("<info>{$blast->getTypeTitle()}:{$blast->id}</info>  {$blast->title} ({$blast->locationsCount} Locations)");
        $this->renderLocations($blast);
    }

    protected function renderLocations($blast) {
        $locations = $blast->locations;
        $locations->each(function($location){
            $this->output->line("    {$location->number} {$location->name}");
        });
    }
}
