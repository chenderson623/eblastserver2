<?php

namespace App\Console\Commands;

class Blasts extends BaseCommand
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Eblasts:blasts {action=list : What to do: list, list_locations}
                            {--id=             : Blast Id}
                            {--blast_type=     : Blast type (AD, EBLAST_COUPON, CUSTOM_EBLAST)}
                            {--location_id=    : Limit to one location}
                            {--location_group= : Filter to location\'s group id}
                            {--date=           : Blast date. format: YYYY-MM-DD. Set to "today" for today\'s date.}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Query blasts';

    protected function rules()
    {
        return [
            'id'             => 'integer',
            'blast_type'     => 'in:AD,EBLAST_COUPON,CUSTOM_EBLAST',
            'location_id'    => 'integer',
            'location_group' => 'integer',
            'date'           => ($this->option('date') === 'today') ? '' : 'date',
        ];
    }

    public function transformOptions()
    {
        $transformer = new BlastsOptionsTransformer();
        return $transformer->transform($this->option());
    }

    protected function getBlastsQuery() {
        return $this->laravel->make('BlastsQuery', $this->transformOptions());
    }

    protected function getBlastCollection() {
        $blasts = $this->getBlastsQuery();
        $blasts->addLocationCountField();
        return $blasts->fetchCollection();
    }

    public function handle()
    {
        // Handle action:
        switch($this->argument('action')) {
            case 'list':
                return $this->handleList();
                break;
            case 'list_locations':
                return $this->handleListLocations();
                break;
            case 'explode_locations':
                return $this->handleExplodeLocations();
                break;
            default:
                throw new \Exception("{$this->argument('action')} is not a valid action.");
        }

    }

    protected function echoHead() {
        $this->output->title("Blasts");
        $this->echoSettings();
    }

    public function handleList()
    {
        $this->echoHead();

        $output = new BlastsListTableOutput($this->getBlastCollection(), $this);
        $output->render();
    }

    public function handleListLocations()
    {
        $this->echoHead();

        $output = new BlastsListLocationsOutput($this->getBlastCollection(), $this);
        $output->render();
    }

    public function handleExplodeLocations()
    {
        $this->echoHead();

        $blasts = $this->getBlastsQuery();
        $blasts->explodeByLocationId();
        $output = new BlastsExplodeLocationsOutput($blasts, $this);
        $output->render();
    }

}
