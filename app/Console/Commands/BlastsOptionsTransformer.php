<?php

namespace App\Console\Commands;

use EblastServer\Blasts\BlastType;

/**
 * Transform options to format that the BlastsQuery will expect
 */
class BlastsOptionsTransformer
{

    protected function value($key, $options) {
        return (isset($options[$key])) ? $options[$key] : null;
    }

    protected function transformOptionInt($key, $options) {
        return ((int) $this->value($key, $options) > 0) ? (int) $this->value($key, $options) : null;
    }

    protected function transformOptionDate($key, $options) {
        $value = $this->value($key, $options);
        if(empty($value)) {
            return null;
        }
        if($value === 'today') {
            return new \DateTime();
        }
        return new \DateTime($value);
    }

    protected function transformOptionBlastType($key, $options) {
        $value = $this->value($key, $options);
        if(empty($value)) {
            return null;
        }
        return BlastType::getByName($value);
    }

    public function transform(array $options) {

        $transformed = [
            'id'             => $this->transformOptionInt('id', $options),
            'blast_type'     => $this->transformOptionBlastType('blast_type', $options),
            'location_id'    => $this->transformOptionInt('location_id', $options),
            'location_group' => $this->transformOptionInt('location_group', $options),
            'date'           => $this->transformOptionDate('date', $options),
        ];
        return array_filter($transformed);
    }

}
