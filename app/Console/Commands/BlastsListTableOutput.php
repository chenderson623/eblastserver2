<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class BlastsListTableOutput
{

    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var Command
     */
    protected $output;

    public function __construct(Collection $collection, Command $output)
    {
        $this->collection = $collection;
        $this->output     = $output;
    }

    protected function getBlastsTableArray()
    {
        return $this->collection->reduce(function($carry, $blast){
            $carry['rows'][] = [
                "{$blast->getTypeTitle()}:{$blast->id}", $blast->title, $blast->locationsCount
            ];
            $carry['count_locations']+= $blast->locationsCount;
            $carry['count_rows']++;
            return $carry;
        }, [
            'count_locations' => 0,
            'count_rows'      => 0,
            'rows'            => []
        ]);
    }

    public function render()
    {
        $table_array = $this->getBlastsTableArray();
        $this->output->table(['Blast', 'Title', 'Locations'], $table_array['rows']);
        $this->output->line('<info>Blasts:</info>       ' . $table_array['count_rows']);
        $this->output->line('<info>Total Emails:</info> ' . $table_array['count_locations']);
        $this->output->line('');
    }
}
