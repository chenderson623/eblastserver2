<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use EblastServer\Blasts\Services\BlastsQuery;

class BlastsExplodeLocationsOutput
{

    /**
     * @var BlastsQuery
     */
    protected $blasts_query;

    /**
     * @var Command
     */
    protected $output;

    public function __construct(BlastsQuery $blasts_query, Command $output)
    {
        $this->blasts_query = $blasts_query;
        $this->output       = $output;
    }

    public function render()
    {
        $count_rows      = 0;
        $this->blasts_query->each(function($blast) use(&$count_rows){
            $this->renderBlast($blast);
            $count_rows++;
        });

        $this->output->line('<info>Emails:</info>       ' . $count_rows);
        $this->output->line('');
    }

    protected function renderBlast($blast) {
        $this->output->line("<info>{$blast->getTypeTitle()}:{$blast->id}</info>  {$blast->title} (Location: {$blast->location_id})");
    }
}
