<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

abstract class BaseCommand extends Command
{
    protected $setting_indent = 4;
    protected $setting_tab    = 23;

    use ValidateOptionsTrait;

    protected function echoSetting($label, $value) {
        $label_padded = str_repeat(' ', $this->setting_indent) . $label;
        if(strlen($label_padded) > $this->setting_tab) {
            $label_padded = substr($label_padded, 0, $this->setting_tab);
        }
        $label_padded = str_pad($label_padded, $this->setting_tab, ' ');
        $this->output->write($label_padded . ': ');
        $this->info($value);
    }

    protected function echoSettings() {
        foreach(array_filter($this->option()) as $key=>$value) {
            $this->echoSetting($key, $value);
        }
    }
}
