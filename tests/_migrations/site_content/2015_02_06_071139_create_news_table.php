<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news', function(Blueprint $table)
		{
			$table->increments('id');
			$table->datetime('start_date')->index();
			$table->datetime('end_date')->index();
			$table->string('title');
			$table->text('content');
			$table->string('directory');
			$table->string('image_filename');
			$table->timestamps();
			$table->softDeletes();
		});
		Schema::create('news_locations_groups', function(Blueprint $table)
		{
			$table->integer('news_id')->index();
			$table->integer('location_id')->index();
			$table->integer('group_id')->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
		Schema::drop('news_locations_groups');
	}

}
