<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rewards_cards', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('name');
            $table->boolean('active')->default(1);
            $table->string('template_name');
            $table->string('default_url');
            $table->string('default_menu_label');
            $table->text('card_meta_fields')->nullable();
            $table->text('program_meta_fields')->nullable();
            $table->text('location_meta_fields')->nullable();

            $table->timestamps();
		});

        Schema::create('rewards_meta', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('rewards_card_id')->index();
            $table->integer('rewards_program_id')->index();
            $table->integer('location_id')->index();

            $table->string('key')->index();
            $table->smallInteger('index')->nullable();
            $table->text('value');

            $table->timestamps();
        });

        Schema::create('rewards_programs', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('rewards_card_id')->index();
            $table->boolean('active')->default(1);
            $table->string('name');
            $table->string('username');
            $table->string('password');
            $table->string('display_name');
            $table->string('menu_label');

            $table->timestamps();
        });

        Schema::create('rewards_programs_locations', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('location_id')->index();
            $table->integer('group_id')->index();
            $table->integer('rewards_program_id')->index();
            $table->boolean('active')->default(1);
            $table->string('program_display_name');
            $table->string('program_store_num');

            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rewards_cards');
        Schema::drop('rewards_meta');
        Schema::drop('rewards_programs');
        Schema::drop('rewards_programs_locations');
	}
}