<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebblastTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{


        $create_sql = <<<CREATE
CREATE TABLE IF NOT EXISTS `webblast_templates` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `template_name` varchar(35) NOT NULL,
  `template_desc` varchar(255) NOT NULL,
  `template_source` varchar(50) NOT NULL,
  `web_slug` varchar(35) NOT NULL,
  `blast_type` varchar(25) NOT NULL,
  `email_subject` varchar(100) NOT NULL,
  `email_plain_text` text NOT NULL,
  `blast_template_values` text NOT NULL,
  `image_dir` varchar(35) NOT NULL,
  `thumbnail_filename` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1
CREATE;

        DB::connection()->getPdo()->exec($create_sql);

        $populate_sql = <<<POPULATE
INSERT INTO `webblast_templates` (`id`, `template_name`, `template_desc`, `template_source`, `web_slug`, `blast_type`, `email_subject`, `email_plain_text`, `blast_template_values`, `image_dir`, `thumbnail_filename`) VALUES
(506, 'coupons.com announcement', '', 'WebBlastTemplateSource_CouponsCom', 'Coupons.Com', 'special_offers', '--|site_loc_name|-- - Start Saving Now with Coupons.com', 'Announcing a new feature from --|site_loc_name|--\\r\\n\\r\\nStart Saving Now With Our Online Savings Center\\r\\n\\r\\nPrint at Home, Shop Local, Save Better!\\r\\n\\r\\nCounpons.com - Get Free printable coupons and more from our Coupons.com Online Savings Center.\\r\\n\\r\\nTo Access Our Online Savings Center:\\r\\n1. Visit our Website\\r\\n2. Click on Ads & Coupons\\r\\n3. Select Coupons.com\\r\\n4. Select the Coupons You Want to Print\\r\\n5. Click Print Coupons and Start Saving!\\r\\n\\r\\nForward this to a friend: --|web_url|--?mid=-track_code-&recid=-blast_recipient-&l=pt\\r\\n\\r\\nVisit our website: --|site_url|--?mid=-track_code-&recid=-blast_recipient-&l=pt\\r\\n--|store_name|--\\r\\n--|site_address_print_one_line|--\\r\\n\\r\\nUnsubscribe from this mailing list: --|site_url|--/subscribe/opt.php?mid=-track_code-&recid=-blast_recipient-&l=pt', '{\\n    "main_image_alt_text":"Enable images to view",\\n    "main_image_title_text":"--|site_loc_name|-- - Coupons.com",\\n    "main_image_link_url":"--|web_url|--"\\n}', '', ''),
(528, 'With Coupon', '', 'WebBlastTemplateSource_WithCoupon', 'SpecialCoupon', 'special_offers', '[fill in offer here]', '--|site_loc_name|--\\r\\n\\r\\nHere is a special coupon for you.\\r\\n\\r\\nOffer available from --|offer_start_date_formatted|-- through --|offer_end_date_formatted|--\\r\\n\\r\\nGo here for more info: --|blast_url|--&l=pt\\r\\n\\r\\nVisit our website: --|homepage_url|--&l=pt\\r\\n\\r\\n--|store_name|--\\r\\n--|site_address_print_one_line|--\\r\\n\\r\\nUnsubscribe from this mailing list:\\r\\n--|unsubscribe_url|--&l=pt', '{\\r\\n    "main_image_alt_text":"Enable images to view",\\r\\n    "main_image_title_text":"Special Offers",\\r\\n    "main_image_link_url":"--|blast_url|--&l=main",\\r\\n    "has_coupons": "true"\\r\\n}', '', ''),
(624, 'Special Deal-ivery', '', 'WebBlastTemplateSource_SpecialDealivery', 'SpecialDealivery', 'special_offers', 'Special Deal-ivery', '--|site_loc_name|--\\r\\n\\r\\nFree [CHANGE TO ITEM] with $25.00 purchase.\\r\\n\\r\\nOffer available from --|offer_start_date_formatted|-- through --|offer_end_date_formatted|--\\r\\n\\r\\nGo here for more info: --|blast_url|--&l=pt\\r\\n\\r\\nVisit our website: --|homepage_url|--&l=pt\\r\\n\\r\\n--|store_name|--\\r\\n--|site_address_print_one_line|--\\r\\n\\r\\nUnsubscribe from this mailing list:\\r\\n--|unsubscribe_url|--&l=pt', '{\\r\\n    "main_image_alt_text":"Enable images to view",\\r\\n    "main_image_title_text":"Special Offers",\\r\\n    "main_image_link_url":"--|blast_url|--&l=main",\\r\\n    "has_coupons": "true"\\r\\n}', '', ''),
(625, 'Standard Webblast', '', 'WebBlastTemplateSource_CenteredLogo', '[replace with something meaningful]', 'special_offers', 'A Special Offer From --|site_loc_name|--', '--|site_loc_name|--\\r\\n\\r\\nA Special offer for you\\r\\n\\r\\nOffer available from --|offer_start_date_formatted|-- through --|offer_end_date_formatted|--\\r\\n\\r\\nGo here for more info: --|blast_url|--&l=pt\\r\\n\\r\\nVisit our website: --|homepage_url|--&l=pt\\r\\n\\r\\n--|store_name|--\\r\\n--|site_address_print_one_line|--\\r\\n\\r\\nUnsubscribe from this mailing list:\\r\\n--|unsubscribe_url|--&l=pt', '{\\r\\n    "main_image_alt_text":"Enable images to view",\\r\\n    "main_image_title_text":"Special Offers",\\r\\n    "main_image_link_url":"--|blast_url|--&l=main",\\r\\n    "has_print_view": "true"\\r\\n}', '', ''),
(626, '3 Day Sale Animated', '', 'WebBlastTemplateSource_3DaySaleAnimated', '[replace with something meaningful]', 'special_offers', 'A Special Offer From --|site_loc_name|--', '--|site_loc_name|--\\r\\n\\r\\nA Special offer for you\\r\\n\\r\\nOffer available from --|offer_start_date_formatted|-- through --|offer_end_date_formatted|--\\r\\n\\r\\nGo here for more info: --|blast_url|--&l=pt\\r\\n\\r\\nVisit our website: --|homepage_url|--&l=pt\\r\\n\\r\\n--|store_name|--\\r\\n--|site_address_print_one_line|--\\r\\n\\r\\nUnsubscribe from this mailing list:\\r\\n--|unsubscribe_url|--&l=pt', '{\\r\\n    "banner_image_alt_text":"Enable images to view",\\r\\n    "banner_image_title_text":"3-Day Sale",\\r\\n    "main_image_alt_text":"Enable images to view",\\r\\n    "main_image_title_text":"Special Offers",\\r\\n    "main_image_link_url":"--|blast_url|--&l=main",\\r\\n    "has_print_view": "true"\\r\\n}', '', '')
POPULATE;

        DB::connection()->getPdo()->exec($populate_sql);

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('webblast_templates');
	}

}
