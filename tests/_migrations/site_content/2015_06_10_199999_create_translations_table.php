<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('translations', function(Blueprint $table) {
            $table->increments('id');

            $table->integer('location_id')->index();
            $table->string('content_type', 50);
            $table->string('slug', 25);
            $table->string('language', 25);
            $table->string('translated_title', 255);
            $table->text('translated_content');
        });

        $translations = [
            ['Spanish', 'subscribe', 'sign_up_header', 'Reciba ofertas exclusivas por email'],
            ['Spanish', 'subscribe', 'header_input_hint','dirección de correo electrónico'],
            ['Spanish', 'subscribe', 'subscribe_title','Anuncio Especiales y promociones exclusivas'],
            ['Spanish', 'subscribe', 'sign_up_welcome','Regístrese para recibir nuestras distintas opciones publicitarias exclusivas y ofertas exclusivas a través de correo electrónico o actualizar las preferencias de mensajes y anular la suscripción a listas de correo (usuarios actuales). Introduzca su dirección de correo electrónico y haga clic en siguiente.'],
            ['Spanish', 'subscribe', 'trouble_viewing_email','Tiene problemas para ver nuestro correo electrónico?'],
            ['Spanish', 'subscribe', 'email_tips_button','Ver Email Consejos'],
            ['Spanish', 'subscribe', 'your_email_label','Su correo electrónico'],
            ['Spanish', 'subscribe', 'submit_email_button','Enviar E-mail'],
            ['Spanish', 'landing', 'preferred_store','Mi tienda Preferido'],

            ['Korean', 'subscribe', 'sign_up_header', '전용 이메일 제공 가입'],
            ['Korean', 'subscribe', 'subscribe_title', '광고 독점 상품을 특가'],

        ];


        foreach($translations as $translation) {
            DB::table('translations')->insert([
                'content_type' => $translation[1],
                'slug' => $translation[2],
                'language' => $translation[0],
                'translated_content' => $translation[3]
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('translations');
    }

}
