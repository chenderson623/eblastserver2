<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ads', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 50);
			$table->string('display_name', 50)->nullable();
			$table->string('labels', 50)->nullable();
			$table->tinyInteger('pages', false, false);
			$table->date('blast_date');
			$table->date('display_start_date');
			$table->date('display_end_date');
			$table->date('offer_start_date');
			$table->date('offer_end_date');
			$table->string('upload_type', 25);
			$table->string('dir_name', 25);
			$table->integer('request_id', false, false);
			$table->boolean('archived')->default(0);

			$table->timestamps();
			$table->softDeletes();
		});
		Schema::create('ads_locations_groups', function(Blueprint $table)
		{
			$table->integer('ad_id')->index();
			$table->integer('location_id')->index();
			$table->integer('group_id')->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ads');
		Schema::drop('ads_locations_groups');
	}

}
