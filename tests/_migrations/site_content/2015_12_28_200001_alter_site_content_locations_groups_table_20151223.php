<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSiteContentLocationsGroupsTable20151223 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::update("ALTER TABLE `site_content_locations_groups` CHANGE `location_id` `location_id` INT(11) NULL");
        DB::update("ALTER TABLE `site_content_locations_groups` CHANGE `group_id` `group_id` INT(11) NULL");
        DB::update("ALTER TABLE `site_content`.`site_content_locations_groups` ADD UNIQUE `unique_location` (`site_content_id`, `location_id`)");
        DB::update("ALTER TABLE `site_content_locations_groups` ADD `content_order` SMALLINT NULL AFTER `group_id`");

    }
}
