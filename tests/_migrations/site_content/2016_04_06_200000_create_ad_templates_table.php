<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{


        $create_sql = <<<CREATE
CREATE TABLE IF NOT EXISTS `ad_templates` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `template_name` varchar(35) NOT NULL,
  `template_desc` varchar(255) NOT NULL,
  `template_source` varchar(50) NOT NULL,
  `web_slug` varchar(35) NOT NULL,
  `blast_type` varchar(25) NOT NULL,
  `email_subject` varchar(100) NOT NULL,
  `email_plain_text` text NOT NULL,
  `blast_template_values` text NOT NULL,
  `image_dir` varchar(35) NOT NULL,
  `thumbnail_filename` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1
CREATE;

        DB::connection()->getPdo()->exec($create_sql);

        $populate_sql = <<<POPULATE
INSERT INTO `ad_templates` (`id`, `template_name`, `template_desc`, `template_source`, `web_slug`, `blast_type`, `email_subject`, `email_plain_text`, `blast_template_values`, `image_dir`, `thumbnail_filename`) VALUES
(500, 'Template Weekly Ad', '', 'WeeklyAdTemplateSource_Default', 'ads', 'Ad', '--|ad_display_name|-- Specials for --|site_loc_name|--', 'As requested, here are our --|ad_display_name|-- Specials:\r\n--|blast_url|--&l=pt\r\n\r\nThanks for being our customer!\r\n\r\nPrices Effective --|offer_start_date_formatted|-- - --|offer_end_date_formatted|--\r\n\r\nVisit our website: --|homepage_url|--&l-pt\r\n\r\n--|site_loc_name|--\r\n--|site_address_print_one_line|--\r\n\r\nUnsubscribe from this mailing list: --|unsubscribe_url|--&l=pt', '{\r\n    "main_image_alt_text":"Enable images to view",\r\n    "main_image_title_text":"--|ad_display_name|-- Specials"\r\n}', '', ''),
(501, 'AFM Good Buys Template', '', 'WeeklyAdTemplateSource_AFMGoodBuys', 'ads', 'Ad', '--|site_loc_name|-- - Say Hello to Good Buys Specials', 'As requested, here are our --|ad_display_name|-- Specials:\r\n--|blast_url|--&l=pt\r\n\r\nThanks for being our customer!\r\n\r\nPrices Effective --|offer_start_date_formatted|-- - --|offer_end_date_formatted|--\r\n\r\nVisit our website: --|homepage_url|--&l-pt\r\n\r\n--|site_loc_name|--\r\n--|site_address_print_one_line|--\r\n\r\nUnsubscribe from this mailing list: --|unsubscribe_url|--&l=pt', '{\r\n    "main_image_alt_text":"Enable images to view",\r\n    "main_image_title_text":"--|ad_display_name|-- Specials"\r\n}', '', '');
POPULATE;

        DB::connection()->getPdo()->exec($populate_sql);

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_templates');
	}

}
