<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideshowsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slideshows', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('name', 50);
            $table->string('description')->nullable();
            $table->boolean('fill_default')->default(1);
            $table->boolean('exclude_wholesaler')->default(0);

			$table->timestamps();
			$table->softDeletes();
		});

        Schema::create('slideshows_locations_groups', function(Blueprint $table)
        {
            $table->integer('slideshow_id')->index();
            $table->integer('location_id')->index();
            $table->integer('group_id')->index();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slideshows');
        Schema::drop('slideshows_locations_groups');
	}

}
