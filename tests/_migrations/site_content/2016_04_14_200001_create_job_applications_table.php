<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateJobApplicationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('job_applications', function(Blueprint $table) {
            $table->increments('id');

            $table->integer('location_id');

            $table->string('position');
            $table->string('previous_app', 3);
            $table->string('previous_app_date');
            $table->string('previous_employee', 3);
            $table->string('previous_employee_when');
            $table->string('previous_employee_reason');
            $table->string('age_verification', 3);
            $table->string('work_verification', 3);
            $table->string('available_start');
            $table->string('availability');
            $table->string('layoff_status', 3);
            $table->string('name');
            $table->string('address');
            $table->string('address2');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('phone');
            $table->string('cell');
            $table->string('contact_time');

            $table->text('jobs_employer');
            $table->text('jobs_address');
            $table->text('jobs_address2');
            $table->text('jobs_city');
            $table->text('jobs_state');
            $table->text('jobs_zip');
            $table->text('jobs_position');
            $table->text('jobs_description');
            $table->text('jobs_start');
            $table->text('jobs_end');
            $table->text('jobs_supervisor');
            $table->text('jobs_supervisor_contact');
            $table->text('jobs_reason');
            $table->text('school_name');
            $table->text('school_address');
            $table->text('school_address2');
            $table->text('school_city');
            $table->text('school_state');
            $table->text('school_zip');
            $table->text('school_highest_grade');
            $table->text('school_degree_areas');
            $table->text('school_currently');
            $table->text('school_currently_status');
            $table->text('school_notes');
            $table->text('reference_name');
            $table->text('reference_relationship');
            $table->text('reference_address');
            $table->text('reference_address2');
            $table->text('reference_city');
            $table->text('reference_state');
            $table->text('reference_zip');
            $table->text('reference_phone');
            $table->string('convicted', 3);
            $table->text('convicted_explain');
            $table->string('ip', 25);

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('job_applications');
    }

}
