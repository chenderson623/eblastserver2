<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLinksTable20151230 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::update("ALTER TABLE `links` ADD `start_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `id`, ADD `end_date` DATETIME NULL AFTER `start_date`");
        DB::update("ALTER TABLE `links` CHANGE `name` `title` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");
        DB::update("ALTER TABLE `links` ADD `directory` VARCHAR(255) NOT NULL AFTER `title`");

    }
}
