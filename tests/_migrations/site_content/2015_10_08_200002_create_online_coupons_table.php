<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineCouponsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('online_coupons', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('coupon_id')->index();

            $table->date('display_start_date')->index();
            $table->date('display_end_date')->index();
            $table->date('offer_start_date');
            $table->date('offer_end_date');

            $table->timestamps();
		});

        Schema::create('online_coupon_locations_groups', function(Blueprint $table)
        {
            $table->integer('online_coupon_id')->index();
            $table->integer('location_id')->index();
            $table->integer('group_id')->index();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('online_coupons');
        Schema::drop('online_coupon_locations_groups');
	}
}