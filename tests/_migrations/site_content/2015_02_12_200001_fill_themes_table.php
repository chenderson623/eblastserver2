<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillThemesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::table('themes')->truncate();

        /*
            array(
            'name'=> '',
            'css'=>'',
            'imageDir' => '',
            'calendar_css'=>'',
            'heading_color'=> '',
            'link_color'=> '',
            'default_color'=> ''
            ),
        */

        $themes = array(
            array(
            'name'=> 'IGA Template',
            'css'=>'theme_iga.css',
            'imageDir' => 'theme-iga-images',
            'calendar_css'=>'/css/jquery-ui.default.css',
            'heading_color'=> '#60b617',
            'link_color'=> '#f97b17',
            'default_color'=> '#948671'
            ),
            array(
            'name'=> 'Shurfine Template',
            'css'=>'theme_shurfine.css',
            'imageDir' => 'theme-shurfine-images',
            'calendar_css'=>'/css/jquery-ui.default.css',
            'heading_color'=> '#2B1E60',
            'link_color'=> '#f93f58',
            'default_color'=> '#727272',
            //'coupon_header_image'=> 'clickHereForPrintableCouponsShurfine.gif'
            ),
            array(
            'name'=> 'Green Red Blue Theme',
            'css'=>'theme_lgc_green.css',
            'imageDir' => 'theme-lgc_green-images',
            'calendar_css'=>'/css/jquery-ui.default.css',
            'heading_color' => '#60b617',
            'link_color' => '#f97b17',
            'default_color' => '#948671'
            ),
            array(
            'name'=> 'Orange and Yellow Theme',
            'css'=>'theme_orange_on_yellow.css',
            'imageDir' => 'theme-orange_on_yellow',
            'calendar_css'=>'/css/jquery-ui.default.css',
            'heading_color' => '#2B1E60',
            'link_color' => '#f93f58',
            'default_color' => '#727272'
            ),
            array(
            'name'=> 'Lime Green Theme',
            'css'=>'theme_lime_green.css',
            'imageDir' => 'theme-lime_green',
            'calendar_css'=>'/css/jquery-ui.default.css',
            'heading_color' => '#2B1E60',
            'link_color' => '#f93f58',
            'default_color' => '#727272'
            ),
            array(
            'name'=> 'Hispanic Red Theme',
            'css'=>'theme_hispanic_red.css',
            'imageDir' => 'theme-hispanic_red-images',
            'calendar_css'=>'/css/jquery-ui.default.css',
            'heading_color' => '#2B1E60',
            'link_color' => '#f93f58',
            'default_color' => '#727272'
            ),
            array(
            'name'=> 'Hispanic Blue Theme',
            'css'=>'theme_hispanic_blue.css',
            'imageDir' => 'theme-hispanic_blue-images',
            'calendar_css'=>'/css/jquery-ui.default.css',
            'heading_color' => '#2B1E60',
            'link_color' => '#f93f58',
            'default_color' => '#727272'
            ),
            array(
            'name'=> 'Blue Vines Theme',
            'css'=>'theme_blue_vines.css',
            'imageDir' => 'theme-blue_vines-images',
            'calendar_css'=>'/css/jquery-ui.default.css',
            'heading_color' => '#2B1E60',
            'link_color' => '#f93f58',
            'default_color' => '#727272'
            ),
            array(
            'name'=> 'Red Stripes Theme',
            'css'=>'theme_red_stripes.css',
            'imageDir' => 'theme-red_stripes-images',
            'calendar_css'=>'/css/jquery-ui.default.css',
            'heading_color' => '#2B1E60',
            'link_color' => '#f93f58',
            'default_color' => '#727272'
            ),
            array(
            'name'=> 'Red Black Template',
            'css'=>'theme_olean_red.css',
            'imageDir' => 'theme-olean_red-images',
            'heading_color' => '#555',
            'link_color' => '#c00',
            'default_color' => '#352b2b',
            'calendar_css'=>'/css/jquery-ui.red.css'
            ),
            array(
            'name'=> 'Red Black Tab Theme',
            'css'=>'theme_olean_red.css',
            'imageDir' => 'theme-olean_red-images',
            'heading_color' => '#555',
            'link_color' => '#c00',
            'default_color' => '#352b2b',
            'calendar_css'=>'/css/jquery-ui.red.css'
            ),
            array(
            'name'=> 'LGC Template',
            'css'=>'theme_lgc_green.css',
            'imageDir' => 'theme-lgc_green-images',
            'calendar_css'=>'/css/jquery-ui.default.css',
            'heading_color'=> '#60b617',
            'link_color'=> '#f97b17',
            'default_color'=> '#948671'
            ),
            array(
            'name'=> 'OGC Template',
            'css'=>'theme_olean_red.css',
            'imageDir' => 'theme-olean_red-images',
            'calendar_css'=>'/css/jquery-ui.red.css',
            'heading_color'=> '#555',
            'link_color'=> '#c00',
            'default_color'=> '#352b2b'
            ),
            array(
            'name'=>'Green Blue Tab Theme',
            'css'=> 'theme_iga.css',
            'imageDir'=> 'theme-iga-images',
            'calendar_css'=> '/css/jquery-ui.default.css',
            'heading_color'=> '#60b617',
            'link_color'=> '#f97b17',
            'default_color'=> '#948671'
            )
        );

        DB::table('themes')->insert( $themes );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
