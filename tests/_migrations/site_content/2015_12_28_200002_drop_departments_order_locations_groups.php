<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropDepartmentsOrderLocationsGroups extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::update("DROP TABLE `departments_order_locations_groups`");

    }
}
