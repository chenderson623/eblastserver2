<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebblastsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('webblasts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('webblast_template_id', false, false);
			$table->string('blast_name', 50);
			$table->string('web_slug', 25)->nullable()->index();
			$table->string('email_subject', 100);
			$table->text('email_plain_text');
			$table->text('blast_template_values');
			$table->string('image_dir', 50)->nullable();
			$table->date('blast_date');
			$table->date('display_start_date')->index();
			$table->date('display_end_date')->index();
			$table->date('offer_start_date')->index();
			$table->date('offer_end_date')->index();

			$table->timestamps();
			$table->softDeletes();
		});
		Schema::create('webblasts_locations_groups', function(Blueprint $table)
		{
			$table->integer('webblast_id')->index();
			$table->integer('location_id')->index();
			$table->integer('group_id')->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('webblasts');
		Schema::drop('webblasts_locations_groups');
	}

}
