<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('coupons', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('title', 100);
            $table->string('price', 100);
            $table->text('description')->nullable();
            $table->text('disclaimer')->nullable();
            $table->string('plu', 15)->nullable();
            $table->string('redemption_value', 15)->nullable();

            $table->string('product_image_filename');
            $table->string('beauty_image_filename');

            $table->string('dir_name');

            $table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('coupons');
	}
}