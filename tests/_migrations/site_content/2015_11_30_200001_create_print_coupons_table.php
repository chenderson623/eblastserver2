<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintCouponsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('print_coupons', function(Blueprint $table)
		{
			$table->increments('id');

            $table->date('display_start_date')->index();
            $table->date('display_end_date')->index();

            $table->string('dir_name', 25);
            $table->string('pdf_filename', 50);

            $table->timestamps();
		});

        Schema::create('print_coupons_locations_groups', function(Blueprint $table)
        {
            $table->integer('print_coupon_id')->index();
            $table->integer('location_id')->index();
            $table->integer('group_id')->index();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('print_coupons');
        Schema::drop('print_coupons_locations_groups');
	}
}