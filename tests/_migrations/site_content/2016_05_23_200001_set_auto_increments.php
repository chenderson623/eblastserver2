<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SetAutoIncrements extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        $statements = [
            "ALTER TABLE `ads` AUTO_INCREMENT = 1800000",
            "ALTER TABLE `events` AUTO_INCREMENT = 18000",
            "ALTER TABLE `feature_boxes` AUTO_INCREMENT = 1800000",
            "ALTER TABLE `feature_schedules` AUTO_INCREMENT = 18000000",
            "ALTER TABLE `links` AUTO_INCREMENT = 18000",
            "ALTER TABLE `news` AUTO_INCREMENT = 180000",
            "ALTER TABLE `online_coupons` AUTO_INCREMENT = 180000",
            "ALTER TABLE `polls` AUTO_INCREMENT = 1800",
            "ALTER TABLE `poll_votes` AUTO_INCREMENT = 180000",
            "ALTER TABLE `print_coupons` AUTO_INCREMENT = 180000",
            "ALTER TABLE `rewards_meta` AUTO_INCREMENT = 18000",
            "ALTER TABLE `rewards_programs` AUTO_INCREMENT = 1800000",
            "ALTER TABLE `site_content` AUTO_INCREMENT = 180000",
            "ALTER TABLE `slideshows` AUTO_INCREMENT = 180000",
            "ALTER TABLE `slideshow_schedules` AUTO_INCREMENT = 18000000",
            "ALTER TABLE `slideshow_slides` AUTO_INCREMENT = 1800000",
            "ALTER TABLE `slideshow_slide_images` AUTO_INCREMENT = 1800000",
            "ALTER TABLE `surveys` AUTO_INCREMENT = 1800",
            "ALTER TABLE `survey_answers` AUTO_INCREMENT = 180000",
            "ALTER TABLE `survey_questions` AUTO_INCREMENT = 18000",
        ];

        foreach($statements as $statement) {
            DB::unprepared($statement);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }
}
