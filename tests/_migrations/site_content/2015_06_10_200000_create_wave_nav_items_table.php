<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaveNavItemsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        /* OLD table:
          CREATE TABLE IF NOT EXISTS `site_options` (
          `option_id` int(11) NOT NULL,
          `option_name` varchar(250) NOT NULL COMMENT 'listed in cms admin',
          `option_description` text NOT NULL,
          `nav_slug` varchar(15) NOT NULL COMMENT 'unique slug - use to check if option is set',
          `nav_parent_slug` varchar(15) NOT NULL,
          `nav_label` varchar(25) NOT NULL,
          `sidebar_title` varchar(25) NOT NULL,
          `url_path` varchar(100) NOT NULL,
          `nav_main` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT 'nav order, if > 0, shows in nag. decimal ok',
          `nav_header` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT 'nav order, if > 0, shows in nag. decimal ok',
          `nav_footer` decimal(4,2) NOT NULL DEFAULT '0.00' COMMENT 'nav order, if > 0, shows in nag. decimal ok',
          `drop_down` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0/1',
          `content_type` varchar(15) NOT NULL COMMENT 'ties to site_content table',
          `paged_content` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'triggers structure for drop downs',
          `option_phase` int(11) NOT NULL,
          `option_included` tinyint(1) NOT NULL,
          `option_required` tinyint(1) NOT NULL,
          `translated_nav_label` varchar(25) NOT NULL,
          `translated_sidebar_title` varchar(25) NOT NULL
          ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
         */

        Schema::create('wave_nav_items', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('service_option_id')->index();

            $table->string('nav_slug', 15);
            $table->string('nav_parent_slug', 15);
            $table->string('nav_label', 25);
            $table->string('sidebar_title', 25);
            $table->string('url_path', 100);
            $table->decimal('nav_main', 4, 2)->comment   = 'nav order, if > 0, shows in nag. decimal ok';
            $table->decimal('nav_header', 4, 2)->comment = 'nav order, if > 0, shows in nag. decimal ok';
            $table->decimal('nav_footer', 4, 2)->comment = 'nav order, if > 0, shows in nag. decimal ok';
            $table->boolean('drop_down')->default(0);
            $table->string('content_type', 15)->comment  = 'ties to site_content table';
            $table->boolean('paged_content')->default(0);
            //$table->string('translated_nav_label', 25);
            //$table->string('translated_sidebar_title', 25);
        });


        $nav_items = array(
            array(
                'service_option_id'        => 0,
                'nav_slug'                 => 'home',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Home',
                'sidebar_title'            => '',
                'url_path'                 => '/',
                'nav_main'                 => '1.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '1.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Inicio',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 2,
                'nav_slug'                 => 'about',
                'nav_parent_slug'          => '',
                'nav_label'                => 'About Us',
                'sidebar_title'            => '',
                'url_path'                 => '/about/',
                'nav_main'                 => '0.00',
                'nav_header'               => '1.00',
                'nav_footer'               => '5.00',
                'drop_down'                => '0',
                'content_type'             => 'about',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Sobre Nosotros',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 3,
                'nav_slug'                 => 'location',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Location',
                'sidebar_title'            => 'We\'re Open',
                'url_path'                 => '/location/',
                'nav_main'                 => '0.00',
                'nav_header'               => '5.00',
                'nav_footer'               => '4.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Ubicación',
                'translated_sidebar_title' => 'Estamos Abierto'
            ),
            array(
                'service_option_id'        => 4,
                'nav_slug'                 => 'jobs',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Jobs',
                'sidebar_title'            => 'Apply Now',
                'url_path'                 => '/jobs/',
                'nav_main'                 => '0.00',
                'nav_header'               => '3.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => 'jobs',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Trabajos',
                'translated_sidebar_title' => 'Solicita Ahora'
            ),
            array(
                'service_option_id'        => 5,
                'nav_slug'                 => 'subscribe',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Subscribe',
                'sidebar_title'            => '',
                'url_path'                 => '/subscribe/',
                'nav_main'                 => '0.00',
                'nav_header'               => '2.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Suscribir',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 6,
                'nav_slug'                 => 'weekly_ad',
                'nav_parent_slug'          => 'promotions',
                'nav_label'                => 'Ad Specials',
                'sidebar_title'            => 'Ad Specials',
                'url_path'                 => '/ads/',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Especiales',
                'translated_sidebar_title' => 'Especiales'
            ),
            array(
                'service_option_id'        => 7,
                'nav_slug'                 => 'recipes',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Recipes',
                'sidebar_title'            => 'Recipes',
                'url_path'                 => '/recipes/',
                'nav_main'                 => '3.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Recetas',
                'translated_sidebar_title' => 'Recetas'
            ),
            array(
                'service_option_id'        => 8,
                'nav_slug'                 => 'health_wellness',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Health & Wellness',
                'sidebar_title'            => 'Health & Wellness',
                'url_path'                 => '/aboutu/',
                'nav_main'                 => '4.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '1',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'La Salud y el Bienestar',
                'translated_sidebar_title' => 'La Salud y el Bienestar'
            ),
            array(
                'service_option_id'        => 9,
                'nav_slug'                 => 'web_coupons',
                'nav_parent_slug'          => 'promotions',
                'nav_label'                => 'Online Coupons',
                'sidebar_title'            => 'Online Coupons',
                'url_path'                 => '/coupons/',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Cupones en Línea',
                'translated_sidebar_title' => 'Cupones en Linea'
            ),
            array(
                'service_option_id'        => 10,
                'nav_slug'                 => 'news',
                'nav_parent_slug'          => '',
                'nav_label'                => 'News',
                'sidebar_title'            => '',
                'url_path'                 => '/news/',
                'nav_main'                 => '10.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Noticias',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 11,
                'nav_slug'                 => 'rewards',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Rewards',
                'sidebar_title'            => '',
                'url_path'                 => '/rewards/',
                'nav_main'                 => '6.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Recompensas',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 19,
                'nav_slug'                 => 'wine_pairing',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Wine Pairing',
                'sidebar_title'            => '',
                'url_path'                 => '/wine_pairing/',
                'nav_main'                 => '7.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Maridaje de Vinos',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 21,
                'nav_slug'                 => 'links',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Links',
                'sidebar_title'            => '',
                'url_path'                 => '/links/',
                'nav_main'                 => '0.00',
                'nav_header'               => '4.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Hiperenlances',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 22,
                'nav_slug'                 => 'poll',
                'nav_parent_slug'          => '',
                'nav_label'                => '',
                'sidebar_title'            => 'Voting',
                'url_path'                 => '',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Encuesta de Cliente',
                'translated_sidebar_title' => 'Encuesta de Cliente'
            ),
            array(
                'service_option_id'        => 25,
                'nav_slug'                 => 'shopping_list',
                'nav_parent_slug'          => '',
                'nav_label'                => '',
                'sidebar_title'            => 'Shopping List',
                'url_path'                 => '',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => '',
                'translated_sidebar_title' => 'Lista de Compras'
            ),
            array(
                'service_option_id'        => 28,
                'nav_slug'                 => 'events',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Events',
                'sidebar_title'            => 'events',
                'url_path'                 => '/events/',
                'nav_main'                 => '9.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Eventos',
                'translated_sidebar_title' => 'eventos'
            ),
            array(
                'service_option_id'        => 29,
                'nav_slug'                 => 'planner',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Meal Planner',
                'sidebar_title'            => '',
                'url_path'                 => '/planner/',
                'nav_main'                 => '5.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Planificador de Comidas',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 31,
                'nav_slug'                 => 'survey',
                'nav_parent_slug'          => '',
                'nav_label'                => '',
                'sidebar_title'            => '',
                'url_path'                 => '',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => '',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 32,
                'nav_slug'                 => 'departments',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Departments',
                'sidebar_title'            => 'Departments',
                'url_path'                 => '/departments/',
                'nav_main'                 => '8.50',
                'nav_header'               => '0.00',
                'nav_footer'               => '3.00',
                'drop_down'                => '1',
                'content_type'             => 'dept',
                'paged_content'            => '1',
                'translated_nav_label'     => 'Departamentos',
                'translated_sidebar_title' => 'Departamentos'
            ),
            array(
                'service_option_id'        => 36,
                'nav_slug'                 => 'social_media',
                'nav_parent_slug'          => '',
                'nav_label'                => '',
                'sidebar_title'            => '',
                'url_path'                 => '',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => '',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 44,
                'nav_slug'                 => 'nu_val',
                'nav_parent_slug'          => 'health_wellness',
                'nav_label'                => 'Nu-Val',
                'sidebar_title'            => 'Nu-Val',
                'url_path'                 => '/nuval/',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Nu-val',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 39,
                'nav_slug'                 => 'contact',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Contact',
                'sidebar_title'            => 'Contact Us',
                'url_path'                 => '/contact',
                'nav_main'                 => '0.00',
                'nav_header'               => '2.00',
                'nav_footer'               => '6.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Contáctenos',
                'translated_sidebar_title' => 'Contáctenos'
            ),
            array(
                'service_option_id'        => 0,
                'nav_slug'                 => 'promotions',
                'nav_parent_slug'          => '',
                'nav_label'                => 'Ad Specials',
                'sidebar_title'            => 'Ads & Coupons',
                'url_path'                 => '',
                'nav_main'                 => '2.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '1',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Especiales',
                'translated_sidebar_title' => 'Especiales'
            ),
            array(
                'service_option_id'        => 41,
                'nav_slug'                 => 'coupons_com',
                'nav_parent_slug'          => 'promotions',
                'nav_label'                => 'Coupons.com',
                'sidebar_title'            => '',
                'url_path'                 => '/coupons/coupons_com.php',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'Coupons.com',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 42,
                'nav_slug'                 => 'saving_star',
                'nav_parent_slug'          => 'promotions',
                'nav_label'                => 'Saving Star',
                'sidebar_title'            => '',
                'url_path'                 => '/coupons/saving_star.php',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => '',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 43,
                'nav_slug'                 => 'redplum',
                'nav_parent_slug'          => 'promotions',
                'nav_label'                => 'Redplum',
                'sidebar_title'            => '',
                'url_path'                 => '/coupons/redplum.php',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => '',
                'translated_sidebar_title' => ''
            ),
            array(
                'service_option_id'        => 37,
                'nav_slug'                 => 'all_about_u',
                'nav_parent_slug'          => 'health_wellness',
                'nav_label'                => 'All About U',
                'sidebar_title'            => 'All About U',
                'url_path'                 => '/aboutu/',
                'nav_main'                 => '0.00',
                'nav_header'               => '0.00',
                'nav_footer'               => '0.00',
                'drop_down'                => '0',
                'content_type'             => '',
                'paged_content'            => '0',
                'translated_nav_label'     => 'La Salud y el Bienestar',
                'translated_sidebar_title' => ''
            )
        );


        $wave_nav_items_allowed = ['service_option_id','nav_slug','nav_parent_slug','nav_label','sidebar_title','url_path','nav_main','nav_header','nav_footer','drop_down','content_type','paged_content'];
        foreach($nav_items as $nav_item) {
            $nav_item_data = array_intersect_key($nav_item, array_flip($wave_nav_items_allowed));
            DB::table('wave_nav_items')->insert($nav_item_data);

            if(!empty($nav_item['translated_nav_label'])) {
                DB::table('translations')->insert([
                    'content_type' => 'nav',
                    'slug' => $nav_item['nav_slug'],
                    'language' => 'Spanish',
                    'translated_content' => $nav_item['translated_nav_label']
                ]);
            }
            if(!empty($nav_item['translated_sidebar_title'])) {
                DB::table('translations')->insert([
                    'content_type' => 'sidebar',
                    'slug' => $nav_item['nav_slug'],
                    'language' => 'Spanish',
                    'translated_content' => $nav_item['translated_sidebar_title']
                ]);
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('wave_nav_items');
    }

}
