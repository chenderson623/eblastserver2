<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('polls', function(Blueprint $table)
		{
			$table->increments('id');

            $table->text('question');
            $table->date('date_start')->index();
            $table->date('date_end')->index();

            $table->timestamps();
		});

        Schema::create('polls_locations_groups', function(Blueprint $table)
        {
            $table->integer('poll_id')->index();
            $table->integer('location_id')->index();
            $table->integer('group_id')->index();
        });

        Schema::create('poll_options', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('poll_id')->unsigned()->index();
            $table->string('option', 255);

            $table->timestamps();
        });

        Schema::create('poll_votes', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('poll_option_id')->unsigned()->index();
            $table->string('voter_ip', 25);

            $table->timestamps();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('polls');
        Schema::drop('polls_locations_groups');
        Schema::drop('poll_options');
        Schema::drop('poll_votes');
	}
}