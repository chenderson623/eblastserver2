<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCoupons extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        try{
            DB::update("ALTER TABLE `coupons` ADD `upc_image_file` VARCHAR(255) NOT NULL AFTER `beauty_image_filename`");

        }  catch(Exception $e) {
            //do nothing.
        }

	}

}
