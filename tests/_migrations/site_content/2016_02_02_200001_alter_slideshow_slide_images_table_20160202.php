<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSlideshowSlideImagesTable20160202 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::update("ALTER TABLE `slideshow_slide_images` ADD `directory` VARCHAR(255) NOT NULL AFTER `id`");

    }
}
