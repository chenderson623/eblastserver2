<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsOrderLocationsGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('departments_order_locations_groups', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('dept_slug', 25);
            $table->smallInteger('order');

            $table->integer('location_id')->index();
            $table->integer('group_id')->index();

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('departments_order_locations_groups');
	}
}