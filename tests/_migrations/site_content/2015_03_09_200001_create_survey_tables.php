<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('surveys', function(Blueprint $table)
		{
			$table->increments('id');

            $table->date('date_start')->index();
            $table->date('date_end')->index();
            $table->string('name',100);
            $table->text('copy');
            $table->boolean('include_contact_field')->default(1);

            $table->timestamps();
		});

        Schema::create('surveys_locations_groups', function(Blueprint $table)
        {
            $table->integer('survey_id')->index();
            $table->integer('location_id')->index();
            $table->integer('group_id')->index();
        });

        Schema::create('survey_answers', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('location_id')->unsigned()->index();
            $table->integer('survey_question_id')->unsigned()->index();
            $table->text('answer');
            $table->string('visitor_ip',25)->nullable();

            $table->timestamps();
        });

        Schema::create('survey_questions', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('survey_id')->unsigned()->index();
            $table->string('type', 25);
            $table->text('question');

            $table->timestamps();
        });



	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('surveys');
        Schema::drop('surveys_locations_groups');
        Schema::drop('survey_answers');
        Schema::drop('survey_questions');
	}
}