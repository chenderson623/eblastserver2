<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWaveNavItems2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        try{
            // change nav slugs for a couple of items
            //
            DB::update("ALTER TABLE `wave_nav_items` ADD `nav_mobile` DECIMAL(4,2) NOT NULL AFTER `nav_footer`");
            DB::update("ALTER TABLE `wave_nav_items` ADD `mobile_label` VARCHAR(25) NOT NULL AFTER `sidebar_title`");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `nav_mobile` = '1' WHERE `wave_nav_items`.`id` = 6");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `nav_mobile` = '2' WHERE `wave_nav_items`.`id` = 9");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `mobile_label` = 'Ads' WHERE `wave_nav_items`.`id` = 6");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `mobile_label` = 'Coupons' WHERE `wave_nav_items`.`id` = 9");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `mobile_label` = 'Rewards', `nav_mobile` = '3' WHERE `wave_nav_items`.`id` = 11");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `mobile_label` = 'Subscribe', `nav_mobile` = '5' WHERE `wave_nav_items`.`id` = 5");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `mobile_label` = 'Recipes', `nav_mobile` = '6' WHERE `wave_nav_items`.`id` = 7");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `mobile_label` = 'Location', `nav_mobile` = '4' WHERE `wave_nav_items`.`id` = 22");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `mobile_label` = 'About Us', `nav_mobile` = '10' WHERE `wave_nav_items`.`id` = 2");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `mobile_label` = 'Jobs', `nav_mobile` = '11' WHERE `wave_nav_items`.`id` = 4");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `mobile_label` = 'Events', `nav_mobile` = '8' WHERE `wave_nav_items`.`id` = 16");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `mobile_label` = 'Social Media', `nav_mobile` = '7' WHERE `wave_nav_items`.`id` = 20");

        }  catch(Exception $e) {
            //do nothing.
        }

	}

}
