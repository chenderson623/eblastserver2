<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  CREATE TABLE IF NOT EXISTS `feature_boxes` (
  `feature_box_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `box_name` varchar(55) NOT NULL COMMENT 'internal reference',
  `box_positions` int(1) unsigned NOT NULL DEFAULT '1' COMMENT 'how many positions wide',
  `width` int(3) unsigned NOT NULL DEFAULT '0' COMMENT 'pixels',
  `height` int(3) unsigned NOT NULL DEFAULT '0' COMMENT 'pixels',
  `feature_box_template_id` int(2) NOT NULL COMMENT 'template class for content',
  `feature_box_theme_id` int(2) unsigned NOT NULL DEFAULT '0',
  `content` text NOT NULL COMMENT 'optional for some box_class',
  `content_dir` varchar(50) DEFAULT NULL,
  `template_vars` text NOT NULL,
  `site_option` int(4) unsigned NOT NULL COMMENT 'if depends on site option, set here',
  `sites_table_option` varchar(50) NOT NULL,
  PRIMARY KEY (`feature_box_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30100 ;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_box_schedule`
  --

  CREATE TABLE IF NOT EXISTS `feature_box_schedule` (
  `schedule_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `feature_section_id` int(4) unsigned NOT NULL,
  `feature_box_id` int(5) unsigned NOT NULL,
  `box_template_vars` text NOT NULL COMMENT 'schedule-level overrides to feature_boxes record',
  `feature_box_theme_id` int(2) unsigned NOT NULL DEFAULT '0',
  `css_class` varchar(25) NOT NULL,
  `default_schedule` tinyint(1) unsigned DEFAULT '0',
  `wholesaler_schedule` tinyint(1) unsigned DEFAULT '0',
  `vendor_schedule` int(4) unsigned DEFAULT '0',
  `site_override` int(5) unsigned DEFAULT '0' COMMENT 'site_id for site specific overrides',
  `box_width` int(1) unsigned NOT NULL,
  `box_order` int(3) unsigned NOT NULL,
  `date_start` date NOT NULL,
  `date_stop` date NOT NULL,
  `exclude` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`schedule_id`),
  KEY `feature_section_id` (`feature_section_id`),
  KEY `feature_box_id` (`feature_box_id`),
  KEY `date_start` (`date_start`),
  KEY `date_stop` (`date_stop`),
  KEY `default_schedule` (`default_schedule`),
  KEY `wholesaler_schedule` (`wholesaler_schedule`),
  KEY `vendor_schedule` (`vendor_schedule`),
  KEY `site_override` (`site_override`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=203491 ;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_box_schedule_wholesaler`
  --

  CREATE TABLE IF NOT EXISTS `feature_box_schedule_wholesaler` (
  `schedule_id` int(6) unsigned NOT NULL,
  `site_id` int(6) unsigned NOT NULL,
  KEY `schedule_id` (`schedule_id`),
  KEY `site_id` (`site_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_box_templates`
  --

  CREATE TABLE IF NOT EXISTS `feature_box_templates` (
  `feature_box_template_id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `template_name` varchar(55) NOT NULL,
  `template_description` varchar(255) NOT NULL,
  `template_meta_fields` text NOT NULL,
  PRIMARY KEY (`feature_box_template_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_box_themes`
  --

  CREATE TABLE IF NOT EXISTS `feature_box_themes` (
  `feature_box_theme_id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `theme_name` varchar(55) NOT NULL,
  `theme_description` varchar(255) NOT NULL,
  `theme_meta_fields` text NOT NULL,
  PRIMARY KEY (`feature_box_theme_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_box_vendors`
  --

  CREATE TABLE IF NOT EXISTS `feature_box_vendors` (
  `vendor_id` int(4) unsigned NOT NULL,
  `vendor_name` varchar(25) NOT NULL,
  `vendor_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`vendor_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_changelog`
  --

  CREATE TABLE IF NOT EXISTS `feature_changelog` (
  `changelog_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `object_name` varchar(12) NOT NULL,
  `object_id` int(6) unsigned NOT NULL,
  `action` varchar(25) NOT NULL,
  `description` text NOT NULL,
  `data_json` text NOT NULL,
  `undo_json` text NOT NULL,
  `undone` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `parent_history_id` int(7) unsigned NOT NULL DEFAULT '0',
  `log_datetime` datetime NOT NULL,
  PRIMARY KEY (`changelog_id`),
  KEY `object_name` (`object_name`),
  KEY `object_id` (`object_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_default_boxes`
  --

  CREATE TABLE IF NOT EXISTS `feature_default_boxes` (
  `feature_default_id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `feature_box_id` int(5) unsigned NOT NULL,
  PRIMARY KEY (`feature_default_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_sections`
  --

  CREATE TABLE IF NOT EXISTS `feature_sections` (
  `feature_section_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `fill_default` tinyint(1) unsigned DEFAULT '1' COMMENT 'fill out slideshow with default boxes',
  `exclude_wholesaler` tinyint(1) unsigned DEFAULT '0' COMMENT 'always exclude the wholesaler boxes',
  `feature_box_theme_id` int(2) unsigned NOT NULL DEFAULT '0',
  `css_class` varchar(25) NOT NULL,
  PRIMARY KEY (`feature_section_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5811 ;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_sections_2_vendors`
  --

  CREATE TABLE IF NOT EXISTS `feature_sections_2_vendors` (
  `feature_section_id` int(4) unsigned NOT NULL,
  `vendor_id` int(4) unsigned NOT NULL,
  KEY `feature_section_id` (`feature_section_id`),
  KEY `vendor_id` (`vendor_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_tags`
  --

  CREATE TABLE IF NOT EXISTS `feature_tags` (
  `tag_id` int(4) NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) NOT NULL,
  PRIMARY KEY (`tag_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

  -- --------------------------------------------------------

  --
  -- Table structure for table `feature_tags_index`
  --

  CREATE TABLE IF NOT EXISTS `feature_tags_index` (
  `tagged_id` int(7) NOT NULL AUTO_INCREMENT,
  `object_name` varchar(25) NOT NULL,
  `object_id` int(6) unsigned NOT NULL,
  `tag_id` int(4) NOT NULL,
  PRIMARY KEY (`tagged_id`),
  KEY `object_name` (`object_name`),
  KEY `object_id` (`object_id`),
  KEY `tag_id` (`tag_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

 */

class CreateFeatureTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('feature_boxes', function(Blueprint $table) {
            $table->increments('id');

            $table->string('box_name');
            $table->tinyInteger('box_positions')->default(1)->comment = 'how many positions wide';
            $table->integer('feature_box_template_id')->default(0);
            $table->integer('feature_box_theme_id')->default(0)->index();
            $table->string('content_dir')->nullable();
            $table->text('template_vars')->nullable();
            $table->integer('service_option_id')->default(0);
            $table->string('location_meta_key')->nullable();

            $table->timestamps();
        });

        Schema::create('feature_schedules', function(Blueprint $table) {
            $table->increments('id');

            $table->integer('feature_section_id')->default(0)->index();
            $table->integer('wholesaler_id')->default(0)->index();
            $table->integer('location_override')->default(0)->index()->comment = 'location_id for location specific overrides';
            $table->integer('feature_box_id')->default(0)->index();
            $table->text('box_template_vars')->comment                         = 'schedule-level overrides to feature_boxes record';
            $table->string('css_class');
            $table->boolean('default_schedule')->default(0)->index();
            $table->tinyInteger('box_order')->default(0);
            $table->date('date_start')->index();
            $table->date('date_stop')->index();
            $table->boolean('exclude')->default(0);

            $table->timestamps();
        });

        Schema::create('feature_schedule_wholesaler_locations', function(Blueprint $table) {
            $table->integer('feature_schedule_id', false, false)->nullable()->index();
            $table->integer('location_id', false, false)->nullable()->index();
            $table->text('box_template_vars');

            $table->timestamps();

            $table->unique(['feature_schedule_id','location_id'], 'unique_schedule_locations');
        });

        Schema::create('feature_box_templates', function(Blueprint $table) {
            $table->increments('id');

            $table->integer('feature_box_theme_id')->default(0);
            $table->string('template_name');
            $table->string('template_description');
            $table->text('template_meta_fields');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('feature_box_themes', function(Blueprint $table) {
            $table->increments('id');

            $table->string('theme_name');
            $table->string('theme_description');
            $table->string('theme_css_name');
            $table->smallInteger('width')->default(0)->comment       = 'pixels';
            $table->smallInteger('height')->default(0)->comment      = 'pixels';
            $table->string('theme_pattern');
            $table->text('theme_meta_fields');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('feature_changelog', function(Blueprint $table) {
            $table->increments('id');

            $table->string('object_name')->index();
            $table->integer('object_id')->index();
            $table->string('action');
            $table->string('description');
            $table->text('data_json');
            $table->text('undo_json');
            $table->boolean('undone')->default(0);
            $table->integer('parent_history_id')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('feature_sections', function(Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('description');
            $table->boolean('fill_default')->default(1)->comment       = 'fill out slideshow with default boxes';
            $table->boolean('exclude_wholesaler')->default(0)->comment = 'always exclude the wholesaler boxes';
            $table->integer('feature_box_theme_id')->default(0)->index();
            $table->string('css_class');
            $table->string('order_strategy_class');
            $table->string('box_factory_class');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('feature_sections_locations_groups', function(Blueprint $table)
        {
            $table->integer('feature_section_id')->index();
            $table->integer('location_id')->nullable()->index()->unique();
            $table->integer('group_id')->nullable()->index()->unique();
        });

        Schema::create('feature_tags', function(Blueprint $table) {
            $table->increments('id');
            $table->string('tag');
        });

        Schema::create('feature_tags_index', function(Blueprint $table) {
            $table->increments('id');

            $table->string('object_name')->index();
            $table->integer('object_id')->index();
            $table->integer('tag_id')->index();

            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('feature_box_themes')->insert([
            'id' => 100,
            'theme_name' => "300-wide",
            'theme_description' => "default wave theme",
            'theme_css_name' => "width-300px",
            'theme_pattern' => "300pxFeatureBoxSetBoxPattern",
            'width' => 300,
            'height' => 250,
            'theme_meta_fields' => "{}"
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('feature_boxes');
        Schema::drop('feature_schedule');
        Schema::drop('feature_schedule_wholesaler_locations');
        Schema::drop('feature_box_templates');
        Schema::drop('feature_box_themes');
        Schema::drop('feature_changelog');
        Schema::drop('feature_sections');
        Schema::drop('feature_sections_locations_groups');
        Schema::drop('feature_tags');
        Schema::drop('feature_tags_index');
    }

}
