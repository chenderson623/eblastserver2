<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropEblastCouponTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (Schema::hasTable('eblasts')) {
            Schema::drop('eblasts');
        }

        if (Schema::hasTable('eblast_coupons')) {
            Schema::drop('eblast_coupons');
        }

        if (Schema::hasTable('eblast_locations_groups')) {
            Schema::drop('eblast_locations_groups');
        }

    }
}
