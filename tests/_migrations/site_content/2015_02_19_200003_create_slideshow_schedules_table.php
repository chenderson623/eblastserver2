<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideshowSchedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slideshow_schedules', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('slideshow_slide_id', false, false);
            $table->integer('slideshow_id', false, false)->index();
            $table->boolean('default_schedule')->default(0)->index();
            $table->boolean('wholesaler_schedule')->default(0)->index();
            $table->integer('site_override')->default(0);
            $table->smallInteger('slide_order', false, false);
            $table->date('date_start')->index();
            $table->date('date_stop')->index();
            $table->boolean('exclude')->default(0);

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slideshow_schedules');
	}

}
