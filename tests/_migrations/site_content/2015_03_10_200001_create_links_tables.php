<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('links', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('grouping', 50);
            $table->string('name', 100);
            $table->string('file_name', 100);
            $table->string('url', 255);

            $table->timestamps();
		});

        Schema::create('links_locations_groups', function(Blueprint $table)
        {
            $table->integer('link_id')->index();
            $table->integer('location_id')->index();
            $table->integer('group_id')->index();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('links');
        Schema::drop('links_locations_groups');
	}
}