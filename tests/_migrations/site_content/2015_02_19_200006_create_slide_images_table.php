<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slideshow_slide_images', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('filename', 50);
            $table->string('name', 50);
            $table->string('description')->nullable();
            $table->string('original_filename', 50);

			$table->timestamps();
			$table->softDeletes();
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slideshow_slide_images');
	}
}