<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdsTable20160107 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::update("ALTER TABLE `ads` DROP `name`");
        DB::update("ALTER TABLE `ads` CHANGE `display_name` `title` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
        DB::update("ALTER TABLE `ads` ADD `description` TEXT NULL DEFAULT NULL AFTER `title`");

    }
}
