<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEventsTable20160105 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::update("ALTER TABLE `events` DROP `description_plain`");
        DB::update("ALTER TABLE `events` CHANGE `description_html` `content` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");
        DB::update("ALTER TABLE `events` CHANGE `name` `title` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");

        DB::update("ALTER TABLE `events` CHANGE `date_start` `start_date` DATE NOT NULL");
        DB::update("ALTER TABLE `events` CHANGE `date_end` `end_date` DATE NOT NULL");

    }
}
