<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideWholesalerScheduleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slideshow_wholesaler_schedules', function(Blueprint $table)
		{
			$table->integer('slideshow_schedule_id', false, false);
            $table->integer('location_id', false, false);

			$table->timestamps();
			$table->softDeletes();
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slideshow_wholesaler_schedules');
	}
}