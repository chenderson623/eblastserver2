<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideshowSlidesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slideshow_slides', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('slideshow_slide_image_id', false, false);

            $table->string('name', 50);
            $table->string('title', 100);
            $table->text('content');

            $table->string('translated_title', 100);
            $table->text('translated_content')->nullable();

            $table->text('url')->nullable();
            $table->boolean('url_new_window')->default(1);

            $table->string('price', 25)->nullable();

			$table->timestamps();
			$table->softDeletes();
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slideshow_slides');
	}

}
