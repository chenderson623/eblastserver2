<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDummyDomainsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('solutions_center_domains', function(Blueprint $table)
		{
			$table->increments('id');

            $table->text('notes');

            $table->timestamps();
		});

        DB::table('solutions_center_domains')->insert([
            'notes' => 'Do not delete this table. This is a dummy table so that domains associations will work. No data is needed in this table.'
            ]);


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('solutions_center_domains');
	}
}