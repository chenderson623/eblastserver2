<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWaveNavItems3 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        try{
            // change nav slugs for a couple of items
            //
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `url_path` = '/coupons/coupons_com' WHERE `wave_nav_items`.`id` = 24");

        }  catch(Exception $e) {
            //do nothing.
        }

	}

}
