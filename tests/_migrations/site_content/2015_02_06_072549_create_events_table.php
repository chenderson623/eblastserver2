<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 50);
			$table->text('description_plain');
			$table->text('description_html');
			$table->string('event_time', 50);
			$table->date('date_start')->index();
			$table->date('date_end')->index();
			$table->boolean('default_event')->default(0)->index();
			$table->timestamps();
			$table->softDeletes();
		});
		Schema::create('events_locations_groups', function(Blueprint $table)
		{
			$table->integer('event_id')->index();
			$table->integer('location_id')->index();
			$table->integer('group_id')->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
		Schema::drop('events_locations_groups');
	}

}
