<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNewsTable20151223 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::update("ALTER TABLE `news` CHANGE `start_date` `start_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'");
        DB::update("ALTER TABLE `news` CHANGE `end_date` `end_date` DATETIME NULL DEFAULT NULL");

    }
}
