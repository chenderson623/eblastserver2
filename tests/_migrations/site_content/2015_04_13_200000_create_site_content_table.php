<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteContentTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('site_content', function (Blueprint $table) {

            $table->increments('id');

            $table->string('content_type', 50)->index();
            $table->boolean('default')->default(false)->index();
            $table->string('slug', 50)->index();
            $table->string('title', 100);
            $table->text('content');

            $table->date('start_date')->index();
            $table->date('end_date')->index();

            $table->string('directory');
            $table->string('image_filename');

            $table->timestamps();
        });

        Schema::create('site_content_locations_groups', function(Blueprint $table)
        {
            $table->integer('site_content_id')->index();
            $table->integer('location_id')->index();
            $table->integer('group_id')->index();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('site_content');
        Schema::drop('site_content_locations_groups');
    }
}
