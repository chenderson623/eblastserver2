<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWaveNavItems extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        try{
            // change nav slugs for a couple of items
            //
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `nav_slug` = 'online_coupons' WHERE `wave_nav_items`.`id` = 9");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `nav_slug` = 'ads' WHERE `wave_nav_items`.`id` = 6");
            DB::update("UPDATE `site_content`.`wave_nav_items` SET `url_path` = '/health_wellness/' WHERE `wave_nav_items`.`id` = 8");

        }  catch(Exception $e) {
            //do nothing.
        }

	}

}
