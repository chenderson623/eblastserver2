<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlideTagsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slideshow_tags', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('tag', 50);

			$table->timestamps();
			$table->softDeletes();
		});

        Schema::create('slideshow_tags_index', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('object', 50);
            $table->integer('object_id', false, false);
            $table->integer('slideshow_tag_id', false, false);

            $table->timestamps();
            $table->softDeletes();
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slideshow_tags');
        Schema::drop('slideshow_tags_index');
	}
}