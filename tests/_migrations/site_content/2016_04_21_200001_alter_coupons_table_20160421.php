<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCouponsTable20160421 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (!Schema::hasColumn('coupons', 'product_image_directory')) {
            DB::update("ALTER TABLE `coupons` ADD `product_image_directory` VARCHAR(255) NOT NULL AFTER `redemption_value`;");
            DB::connection()->getPdo()->exec("UPDATE `coupons` SET `product_image_directory`=`dir_name`");
        }

        if (!Schema::hasColumn('coupons', 'beauty_image_directory')) {
            DB::update("ALTER TABLE `coupons` ADD `beauty_image_directory` VARCHAR(255) NOT NULL AFTER `product_image_filename`;");
            DB::connection()->getPdo()->exec("UPDATE `coupons` SET `beauty_image_directory`=`dir_name`");
        }

        if (Schema::hasColumn('coupons', 'upc_image_file')) {
            DB::update("ALTER TABLE `coupons` CHANGE `upc_image_file` `upc_image_filename` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL");
        }

        if (!Schema::hasColumn('coupons', 'upc_image_directory')) {
            DB::update("ALTER TABLE `coupons` ADD `upc_image_directory` VARCHAR(255) NOT NULL AFTER `beauty_image_filename`");
            DB::connection()->getPdo()->exec("UPDATE `coupons` SET `upc_image_directory`=`dir_name` WHERE `upc_image_filename` != ''");
        }

        if (Schema::hasColumn('coupons', 'dir_name')) {
            DB::update("ALTER TABLE `coupons` DROP `dir_name`;");
        }
    }
}
