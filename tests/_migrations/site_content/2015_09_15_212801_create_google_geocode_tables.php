<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoogleGeocodeTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('google_geocode_cache', function(Blueprint $table)
		{
			$table->increments('id');

  			$table->string('query', 255);
  			$table->string('address', 255)->nullable();
  			$table->string('city', 255)->nullable();
  			$table->string('state', 2)->nullable();
  			$table->string('zip_code', 5)->nullable();
  			$table->string('locality_level', 15)->nullable();
			$table->decimal('latitude', 10, 8)->nullable();
			$table->decimal('longitude', 11, 8)->nullable();
  			$table->string('formatted_name', 255);
  			$table->text('response_text');
  			$table->smallInteger('hits');

  			$table->datetime('created_at');
		});

		Schema::create('google_geocode_lookups', function(Blueprint $table)
		{
			$table->increments('id');

  			$table->integer('google_geocode_cache_id');
  			$table->string('query', 255);
  			$table->string('url', 255);
  			$table->integer('location_id');
  			$table->integer('group_id');
  			$table->string('user_agent', 100);
  			$table->string('ip_address', 25);

  			$table->datetime('created_at');

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('google_geocode_cache');
		Schema::drop('google_geocode_lookups');
	}

}
