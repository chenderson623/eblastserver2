<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServicesTable20151020 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (!Schema::hasColumn('services', 'signup')) {
            DB::update("ALTER TABLE `services` ADD `signup` TEXT NOT NULL AFTER `description`");
        }

        if (!Schema::hasColumn('services', 'pricing_codes')) {
            DB::update("ALTER TABLE `services` ADD `pricing_codes` TEXT NOT NULL AFTER `signup`");
        }

        if (!Schema::hasColumn('services', 'optional_pricing')) {
            DB::update("ALTER TABLE `services` ADD `optional_pricing` TEXT NOT NULL AFTER `pricing_codes`");
        }
    }

    public function down() {
    }

}
