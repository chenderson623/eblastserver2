<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDomainsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('domains', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('url')->index();
			$table->integer('domain_type_id')->index();
			$table->integer('alias_id')->index();
			$table->integer('group_id')->index();
			$table->integer('location_id')->index();

			$table->boolean('approved')->default(false);
			$table->boolean('preview')->default(false);

			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('domains');
	}

}
