<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (!Schema::hasTable('notes')) {
            Schema::create('notes', function (Blueprint $table) {
                $table->increments('id');
				$table->integer('user_id')->index();
				$table->integer('public')->default(0)->index();
				$table->string('note');
				$table->timestamps();
				$table->softDeletes();
            });
        }
        if (!Schema::hasTable('notables')) {
            Schema::create('notables', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('note_id')->index();
                $table->integer('notable_id')->index();
                $table->string('notable_type')->index();
				$table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }
}
