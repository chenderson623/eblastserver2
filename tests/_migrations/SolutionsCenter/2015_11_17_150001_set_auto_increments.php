<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SetAutoIncrements extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        $statements = [
            "ALTER TABLE `clients` AUTO_INCREMENT = 200000",
            "ALTER TABLE `comments` AUTO_INCREMENT = 20000",
            "ALTER TABLE `general_requests` AUTO_INCREMENT = 3000",
            "ALTER TABLE `locations` AUTO_INCREMENT = 200000",
            "ALTER TABLE `service_requests` AUTO_INCREMENT = 300000 ",
            "ALTER TABLE `users` AUTO_INCREMENT = 2000",
            "ALTER TABLE `service_request_ads` AUTO_INCREMENT = 150000",
            "ALTER TABLE `service_request_eblast_coupons` AUTO_INCREMENT = 150000",
            "ALTER TABLE `service_request_events` AUTO_INCREMENT = 150000",
            "ALTER TABLE `service_request_jobs` AUTO_INCREMENT = 150000",
            "ALTER TABLE `service_request_links` AUTO_INCREMENT = 150000",
            "ALTER TABLE `service_request_news_items` AUTO_INCREMENT = 150000",
            "ALTER TABLE `service_request_online_coupons` AUTO_INCREMENT = 150000",
            "ALTER TABLE `service_request_printable_coupons` AUTO_INCREMENT = 150000",
            "ALTER TABLE `service_request_site_contents` AUTO_INCREMENT = 150000",
            "ALTER TABLE `service_request_slides` AUTO_INCREMENT = 150000",
        ];

        foreach($statements as $statement) {
            DB::unprepared($statement);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
    }
}
