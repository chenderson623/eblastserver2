<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyServiceRequestCouponables20150509 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::table('service_request_couponables', function($table) {
            $table->date('offer_start_date')->after('service_request_couponable_id');
            $table->date('offer_end_date')->after('offer_start_date');
            $table->smallInteger('coupon_valid_days')->after('offer_end_date');
        });

    }

    public function down() {
    }
}
