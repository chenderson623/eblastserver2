<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStatusHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('status_history', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('service_request_id');
			$table->integer('status_id');
			$table->integer('assignee_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('status_history');
	}

}
