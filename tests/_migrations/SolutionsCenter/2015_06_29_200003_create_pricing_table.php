<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('pricing', function(Blueprint $table) {
            $table->increments('id');

            $table->string('code', 50);
            $table->string('item', 100);
            $table->string('nice_label', 100);
            $table->text('description');
            $table->integer('taxable');
            $table->decimal('price',6,2);
            $table->string('unit', 50);
            $table->decimal('special_price',6,2);
			$table->dateTime('special_start');
			$table->dateTime('special_end');
            $table->integer('active');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('pricing');
    }

}
