<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DefaultAlertTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

         $alertTypes = [
            'Home Page Only',
            'Site Wide',
            'Group',
            'Module',
            'Client',
            'Location'
        ];

        foreach($alertTypes as $type)
        {
            $alert = AlertType::create([
                'name' => $type
            ]);
        }

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
