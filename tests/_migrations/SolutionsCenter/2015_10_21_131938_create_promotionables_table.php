<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionablesTable extends Migration {

    public function up() {
        Schema::create('promotionables', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('promotion_id');
            $table->integer('promotionable_id');
            $table->string('promotionable_type');
        });
    }

    public function down() {
        Schema::drop('promotionables');
    }

}
