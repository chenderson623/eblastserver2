<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPromotionsTable20151210 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (Schema::hasColumn('promotions', 'start_time')) {
            DB::update("ALTER TABLE `promotions` CHANGE `start_time` `start_date` DATE NOT NULL");
        }

        if (Schema::hasColumn('promotions', 'end_time')) {
            DB::update("ALTER TABLE `promotions` CHANGE `end_time` `end_date` DATE NOT NULL");
        }
    }

    public function down() {
    }

}
