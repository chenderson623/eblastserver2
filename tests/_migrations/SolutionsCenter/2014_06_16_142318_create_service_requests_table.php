<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServiceRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_requests', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('admin_user_id');
			$table->integer('assignee_id');
			$table->boolean('archived')->default(null);
			$table->boolean('rush')->default(false);
			$table->integer('service_request_status_id')->default(1);
			$table->boolean('service_request_approval_id')->default(1);
			$table->string('viewMode')->default('client');
			$table->integer('group_id')->default(null);
			$table->dateTime('start_time');
			$table->dateTime('end_time');
			$table->morphs('requestable');
			$table->text('additional_comments');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_requests');
	}

}
