<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyNotifications20151208 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

		Schema::table('notifications', function($table){
			if(!Schema::hasColumn('notifications', 'service_request_id')){
				$table->integer('service_request_id')->after('user_id');
			}
		    $table->dropColumn(array('notification_type_id', 'content', 'is_viewed', 'start_at', 'end_at'));
		});

		Schema::drop('notification_types');

    }

    public function down() {
    }

}
