<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServiceRequestCouponsTable20160421 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::update("ALTER TABLE `service_request_coupons` ADD `product_image_directory` VARCHAR(255) NULL DEFAULT NULL AFTER `product_image_description`;");
        DB::update("ALTER TABLE `service_request_coupons` ADD `beauty_image_directory` VARCHAR(255) NULL DEFAULT NULL AFTER `beauty_image_description`;");
        DB::update("ALTER TABLE `service_request_coupons` ADD `upc_image_file` VARCHAR(255) NULL DEFAULT NULL AFTER `beauty_image_filename`");
        DB::update("ALTER TABLE `service_request_coupons` ADD `upc_image_directory` VARCHAR(255) NULL DEFAULT NULL AFTER `beauty_image_filename`");
        DB::update("ALTER TABLE `service_request_coupons` CHANGE `upc_image_file` `upc_image_filename` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
        DB::update("ALTER TABLE `service_request_coupons` DROP `upload_directory`;");
    }

    public function down() {
    }

}
