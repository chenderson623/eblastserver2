<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceRequestEblastTables extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('service_request_eblasts', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('eblast_id');

            $table->date('display_start_date');
            $table->date('display_end_date');
            $table->date('offer_start_date');
            $table->date('offer_end_date');
            $table->date('blast_date');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_coupons', function(Blueprint $table) {
            $table->increments('id');

            $table->string('title', 100);
            $table->string('price', 100);
            $table->text('description')->nullable();
            $table->text('disclaimer')->nullable();
            $table->string('plu', 15)->nullable();
            $table->string('redemption_value', 15)->nullable();

            $table->string('product_image_description');
            $table->string('product_image_filename');

            $table->string('beauty_image_description');
            $table->string('beauty_image_filename');

            $table->string('upload_directory');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_couponables', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('service_request_coupon_id')->index('coupon_id_index');
            $table->string('service_request_couponable_type')->index('couponable_type_index');
            $table->integer('service_request_couponable_id')->index('couponable_id_index');
            $table->timestamps();
        });


    }

    public function down() {
    }
}
