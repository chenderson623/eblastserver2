<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyServiceOptions20151204 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        $update = <<<EOD
UPDATE `SolutionsCenter`.`service_options` SET `meta_def` = '{"contact_form_email":{"label":"Contact Form Email","type":"text","options":{"help":"Store''s email address that contact form submissions are sent to. Comma separate multiple emails"}}}' WHERE `service_options`.`id` = 39;
EOD;

            DB::update($update);
    }

    public function down() {
    }

}
