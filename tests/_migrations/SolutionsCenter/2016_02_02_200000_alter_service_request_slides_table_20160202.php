<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServiceRequestSlidesTable20160202 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {\
        DB::update("ALTER TABLE `service_request_slides` ADD `slideshow_schedule_id` INT(11) NOT NULL AFTER `request_type`");
        DB::update("ALTER TABLE `service_request_slides` ADD `slideshow_slide_image_id` INT(11) NOT NULL AFTER `slideshow_slide_id`");
    }

    public function down() {
    }

}
