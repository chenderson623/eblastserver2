<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DefaultPhoneTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        $types = array(
            array(
            'name'=> 'Primary'
            ),
            array(
            'name'=> 'Land Line',
            ),
            array(
            'name'=> 'Cell',
            ),
            array(
            'name'=> 'Fax',
            ),
        );

        DB::table('phone_types')->insert( $types );

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
