<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DefaultLocationTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        $types = [
            'Store',
            'Mall',
            'Convention',
            'Concert'
        ];

        foreach($types as $type) {
            LocationType::create(['name' => $type]);
        }

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
