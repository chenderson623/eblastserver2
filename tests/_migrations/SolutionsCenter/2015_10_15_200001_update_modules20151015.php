<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateModules20151015 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        //change path:
        DB::update("UPDATE `SolutionsCenter`.`modules` SET `path` = 'module/adupload', `deleted_at` = NULL WHERE `modules`.`id` = 1");

	}

    public function down() {
    }

}
