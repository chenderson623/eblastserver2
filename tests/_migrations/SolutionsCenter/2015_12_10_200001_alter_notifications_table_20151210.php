<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNotificationsTable20151210 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

		Schema::table('notifications', function($table){
			if(!Schema::hasColumn('notifications', 'service_request_id')){
				$table->string('action',20)->after('service_request_id');
			}
		    $table->dropColumn(array('title'));
		});

    }

    public function down() {
    }

}
