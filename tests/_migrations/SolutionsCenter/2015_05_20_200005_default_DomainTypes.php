<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DefaultDomainTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        $domainTypes = [
            'Wave',
            'Landing',
            'Brochure',
            'Custom',
            'Alias'
        ];

        foreach($domainTypes as $domain)
        {
            DomainType::create([
                'name' => $domain
            ]);
        }

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
