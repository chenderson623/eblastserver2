<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdPages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

        if (Schema::hasTable('site_content.ad_pages')) {
            Schema::drop('site_content.ad_pages');
        }

        Schema::create('site_content.ad_pages', function(Blueprint $table){
          $table->increments('id');
          $table->mediumInteger('ad_id');
          $table->mediumInteger('template_id');
          $table->tinyInteger('page_order');
          $table->string('title');
          $table->string('bg_color',50)->nullable();
          $table->tinyInteger('border_radius')->nullable();
          $table->timestamps();
          $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
        Schema::drop('site_content.ad_pages');
	}

}
