<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServiceRequestJobsTable20160204 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {\
        DB::update("ALTER TABLE `service_request_jobs` CHANGE `upload_directory` `image_directory` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `image_description`");
        DB::update("ALTER TABLE `service_request_jobs` ADD `job_app_directory` VARCHAR(255) NOT NULL AFTER `image_filename`");
        DB::update("ALTER TABLE `service_request_jobs` CHANGE `job_id` `site_content_id` INT(11) NOT NULL");
    }

    public function down() {
    }

}
