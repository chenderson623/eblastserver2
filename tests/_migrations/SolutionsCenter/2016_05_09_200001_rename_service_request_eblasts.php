<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameServiceRequestEblasts extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (Schema::hasTable('service_request_eblasts')) {
            Schema::rename('service_request_eblasts', 'service_request_eblast_coupons');
        }
    }

    public function down() {
    }
}
