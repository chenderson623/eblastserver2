<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyServiceRequestEblastCoupons20150509 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (Schema::hasColumn('service_request_eblast_coupons', 'offer_start_date')) {
            Schema::table('service_request_eblast_coupons', function($table) {
                $table->dropColumn('offer_start_date');
            });
        }

        if (Schema::hasColumn('service_request_eblast_coupons', 'offer_end_date')) {
            Schema::table('service_request_eblast_coupons', function($table) {
                $table->dropColumn('offer_end_date');
            });
        }

    }

    public function down() {
    }
}
