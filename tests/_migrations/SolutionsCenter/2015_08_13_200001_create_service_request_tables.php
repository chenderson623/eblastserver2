<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceRequestTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('service_request_eblast_coupons', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('eblast_coupon_id');

            $table->date('display_start_date');
            $table->date('display_end_date');
            $table->date('offer_start_date');
            $table->date('offer_end_date');
            $table->date('blast_start_date');

            $table->string('title', 100);
            $table->string('price', 100);
            $table->text('description')->nullable();
            $table->text('disclaimer')->nullable();
            $table->string('plu', 15)->nullable();
            $table->string('redemption_value', 15)->nullable();

            $table->string('product_image_description');
            $table->string('product_image_filename');

            $table->string('beauty_image_description');
            $table->string('beauty_image_filename');

            $table->string('upload_directory');

            $table->text('changed');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_site_contents', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('site_content_id');

            $table->string('content_type', 50);
            $table->string('slug', 50);
            $table->string('title', 100);
            $table->text('content');

            $table->string('translated_title', 100);
            $table->text('translated_content');

            $table->date('start_date');
            $table->date('end_date');

            $table->string('image_description');
            $table->string('image_filename');
            $table->string('upload_directory');

            $table->text('changed');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_links', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('link_id');

            $table->string('title', 100);
            $table->string('url', 255);

            $table->string('translated_title', 100);

            $table->string('grouping', 50);

            $table->date('start_date');
            $table->date('end_date');

            $table->string('uploaded_filename');
            $table->string('upload_directory');

            $table->text('changed');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_news_items', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('news_id');

            $table->date('start_date');
            $table->date('end_date')->nullable();

            $table->string('title');
            $table->text('content');

            $table->string('translated_title', 100);
            $table->text('translated_content');

            $table->string('image_description');
            $table->string('image_filename');
            $table->string('upload_directory');

            $table->text('changed');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_slides', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('slideshow_slide_id');

            $table->string('title', 100);
            $table->text('content');

            $table->string('translated_title', 100);
            $table->text('translated_content')->nullable();

            $table->string('url', 100)->nullable();
            $table->boolean('url_new_window')->default(1);

            $table->string('price', 25)->nullable();

            $table->date('start_date');
            $table->date('end_date');

            $table->smallinteger('display_order');

            $table->string('image_description');
            $table->string('image_filename');
            $table->string('upload_directory');

            $table->text('changed');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_events', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('event_id');

            $table->string('title', 100);
            $table->text('content');

            $table->string('translated_title', 100);
            $table->text('translated_content')->nullable();

            $table->string('event_time', 50);

            $table->date('start_date');
            $table->date('end_date');

            $table->boolean('default_event')->default(0);

            $table->text('changed');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_jobs', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('job_id');

            $table->date('start_date');
            $table->date('end_date');

            $table->string('title');
            $table->text('content');

            $table->string('translated_title', 100);
            $table->text('translated_content');

            $table->string('image_description');
            $table->string('image_filename');
            $table->string('job_app_filename');
            $table->string('upload_directory');

            $table->text('changed');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_ads', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('ad_id');

            $table->date('display_start_date');
            $table->date('display_end_date');
            $table->date('offer_start_date');
            $table->date('offer_end_date');
            $table->date('blast_date');

            $table->string('title');
            $table->text('description');
            $table->tinyInteger('pages', false, false);
            $table->string('labels', 50)->nullable();

            $table->string('translated_title', 100);

            $table->string('upload_type', 25);
            $table->string('upload_directory');

            $table->text('changed');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_online_coupons', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('online_coupon_id');

            $table->date('display_start_date');
            $table->date('display_end_date');
            $table->date('offer_start_date');
            $table->date('offer_end_date');

            $table->string('title', 100);
            $table->string('price', 100);
            $table->text('description')->nullable();
            $table->text('disclaimer')->nullable();
            $table->string('plu', 15)->nullable();
            $table->string('redemption_value', 15)->nullable();

            $table->string('product_image_description');
            $table->string('product_image_filename');

            $table->string('beauty_image_description');
            $table->string('beauty_image_filename');

            $table->string('upload_directory');

            $table->text('changed');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('service_request_printable_coupons', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('printable_coupon_id');

            $table->date('display_start_date');
            $table->date('display_end_date');

            $table->string('image_filename');
            $table->string('upload_directory');

            $table->text('changed');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('service_request_eblast_coupons');
        Schema::drop('service_request_site_contents');
        Schema::drop('service_request_links');
        Schema::drop('service_request_news_items');
        Schema::drop('service_request_slides');
        Schema::drop('service_request_events');
        Schema::drop('service_request_jobs');
        Schema::drop('service_request_ads');
        Schema::drop('service_request_online_coupons');
        Schema::drop('service_request_printable_coupons');

    }

}
