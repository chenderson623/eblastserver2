<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyAddTextModules extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {


        $module = DB::table('modules')->where('id',38)->get();
        if(empty($module))
            DB::table('modules')->insert(['id'=>38, 'service_option_id'=>65, 'name'=>'TextDeals Instant Coupons', 'description'=>'Create and manage TextDeals instant and subscriber coupons', 'pricing_codes'=>'45010-5,45030-0', 'path'=>'module/textdeals/coupons', 'adminable'=>1, 'groupable'=>1, 'locationable'=>1, 'sort_order'=>2]);

        $module = DB::table('modules')->where('id',39)->get();
        if(empty($module))
            DB::table('modules')->insert(['id'=>39, 'service_option_id'=>65, 'name'=>'TextDeals Subkeywords', 'description'=>'Create and manage TextDeals subkeywords', 'pricing_codes'=>'45010-5,45030-0', 'path'=>'module/textdeals/subkeywords', 'adminable'=>1, 'groupable'=>0, 'locationable'=>0, 'sort_order'=>4]);

        $module = DB::table('modules')->where('id',40)->get();
        if(empty($module))
            DB::table('modules')->insert(['id'=>40, 'service_option_id'=>15, 'name'=>'TextDeals Subscriber Offers', 'description'=>'Create and manage TextDeals welcome offers for new subscribers to your alerts.', 'pricing_codes'=>'45010-5,45030-0', 'path'=>'module/textdeals/subscribers', 'adminable'=>1, 'groupable'=>1, 'locationable'=>1, 'sort_order'=>2]);

        $module = DB::table('service_options')->where('id',65)->get();
        if(empty($module))
            DB::table('modules')->insert(['id'=>65, 'service_id'=>3, 'name'=>'Text Instant Coupons', 'description'=>'', 'slug'=>'text_coupons', 'meta'=>'{}', 'groupable'=>1, 'locationable'=>1]);

        $module = DB::table('serviceables')->where('service_id',3)->where('serviceable_id',65)->where('serviceable_type','ServiceOption')->get();
        if(empty($module))
            DB::table('modules')->insert(['service_id'=>3, 'serviceable_id'=>65, 'serviceable_type'=>'ServiceOption']);

    }

    public function down() {
    }
}
