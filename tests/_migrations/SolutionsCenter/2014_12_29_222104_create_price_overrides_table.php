<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePriceOverridesTable extends Migration {

    public function up() {
        Schema::create('price_overrides', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('overrideable_id')->unsigned()->index();
            $table->string('overrideable_type', 7);
            $table->decimal('price', 6, 2);
            $table->datetime('start');
            $table->datetime('end');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down() {
        Schema::drop('price_overrides');
    }

}