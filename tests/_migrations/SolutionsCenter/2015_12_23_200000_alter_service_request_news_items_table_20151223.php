<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServiceRequestNewsItemsTable20151223 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {\
        DB::update("ALTER TABLE `service_request_news_items` CHANGE `start_date` `start_date` DATETIME NULL DEFAULT NULL");
        DB::update("ALTER TABLE `service_request_news_items` CHANGE `end_date` `end_date` DATETIME NULL DEFAULT NULL");
    }

    public function down() {
    }

}
