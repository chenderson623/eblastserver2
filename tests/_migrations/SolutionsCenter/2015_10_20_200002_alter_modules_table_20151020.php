<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterModulesTable20151020 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (!Schema::hasColumn('modules', 'instructions')) {
            DB::update("ALTER TABLE `modules` ADD `instructions` TEXT NOT NULL AFTER `description`");
        }

        if (!Schema::hasColumn('modules', 'pricing_codes')) {
            DB::update("ALTER TABLE `modules` ADD `pricing_codes` TEXT NOT NULL AFTER `instructions`");
        }
    }

    public function down() {
    }

}
