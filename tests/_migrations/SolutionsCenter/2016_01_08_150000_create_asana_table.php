<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsanaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        if (!Schema::hasTable('asana')) {
            Schema::create('asana', function (Blueprint $table) {
                $table->increments('id');
				$table->string('name');
				$table->string('task');
				$table->text('subtasks');
				$table->timestamps();
            });
        }
		Schema::table('service_requests', function($table){
			if (!Schema::hasColumn('service_requests', 'asana_id')) {
					$table->string('asana_id')->after('requestable_type');

			}
		});
    }

    public function down() {
    }

}
