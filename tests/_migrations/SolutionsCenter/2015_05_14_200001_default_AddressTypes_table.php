<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DefaultAddressTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        $types = array(
            array(
            'name'=> 'Primary'
            ),
            array(
            'name'=> 'Street',
            ),
            array(
            'name'=> 'Mailing',
            ),
            array(
            'name'=> 'Billing',
            ),
        );

        DB::table('address_types')->insert( $types );

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
