<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('groups', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('group_type_id');
			$table->string('name');
			$table->string('display_name');
			$table->boolean('brochure')->default(false);
			$table->integer('brochure_id');
			$table->integer('landing_id');
			$table->integer('hourly_rate');
			$table->integer('sort_weight');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('groups');
	}

}
