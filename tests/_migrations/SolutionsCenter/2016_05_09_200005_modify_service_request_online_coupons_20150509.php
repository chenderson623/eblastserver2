<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyServiceRequestOnlineCoupons20150509 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::table('service_request_online_coupons', function($table) {

            $table->dropColumn('offer_start_date');
            $table->dropColumn('offer_end_date');
            $table->dropColumn('title');
            $table->dropColumn('price');
            $table->dropColumn('description');
            $table->dropColumn('disclaimer');
            $table->dropColumn('plu');
            $table->dropColumn('redemption_value');
            $table->dropColumn('product_image_description');
            $table->dropColumn('product_image_filename');
            $table->dropColumn('beauty_image_description');
            $table->dropColumn('beauty_image_filename');
            $table->dropColumn('upload_directory');
            $table->dropColumn('changed');

        });

    }

    public function down() {
    }
}
