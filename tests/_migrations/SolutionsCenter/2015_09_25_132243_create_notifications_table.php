<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('notification_type_id')->unsigned()->index();
            $table->string('title', 255);
            $table->text('content');
            $table->boolean('is_active')->unsigned()->default(1);
            $table->boolean('is_viewed')->unsigned()->default(1);
            $table->datetime('viewed_at');
            $table->datetime('start_at');
            $table->datetime('end_at');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}
