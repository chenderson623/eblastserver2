<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('modules', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('service_option_id');
			$table->string('name');
			$table->text('description');
			$table->string('path');
			$table->string('action');
			$table->integer('adminable');
			$table->integer('groupable');
			$table->integer('locationable');
			$table->integer('sort_order');
			$table->string('image');
			$table->boolean('standalone');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('modules');
	}

}
