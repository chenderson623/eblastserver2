<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DefaultGroupTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        $types = array(
            array(
            'name'=> 'Wholesaler'
            ),
            array(
            'name'=> 'Store Group',
            ),
            array(
            'name'=> 'Ad Room',
            ),
            array(
            'name'=> 'CPG Group',
            ),
        );

        DB::table('group_types')->insert( $types );

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
