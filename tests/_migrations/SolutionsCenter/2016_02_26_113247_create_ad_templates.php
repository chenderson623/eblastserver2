<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdTemplates extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('site_content.ad_page_templates', function(Blueprint $table)
		{
      $table->increments('id');
      $table->text('template_json');
      $table->string('name',42);
			$table->timestamps();
      $table->timestamp('deleted_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('site_content.ad_page_templates');
	}

}
