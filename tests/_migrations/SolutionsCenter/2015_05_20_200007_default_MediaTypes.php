<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DefaultMediaTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

        $mediaTypes = [
            [
                'name' => 'JPEG',
                'media_type' => 'image',
                'format' => '.jpg'
            ],
            [
                'name' => 'PNG',
                'media_type' => 'image',
                'format' => '.png'
            ],
            [
                'name' => 'Printable Document Format',
                'media_type' => 'document',
                'format' => '.pdf'
            ],
            [
                'name' => 'MPEG Layer 4',
                'media_type' => 'video',
                'format' => '.mp4'
            ]
        ];

        foreach($mediaTypes as $key => $value)
        {
            MediaType::create($value);
        }

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	}

}
