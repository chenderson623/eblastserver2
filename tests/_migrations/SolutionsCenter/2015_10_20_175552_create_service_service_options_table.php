<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServiceServiceOptionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        if (!Schema::hasTable('service_service_options')) {
            Schema::create('service_service_options', function (Blueprint $table) {
                $table->integer('service_id')->index();
                $table->integer('service_option_id')->index();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('service_service_options');
    }
}
