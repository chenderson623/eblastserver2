<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPriceOverridesTable extends Migration {

    public function up() {
		
		Schema::table('price_overrides', function($table){

			if(!Schema::hasColumn('price_overrides', 'group_id')){
				$table->integer('group_id')->after('id');
			}
			if(!Schema::hasColumn('price_overrides', 'pricing_id')){
				$table->integer('pricing_id')->after('group_id');
			}
			if(!Schema::hasColumn('price_overrides', 'ongoing')){
				$table->tinyInteger('ongoing')->after('pricing_id');
			}
		});
    }

    public function down() {
    }

}
