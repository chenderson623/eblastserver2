<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterServiceRequestSiteContentsTable20160104 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {\
        DB::update("ALTER TABLE `service_request_site_contents` ADD `page_order` TEXT NOT NULL AFTER `upload_directory`");
    }

    public function down() {
    }

}
