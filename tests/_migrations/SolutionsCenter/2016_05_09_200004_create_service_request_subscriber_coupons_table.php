<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceRequestSubscriberCouponsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('service_request_subscriber_coupons', function(Blueprint $table) {
            $table->increments('id');

            $table->string('request_type', 50);
            $table->integer('subscriber_coupon_id');

            $table->date('offer_start_date');
            $table->date('offer_end_date');
            $table->smallInteger('coupon_valid_days');

            $table->timestamps();
            $table->softDeletes();
        });

    }

    public function down() {
    }
}
