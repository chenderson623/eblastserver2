<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyServiceRequestEblastCoupons20150527 extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::table('service_request_eblast_coupons', function($table) {
            $table->string('title')->after('blast_date');
            $table->smallInteger('eblast_coupon_template_id')->default(500)->after('title');
            $table->string('slug')->after('eblast_coupon_template_id');
            $table->string('email_subject')->after('slug');
            $table->text('email_plain_text')->after('email_subject');
            $table->text('blast_template_values')->after('email_plain_text');

        });


    }

    public function down() {
    }
}
