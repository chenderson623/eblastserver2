<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration {

    public function up() {
        Schema::create('promotions', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->text('content');
            $table->date('start_time');
            $table->date('end_time');
            $table->boolean('groupable')->default(0)->unsigned();
            $table->boolean('locationable')->default(0)->unsigned();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('promotions');
    }

}
