<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhoneablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('phoneables', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('phone_id')->index();
            $table->integer('phoneable_id')->index();
            $table->string('phoneable_type')->index();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('phoneables');
	}

}
