<?php

namespace spec\EblastServer\BlastEmail\Criteria\Eloquent;

use EblastServer\BlastEmail\Criteria\Eloquent\BlastDateCriteria;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BlastDateCriteriaSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(BlastDateCriteria::class);
    }
}
