<?php namespace EblastServer\BlastEmail\Models\Eloquent;

class BlastEmailRecipientTest extends \TestCase {

    /**
     * @var BlastEmailRecipient
     */
    protected $object;

    public static function getObject() {
        return new BlastEmailRecipient();
    }

    public function setup() {
        parent::setup();
        $this->object = self::getObject();
    }

    public function testInstance() {
        $this->assertInstanceOf('EblastServer\BlastEmail\Models\Eloquent\BlastEmailRecipient', $this->object);
    }


}