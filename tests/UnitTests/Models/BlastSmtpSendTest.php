<?php namespace EblastServer\BlastEmail\Models\Eloquent;

class BlastSmtpSendTest extends \TestCase {

    /**
     * @var BlastSmtpSend
     */
    protected $object;

    public static function getObject() {
        return new BlastSmtpSend();
    }

    public function setup() {
        parent::setup();
        $this->object = self::getObject();
    }

    public function testInstance() {
        $this->assertInstanceOf('EblastServer\BlastEmail\Models\Eloquent\BlastSmtpSend', $this->object);
    }


}