<?php namespace EblastServer\BlastEmail\Models\Eloquent;

class BlastSmtpBatchTest extends \TestCase {

    /**
     * @var BlastSmtpBatch
     */
    protected $object;

    public static function getObject() {
        return new BlastSmtpBatch();
    }

    public function setup() {
        parent::setup();
        $this->object = self::getObject();
    }

    public function testInstance() {
        $this->assertInstanceOf('EblastServer\BlastEmail\Models\Eloquent\BlastSmtpBatch', $this->object);
    }


}