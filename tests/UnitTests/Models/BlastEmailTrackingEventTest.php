<?php namespace EblastServer\BlastEmail\Models\Eloquent;

class BlastEmailTrackingEventTest extends \TestCase {

    /**
     * @var BlastEmailTrackingEvent
     */
    protected $object;

    public static function getObject() {
        return new BlastEmailTrackingEvent();
    }

    public function setup() {
        parent::setup();
        $this->object = self::getObject();
    }

    public function testInstance() {
        $this->assertInstanceOf('EblastServer\BlastEmail\Models\Eloquent\BlastEmailTrackingEvent', $this->object);
    }


}