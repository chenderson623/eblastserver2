<?php namespace EblastServer\BlastEmail\Models\Eloquent;

class BlastEmailTest extends \TestCase {

    /**
     * @var BlastEmail
     */
    protected $object;

    public static function getObject() {
        return new BlastEmail();
    }

    public function setup() {
        parent::setup();
        $this->object = self::getObject();
    }

    public function testInstance() {
        $this->assertInstanceOf('EblastServer\BlastEmail\Models\Eloquent\BlastEmail', $this->object);
    }


}