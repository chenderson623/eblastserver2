<?php namespace EblastServer\Common\Repositories\Eloquent;

class QueryBuilderTest extends \TestCase {

    protected $object;

    protected $model;
    protected $builder;
    protected $query_builder;

    public function setUp() {
        parent::setUp();
        $this->model         = new \EblastServer\Blasts\Models\Eloquent\EblastCoupon();
        $this->builder       = $this->model->query();
        $this->query_builder = new QueryBuilder($this->builder);
    }

    public function testEloquentQueryBuilderJoinsNull() {
        $this->assertNull($this->builder->getQuery()->joins);
    }

    public function testEloquentQueryBuilderJoins() {
        $this->builder->leftJoin('eblast_coupon_locations_groups', 'eblast_coupon_locations_groups.eblast_coupon_id', '=', 'eblast_coupons.id');
        $this->assertTrue(is_array($this->builder->getQuery()->joins));
    }

    public function testHasJoinFalse() {
        $this->assertFalse($this->query_builder->hasJoin('eblast_coupon_locations_groups'));
    }

    public function testHasJoinTrue() {
        $this->builder->leftJoin('eblast_coupon_locations_groups', 'eblast_coupon_locations_groups.eblast_coupon_id', '=', 'eblast_coupons.id');
        $this->assertTrue($this->query_builder->hasJoin('eblast_coupon_locations_groups'));
    }

}