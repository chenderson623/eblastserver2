<?php namespace EblastServer\BlastEmail\Repositories\Eloquent;

class BlastEmailEloquentRepositoryTest extends \TestCase {

    /**
     * @var EblastCouponsEloquentRepository
     */
    protected $object;

    protected function setUp()
    {
        parent::setUp();
        $model = new \EblastServer\BlastEmail\Models\Eloquent\BlastEmail();
        $this->object = new BlastEmailsEloquentRepository($model);
    }

    public function testInstance() {
        $this->assertInstanceOf('EblastServer\BlastEmail\Repositories\Eloquent\BlastEmailsEloquentRepository', $this->object);
    }

    public function testGetBlastsQueryBuilder() {
        $this->assertInstanceOf('EblastServer\BlastEmail\QueryBuilders\Eloquent\BlastEmailsEloquentQueryBuilder', $this->object->getBlastEmailsQueryBuilder());
    }

}