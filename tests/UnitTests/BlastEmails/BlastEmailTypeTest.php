<?php namespace EblastServer\BlastEmail;

class BlastEmailTypeTest extends \TestCase {

    public function testInstance() {
        $enum = BlastEmailType::MailSubscribers();
        $this->assertInstanceOf('EblastServer\BlastEmail\BlastEmailType', $enum);
    }

    public function testValue() {
        $enum = BlastEmailType::MailSubscribers();
        $this->assertEquals(1, $enum->getValue());
    }

    public function testConst() {
        $enum = BlastEmailType::MailSubscribers;
        $this->assertEquals(1, $enum);
    }

    public function testEquality() {
        $enum = BlastEmailType::MailSubscribers();
        $this->assertEquals(BlastEmailType::MailSubscribers(), $enum);
    }

    public function testEqualityConst() {
        $enum = BlastEmailType::MailSubscribers();
        $this->assertEquals(BlastEmailType::MailSubscribers, $enum->getValue());
    }

    public function testGetTitle() {
        $enum = BlastEmailType::MailSubscribers();
        $this->assertEquals('MailSubscribers', $enum->getTitle());
    }

    public function testGetByName() {
        $enum = BlastEmailType::getByName('MailSubscribers');
        $this->assertEquals('MailSubscribers', $enum->getTitle());
    }

    public function testGetName() {
        $enum = BlastEmailType::MailSubscribers();
        $this->assertEquals('MailSubscribers', $enum->getName());
    }

    public function testThrowsExceptionOnBadName() {
        $exception_thrown = false;
        try {
            $enum = BlastEmailType::getByName('XX');
        } catch (\InvalidArgumentException $e) {
            $exception_thrown = true;
        }
        $this->assertEquals(true, $exception_thrown);
    }

}