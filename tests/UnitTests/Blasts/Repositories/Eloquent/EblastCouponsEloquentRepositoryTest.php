<?php namespace EblastServer\Blasts\Repositories\Eloquent;

class EblastCouponsEloquentRepositoryTest extends \TestCase {

    /**
     * @var EblastCouponsEloquentRepository
     */
    protected $object;

    protected function setUp()
    {
        parent::setUp();
        $model = new \EblastServer\Blasts\Models\Eloquent\EblastCoupon();
        $this->object = new EblastCouponsEloquentRepository($model);
    }

    public function testInstance() {
        $this->assertInstanceOf('EblastServer\Blasts\Repositories\Eloquent\EblastCouponsEloquentRepository', $this->object);
    }

    public function testGetBlastsQueryBuilder() {
        $this->assertInstanceOf('EblastServer\Blasts\QueryBuilders\Eloquent\BlastsEloquentQueryBuilder', $this->object->getBlastsQueryBuilder());
    }

}