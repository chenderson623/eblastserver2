<?php namespace EblastServer\Blasts\Repositories\Eloquent;

class CustomEblastsEloquentRepositoryTest extends \TestCase {

    /**
     * @var CustomEblastsEloquentRepository
     */
    protected $object;

    protected function setUp()
    {
        parent::setUp();
        $model = new \EblastServer\Blasts\Models\Eloquent\CustomEblast();
        $this->object = new CustomEblastsEloquentRepository($model);
    }

    public function testInstance() {
        $this->assertInstanceOf('EblastServer\Blasts\Repositories\Eloquent\CustomEblastsEloquentRepository', $this->object);
    }

}