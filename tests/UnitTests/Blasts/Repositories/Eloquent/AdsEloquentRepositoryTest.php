<?php namespace EblastServer\Blasts\Repositories\Eloquent;

class AdsEloquentRepositoryTest extends \TestCase {

    /**
     * @var AdsEloquentRepository
     */
    protected $object;

    protected function setUp()
    {
        parent::setUp();
        $model = new \EblastServer\Blasts\Models\Eloquent\Ad();
        $this->object = new AdsEloquentRepository($model);
    }

    public function testInstance() {
        $this->assertInstanceOf('EblastServer\Blasts\Repositories\Eloquent\AdsEloquentRepository', $this->object);
    }

}