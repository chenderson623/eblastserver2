<?php namespace EblastServer\Blasts;

class BlastTypeTest extends \TestCase {

    public function testInstance() {
        $enum = BlastType::AD();
        $this->assertInstanceOf('EblastServer\Blasts\BlastType', $enum);
    }

    public function testValue() {
        $enum = BlastType::AD();
        $this->assertEquals(1, $enum->getValue());
    }

    public function testConst() {
        $enum = BlastType::AD;
        $this->assertEquals(1, $enum);
    }

    public function testEquality() {
        $enum = BlastType::AD();
        $this->assertEquals(BlastType::AD(), $enum);
    }

    public function testEqualityConst() {
        $enum = BlastType::AD();
        $this->assertEquals(BlastType::AD, $enum->getValue());
    }

    public function testGetTitle() {
        $enum = BlastType::AD();
        $this->assertEquals('Ad', $enum->getTitle());
    }

    public function testGetByName() {
        $enum = BlastType::getByName('AD');
        $this->assertEquals('Ad', $enum->getTitle());
    }

    public function testGetName() {
        $enum = BlastType::AD();
        $this->assertEquals('AD', $enum->getName());
    }

    public function testGetByNameLowerCase() {
        $exception_thrown = false;
        try {
            $enum = BlastType::getByName('ad');
        } catch (\InvalidArgumentException $e) {
            $exception_thrown = true;
        }
        $this->assertEquals(true, $exception_thrown);
    }

    public function testThrowsExceptionOnBadName() {
        $exception_thrown = false;
        try {
            $enum = BlastType::getByName('XX');
        } catch (\InvalidArgumentException $e) {
            $exception_thrown = true;
        }
        $this->assertEquals(true, $exception_thrown);
    }

}