<?php namespace EblastServer\Blasts;

class BlastsServiceProviderTest extends \TestCase {

    public function testAdRepositoryInterfaceExists() {
        //$this->assertTrue(interface_exists('EblastServer\Blasts\Repositories\AdsRepository'));
        $this->assertTrue(interface_exists('EblastServer\Blasts\Repositories\AdsRepository'));
    }

    public function testAdsRepository() {
        $repository = \App::make('EblastServer\Blasts\Repositories\AdsRepository');
        $this->assertInstanceOf('EblastServer\Blasts\Repositories\Eloquent\AdsEloquentRepository', $repository);
        // automatically injects Ads Model
        $this->assertInstanceOf('EblastServer\Blasts\Models\Eloquent\Ad', $repository->model);
        $this->assertTrue(method_exists($repository, 'getBlastsQueryBuilder'));
    }

    public function testCustomEblastsRepository() {
        $repository = \App::make('EblastServer\Blasts\Repositories\CustomEblastsRepository');
        $this->assertInstanceOf('EblastServer\Blasts\Repositories\Eloquent\CustomEblastsEloquentRepository', $repository);
    }

    public function testEblastCouponsRepository() {
        $repository = \App::make('EblastServer\Blasts\Repositories\EblastCouponsRepository');
        $this->assertInstanceOf('EblastServer\Blasts\Repositories\Eloquent\EblastCouponsEloquentRepository', $repository);
    }

    public function testBlastTypeRepositoriesService() {
        $service = \App::make('BlastTypeRepositories');
        $this->assertInstanceOf('EblastServer\Blasts\Services\BlastTypeRepositories', $service);
    }

    public function testBlastTypeRepositoriesServiceAdRepository() {
        $repository = \App::make('BlastTypeRepositories')->repository(\EblastServer\Blasts\BlastType::AD);
        $this->assertInstanceOf('EblastServer\Blasts\Repositories\Eloquent\AdsEloquentRepository', $repository);
    }

    public function testBlastsQueryServiceGetQueryBuildersWithBlastType() {
        $query = \App::make('BlastsQuery', [
            'id'         => 123,
            'blast_type' => BlastType::getByName('AD'),
        ]);
        $this->assertEquals(true, is_array($query->getQueryBuilders()));
        $this->assertEquals(1, count($query->getQueryBuilders()));
    }

}

