<?php

namespace EblastServer\Blasts\Services;

use EblastServer\Blasts\BlastType;

class BlastsQueryTest extends \TestCase {

    public function testInstance() {
        $factory = new BlastsQuery($this->app, [
            'date' => new \DateTime()
        ]);
        $this->assertInstanceOf('EblastServer\Blasts\Services\BlastsQuery', $factory);
    }

    public function testConstructor() {
        $factory = new BlastsQuery($this->app, [
            'id'         => 123,
            'blast_type' => BlastType::getByName('AD'),
            'bogus'      => 'something'
        ]);

        $reflection = new \ReflectionClass($factory);
        $reflection_property = $reflection->getProperty('search_options');
        $reflection_property->setAccessible(true);

        //var_dump($factory->search_options);
        $search_options = $reflection_property->getValue($factory);

        $this->assertEquals(123, $search_options['id']);
        $this->assertArrayNotHasKey('bogus', $search_options);
    }

    public function testConstructorDateException() {
        $exception_thrown = false;
        try {
            $factory = new BlastsQuery($this->app, [
                'date' => 123,
            ]);
        } catch (InvalidSearchOptionException $e) {
            $exception_thrown = true;
        }
        $this->assertEquals(true, $exception_thrown);

    }

    public function testValidate() {
        $exception_thrown = false;
        try {
            $factory = new BlastsQuery($this->app, [
                'id' => 123,
            ]);
        } catch (InvalidUserSearchOptionException $e) {
            $exception_thrown = true;
        }
        $this->assertEquals(true, $exception_thrown);

    }

    public function testGetBlastTypes() {
        $factory = new BlastsQuery($this->app, [
            'date' => new \DateTime()
        ]);
        $this->assertEquals(true, is_array($factory->getBlastTypes()));
        $this->assertEquals(3, count($factory->getBlastTypes()));
    }

    public function testGetBlastTypesWithBlastType() {
        $factory = new BlastsQuery($this->app, [
            'id'         => 123,
            'blast_type' => BlastType::getByName('AD'),
        ]);
        $this->assertEquals(true, is_array($factory->getBlastTypes()));
        $this->assertEquals(1, count($factory->getBlastTypes()));
    }
    // ---------- Query Builders -----------
    public function testGetQueryBuilders() {
        $factory = new BlastsQuery($this->app, [
            'date' => new \DateTime()
        ]);
        $this->assertEquals(true, is_array($factory->getQueryBuilders()));
        $this->assertEquals(3, count($factory->getQueryBuilders()));
        foreach($factory->getQueryBuilders() as $query_builder) {
            $this->assertInstanceOf('\EblastServer\Blasts\QueryBuilders\Eloquent\BlastsEloquentQueryBuilder', $query_builder);
        }
    }

    public function testGetQueryBuildersWithBlastType() {
        $factory = new BlastsQuery($this->app, [
            'id'         => 123,
            'blast_type' => BlastType::getByName('AD'),
        ]);
        $this->assertEquals(true, is_array($factory->getQueryBuilders()));
        $this->assertEquals(1, count($factory->getQueryBuilders()));
    }

    public function testGetQueryBuildersQueryHasBlastId() {
        $factory = new BlastsQuery($this->app, [
            'id'         => 123,
            'blast_type' => BlastType::getByName('AD'),
        ]);
        $builder  = $factory->getQueryBuilders()[0];
        $this->assertContains('`id` = ?', $builder->getQuery()->toSql());
    }

    public function testGetQueryBuildersQueryHasDate() {
        $factory = new BlastsQuery($this->app, [
            'date'  => new \DateTime()
        ]);
        $builder  = $factory->getQueryBuilders()[0];
        $this->assertContains('`blast_date` = ?', $builder->getQuery()->toSql());
    }

    public function testGetQueryBuildersQueryHasLocationId() {
        $factory = new BlastsQuery($this->app, [
            'date'  => new \DateTime(),
            'location_id'  => 12345
        ]);
        $builder  = $factory->getQueryBuilders()[0];
        $this->assertContains('`blast_date` = ?', $builder->getQuery()->toSql());
        $this->assertContains('`location_id` = ?', $builder->getQuery()->toSql());
    }

}