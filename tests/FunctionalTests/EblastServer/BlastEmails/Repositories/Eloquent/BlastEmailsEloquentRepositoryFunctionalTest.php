<?php
namespace EblastServer\BlastEmail\Repositories\Eloquent;

class BlastEmailsEloquentRepositoryFunctionalTest extends \TestCase
{
    /**
     * @var BlastEmailsEloquentRepository
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        parent::setUp();
        $model = new \EblastServer\BlastEmail\Models\Eloquent\BlastEmail();
        $this->object = new BlastEmailsEloquentRepository($model);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        parent::tearDown();
    }

    // ---------- Object Types ----------

    public function testFindNull() {
        $record = $this->object->find(999999);
        $this->assertEquals(null, $record);
    }

    public function testFind() {
        $record = $this->object->find(18001362);
        $this->assertInstanceOf('EblastServer\BlastEmail\Models\Eloquent\BlastEmail', $record);
    }

    public function testGetQueryBuilder() {
        $builder = $this->object->getQueryBuilder();
        $this->assertInstanceOf('\EblastServer\Common\Repositories\Eloquent\QueryBuilder', $builder);
    }

    public function testGetBlastEmailsQueryBuilder() {
        $builder = $this->object->getBlastEmailsQueryBuilder();
        $this->assertInstanceOf('\EblastServer\BlastEmail\QueryBuilders\Eloquent\BlastEmailsEloquentQueryBuilder', $builder);
    }

    // ---------- Test Builders SQL ----------

    public function testAddSendDateCriteria() {
        $datetime = new \DateTime();
        $builder  = $this->object->getBlastEmailsQueryBuilder()->addSendDateCriteria($datetime);
        $this->assertContains('where `send_date` = ?', $builder->getQuery()->toSql());
    }

    public function testAddBlastIdCriteria() {
        $builder  = $this->object->getBlastEmailsQueryBuilder()->addBlastIdCriteria(12345);
        $this->assertContains('where `blast_id` = ?', $builder->getQuery()->toSql());
    }

    public function testAddLocationIdCriteria() {
        $builder  = $this->object->getBlastEmailsQueryBuilder()->addLocationIdCriteria(12345);
        $this->assertContains('where `location_id` = ?', $builder->getQuery()->toSql());
    }

    public function testSendDateCriteria() {
        $criteria = new \EblastServer\BlastEmail\Criteria\Eloquent\SendDateCriteria(new \DateTime());
        $this->assertInstanceOf('\EblastServer\BlastEmail\Criteria\Eloquent\SendDateCriteria', $criteria);

        $builder = $this->object->getQueryBuilder();
        $builder->applyCriteria($criteria);

        $this->assertContains('where `send_date` = ?', $builder->getQuery()->toSql());
    }

    // ---------- Test Database Access ----------

    public function testAddBlastDateCriteriaResult() {
        $datetime = new \DateTime('2016-09-15');
        $builder  = $this->object->getBlastEmailsQueryBuilder()->addSendDateCriteria($datetime);
        $this->assertEquals(11, $builder->get()->count());
    }

    public function testAddBlastIdCriteriaResult() {
        $builder  = $this->object->getBlastEmailsQueryBuilder()->addBlastIdCriteria(300176);
        $this->assertEquals(1, $builder->get()->count());
    }

    public function testAddBlastIdCriteriaResultMultiple() {
        $builder  = $this->object->getBlastEmailsQueryBuilder()->addBlastIdCriteria(25124);
        $this->assertEquals(6, $builder->get()->count());
    }

    public function testAddLocationIdCriteriaResult() {
        $builder  = $this->object->getBlastEmailsQueryBuilder()->addLocationIdCriteria(20015);
        $this->assertEquals(2, $builder->get()->count());
    }

    // Works with applyCriteria also
    public function testSendDateCriteriaQuery() {
        $criteria = new \EblastServer\BlastEmail\Criteria\Eloquent\SendDateCriteria(new \DateTime('2016-09-15'));
        $this->assertInstanceOf('\EblastServer\BlastEmail\Criteria\Eloquent\SendDateCriteria', $criteria);

        $builder = $this->object->getQueryBuilder();
        $builder->applyCriteria($criteria);

        $collection = $builder->get();
        $this->assertInstanceOf('\Illuminate\Database\Eloquent\Collection', $collection);

        $this->assertEquals(11, $builder->get()->count());
    }

}
