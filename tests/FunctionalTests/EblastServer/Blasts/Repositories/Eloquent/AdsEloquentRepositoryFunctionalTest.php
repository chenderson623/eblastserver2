<?php
namespace EblastServer\Blasts\Repositories\Eloquent;

class AdsEloquentRepositoryFunctionalTest extends \TestCase
{
    /**
     * @var AdsEloquentRepository
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        parent::setUp();
        $model = new \EblastServer\Blasts\Models\Eloquent\Ad();
        $this->object = new AdsEloquentRepository($model);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        parent::tearDown();
    }

    // ---------- Object Types ----------

    public function testFind() {
        $record = $this->object->find(106073);
        $this->assertInstanceOf('EblastServer\Blasts\Models\Eloquent\Ad', $record);
    }


}
