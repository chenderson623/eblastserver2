<?php

namespace EblastServer\Blasts\Services;

use EblastServer\Blasts\BlastType;

class BlastsQueryFunctionalTest extends \TestCase {

    public function testFetchCollectionByDate() {
        $factory = new BlastsQuery($this->app, [
            'date'  => new \DateTime('2016-07-11'),
        ]);

        $collection = $factory->fetchCollection();
        $this->assertEquals(90, $collection->count());
    }

    public function testFetchCollectionByDateAndType() {
        $factory = new BlastsQuery($this->app, [
            'date'  => new \DateTime('2016-07-11'),
            'blast_type' => BlastType::getByName('AD'),
        ]);

        $collection = $factory->fetchCollection();
        $this->assertEquals(88, $collection->count());
    }

    public function testFetchCollectionByDateAndLocation() {
        $factory = new BlastsQuery($this->app, [
            'date'        => new \DateTime('2016-07-11'),
            'location_id' => 10037
        ]);

        $collection = $factory->fetchCollection();
        $this->assertEquals(1, $collection->count());
    }

    public function testFetchCollectionByDateAndLocationGroup() {
        $factory = new BlastsQuery($this->app, [
            'date'           => new \DateTime('2016-07-11'),
            'location_group' => 201
        ]);

        $collection = $factory->fetchCollection();
        $this->assertEquals(14, $collection->count());
    }

    public function testEachByDate() {
        $factory = new BlastsQuery($this->app, [
            'date'  => new \DateTime('2016-07-11'),
        ]);

        $count = 0;
        $factory->each(function($model) use(&$count){
            $count++;
        }, 10);

        $this->assertEquals(90, $count);
    }

    public function testEachWithLocationCount() {
        $factory = new BlastsQuery($this->app, [
            'date'  => new \DateTime('2016-07-11'),
        ]);

        $factory->addLocationCountField();

        $location_count = 0;
        $factory->each(function($model) use(&$location_count){
            $location_count += $model->locationsCount;
        }, 10);

        $this->assertEquals(300, $location_count);
    }

}