<?php

require __DIR__.'/../../../bootstrap/autoload.php';
$app = require __DIR__.'/../../../bootstrap/app.php';
$app->loadEnvironmentFrom('.env');
$app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

$model = new \EblastServer\Blasts\Models\Eloquent\EblastCoupon();

$repository = new \EblastServer\Blasts\Repositories\Eloquent\EblastCouponsEloquentRepository($model);

$criteria = new \EblastServer\Blasts\Criteria\Eloquent\BlastDateCriteria(new \DateTime('2015-09-30'));

$builder = $repository->getQueryBuilder();
$builder->applyCriteria($criteria);

$collection = $builder->get();
var_dump($collection->count());
