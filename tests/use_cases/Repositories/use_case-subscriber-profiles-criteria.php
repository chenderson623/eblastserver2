<?php

require __DIR__.'/../../../bootstrap/autoload.php';
$app = require __DIR__.'/../../../bootstrap/app.php';
$app->loadEnvironmentFrom('.env');
$app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

$model = new EblastServer\Subscribers\Models\Eloquent\SubscriberProfile();
//var_dump($model->where('first_name','=', 'Chris'));
// return Illuminate\Database\Eloquent\Builder


$eloquent_builder = $model->where('first_name','=', 'Chris');
$query_builder = $eloquent_builder->getQuery();
$connection = $query_builder->getConnection();

//var_dump(get_class($connection->getPdo()));
var_dump($query_builder->from);

var_dump($query_builder->toSql(), $query_builder->getBindings());