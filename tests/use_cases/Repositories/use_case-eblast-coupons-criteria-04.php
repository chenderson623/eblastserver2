<?php

require __DIR__.'/../../../bootstrap/autoload.php';
$app = require __DIR__.'/../../../bootstrap/app.php';
$app->loadEnvironmentFrom('.env');
$app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

$model = new \EblastServer\Blasts\Models\Eloquent\EblastCoupon();

$repository = new \EblastServer\Blasts\Repositories\Eloquent\EblastCouponsEloquentRepository($model);

// eblast_coupons 22391 belongs to locations 20010,20027,20191,20192, + more

$date_criteria     = new \EblastServer\Blasts\Criteria\Eloquent\BlastDateCriteria(new \DateTime('2015-09-30'));
//TODO: how to make a join?
//  apply() has query_builder
$location_criteria = new \EblastServer\Blasts\Criteria\Eloquent\BlastLocationIdCriteria(20010);

$collection = $repository->fetchCriteria([
    $location_criteria
]);

var_dump($collection->count());