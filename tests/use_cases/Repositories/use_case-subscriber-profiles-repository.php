<?php

require __DIR__.'/../../../bootstrap/autoload.php';
$app = require __DIR__.'/../../../bootstrap/app.php';
$app->loadEnvironmentFrom('.env.testing');
$app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

$repo = $app->make(EblastServer\Subscribers\Repositories\SubscriberProfilesRepository::class);
$subscriber = $repo->findById(50070);
var_dump($subscriber->id);