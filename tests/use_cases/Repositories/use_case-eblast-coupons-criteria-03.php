<?php

require __DIR__.'/../../../bootstrap/autoload.php';
$app = require __DIR__.'/../../../bootstrap/app.php';
$app->loadEnvironmentFrom('.env');
$app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

$model = new \EblastServer\Blasts\Models\Eloquent\EblastCoupon();

$repository = new \EblastServer\Blasts\Repositories\Eloquent\EblastCouponsEloquentRepository($model);

$date_criteria = new \EblastServer\Blasts\Criteria\Eloquent\BlastDateCriteria(new \DateTime('2015-09-30'));
$title_criteria = new \EblastServer\Blasts\Criteria\Eloquent\BlastTitleCriteria('Kemp');

$collection = $repository->fetchCriteria([
    $date_criteria,
    $title_criteria
]);

var_dump($collection->count());