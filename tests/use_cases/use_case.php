<?php

require __DIR__.'/../../bootstrap/autoload.php';
$app = require __DIR__.'/../../bootstrap/app.php';
$app->loadEnvironmentFrom('.env.testing');
$app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

$email_address = new App\Libraries\EmailAddresses\EmailAddress('test@any.com');

var_dump($email_address);