<?php

require __DIR__.'/../../../bootstrap/autoload.php';
$app = require __DIR__.'/../../../bootstrap/app.php';
$app->loadEnvironmentFrom('.env.testing');
$app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

$data = [
    'body_width' => 600,
    'default_color' => '#ccc',
    'heading_color' => '#ddd',
    'homepage_url' => 'afmpresto.com',
    'site_logo_url' => 'http://afmpresto.com/logo.jpg',
    'logo_size_string' => 'width="200px"',
    'site_loc_name' => 'My Store',
    'facebook_link_head_section' => 'TODO',
    'ftf_header' => 'TODO',
    'ad_display_name' => 'DISPLAY NAME',
    'display_date_conditional' => 'Jan 1, 2016',

];

dd(view('wave_eblasts.ads.ad_top', $data)->render());