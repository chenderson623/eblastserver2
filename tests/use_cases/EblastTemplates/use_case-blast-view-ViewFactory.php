<?php

require __DIR__.'/../../../bootstrap/autoload.php';
$app = require __DIR__.'/../../../bootstrap/app.php';
$app->loadEnvironmentFrom('.env.testing');
$app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

use Illuminate\View\Factory;

$resolver = $app['view.engine.resolver'];
$finder = $app['view.finder'];

$view_factory = new Factory($resolver, $finder, $app['events']);


$data = [
    'body_width' => 600,
    'default_color' => '#ccc',
    'heading_color' => '#ddd',
    'homepage_url' => 'afmpresto.com',
    'site_logo_url' => 'http://afmpresto.com/logo.jpg',
    'logo_size_string' => 'width="200px"',
    'site_loc_name' => 'My Store',
    'facebook_link_head_section' => 'TODO',
    'ftf_header' => 'TODO',
    'ad_display_name' => 'DISPLAY NAME',
    'display_date_conditional' => 'Jan 1, 2016',

];

$result = $view_factory->make('wave_eblasts.ads.ad_top', $data)->render();
dd($result);

//TODO: How to have "Global" view composers as well as add view composers to each instance of view_factory?