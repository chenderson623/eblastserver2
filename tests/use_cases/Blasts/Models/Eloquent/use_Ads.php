<?php namespace EblastServer\Blasts\Models\Eloquent;

require "../../../bootstrap.php";

$model = new Ad();

\DB::enableQueryLog();
\DB::connection('site_content_database')->enableQueryLog();
\DB::connection('solutions_center_database')->enableQueryLog();

$blasts = Ad::where('blast_date', '=', '2016-08-25')->get();

$blasts->load('locations');

echo "\nBlasts: " . $blasts->count();

echo "\n\n";
var_dump(\DB::getQueryLog());
var_dump(\DB::connection('site_content_database')->getQueryLog());
var_dump(\DB::connection('solutions_center_database')->getQueryLog());
