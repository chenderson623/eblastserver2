<?php namespace EblastServer\Blasts\Models\Eloquent;

require "../../../bootstrap.php";

$model = new Ad();

\DB::enableQueryLog();
\DB::connection('site_content_database')->enableQueryLog();
\DB::connection('solutions_center_database')->enableQueryLog();

$blast = Ad::find(299072);

echo "\nLocations: " . $blast->locations->count();

echo "\n\n";
var_dump(\DB::getQueryLog());
var_dump(\DB::connection('site_content_database')->getQueryLog());
var_dump(\DB::connection('solutions_center_database')->getQueryLog());
