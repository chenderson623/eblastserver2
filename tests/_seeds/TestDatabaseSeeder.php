<?php

use Illuminate\Database\Seeder;

class TestDatabaseSeeder extends Seeder
{

    public function run() {
        Eloquent::unguard();

        $this->call('EventsTableSeeder');
        $this->call('SmtpProviderAccountsTableSeeder');
        $this->call('EblastCouponsSeeder');
        $this->call('AdsSeeder');
        //$this->call('BlastEmailsSeeder');

    }

}
