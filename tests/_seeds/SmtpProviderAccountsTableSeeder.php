<?php

use Illuminate\Database\Seeder;

class SmtpProviderAccountsTableSeeder extends Seeder {

    /*
     * collect from database with this:
        SELECT sendgrid_credentials.*, sites.site_id
        FROM sites
        LEFT JOIN sendgrid_credentials ON sites.site_url LIKE CONCAT('%',sendgrid_credentials.url)
     */


    public function run()
    {

        DB::table('smtp_provider_accounts')->truncate();
        DB::table('smtp_provider_accounts_locations')->truncate();

        $null_smtp_provider_account = [
            'id' => 1,
            'smtp_provider_id' => 3,
            'name' => 'Null Mailer'
        ];
        DB::table('smtp_provider_accounts')->insert($null_smtp_provider_account);

        // CSV file: be sure to change the column names to the new column names
        $csv_filename = __DIR__ . '/data/sendgrid_credentials-owg-20160304.csv';
        $data = $this->loadFromCSV($csv_filename);

        $account_ids = [];

        foreach($data as $credential) {
            $account = array_filter($credential, function($key) {
                return in_array($key, ['id', 'domain', 'username', 'password']);
            }, ARRAY_FILTER_USE_KEY);
            $account['active']           = 1;
            $account['smtp_provider_id'] = 1;
            $account['name']             = ucwords($account['username']);

            if(!in_array($account['id'], $account_ids)) {
                DB::table('smtp_provider_accounts')->insert($account);
                $account_ids[] = $account['id'];
            }

            DB::table('smtp_provider_accounts_locations')->insert([
                'smtp_provider_account_id' => $account['id'],
                'location_id' => $credential['location_id']
            ]);

        }

    }

    private function loadFromCSV($filename, $deliminator = ",") {
        ini_set("auto_detect_line_endings", true);
        if (!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $header = NULL;
        $data   = array();

        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $deliminator)) !== FALSE) {
                if (!$header) {
                    $header = $row;
                } else {
                    $row_assoc = array_combine($header, $row);

                    if(isset($row_assoc['location_id'])) {
                        $row_assoc['location_id'] = 40000 + $row_assoc['location_id']; // fix for owg locations
                    }

                    $data[]    = $row_assoc;
                }
            }
            fclose($handle);
        }

        return $data;
    }

}
