<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder {

    /*
     * collect database like this:
     *
     *
        SELECT *
        FROM subscriber_subscriptions
        WHERE site_id IN (440)

        SELECT subscriber_profiles.*
        FROM subscriber_profiles
        LEFT JOIN subscriber_subscriptions ON subscriber_subscriptions.subscriber_id = subscriber_profiles.subscriber_id
        WHERE  `subscriber_subscriptions`.site_id IN (440)
        GROUP BY subscriber_profiles.subscriber_id
     *
     */



    public function run()
    {

        // ---------Subscribers

        DB::table('subscriber_subscriptions')->truncate();

        // CSV file: be sure to change the column names to the new column names
        $csv_filename = __DIR__ . '/data/subscriber_subscriptions-owg-465-20160304.csv';
        $data = $this->loadFromCSV($csv_filename);
        DB::table('subscriber_subscriptions')->insert($data);

        $csv_filename = __DIR__ . '/data/subscriber_subscriptions-owg-475-478-20160324.csv';
        $data = $this->loadFromCSV($csv_filename);
        DB::table('subscriber_subscriptions')->insert($data);

        // ------Profiles

        DB::table('subscriber_profiles')->truncate();

        // CSV file: be sure to change the column names to the new column names
        $csv_filename = __DIR__ . '/data/subscriber_profiles-owg-465-20160304.csv';
        $data = $this->loadFromCSV($csv_filename);
        DB::table('subscriber_profiles')->insert($data);

        $csv_filename = __DIR__ . '/data/subscriber_profiles-owg-475-478-20160324.csv';
        $data = $this->loadFromCSV($csv_filename);
        DB::table('subscriber_profiles')->insert($data);

    }

    private function loadFromCSV($filename, $deliminator = ",") {
        ini_set("auto_detect_line_endings", true);
        if (!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $header = NULL;
        $data   = array();

        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $deliminator)) !== FALSE) {
                if (!$header) {
                    $header = $row;
                } else {
                    $row_assoc = array_combine($header, $row);

                    if(isset($row_assoc['location_id'])) {
                        $row_assoc['location_id'] = 40000 + $row_assoc['location_id']; // fix for owg locations
                    }

                    $data[]    = $row_assoc;
                }
            }
            fclose($handle);
        }

        return $data;
    }

}
