<?php

use Illuminate\Database\Seeder;

class BlastEmailsSeeder extends Seeder {

    public function run()
    {
        DB::table('blast_emails')->truncate();
        DB::table('blast_email_recipients')->truncate();

        $blast_emails = array(
          array('id' => '18001362','email_type' => 'Ad','send_date' => '2016-09-15','send_time' => '00:15:02','location_id' => '20015','blast_type' => 'Ad','blast_id' => '300173','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '755','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 00:15:02','updated_at' => '2016-09-15 00:15:04'),
          array('id' => '18001363','email_type' => 'Ad','send_date' => '2016-09-15','send_time' => '00:15:04','location_id' => '20025','blast_type' => 'Ad','blast_id' => '300174','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '485','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 00:15:04','updated_at' => '2016-09-15 00:15:05'),
          array('id' => '18001364','email_type' => 'Ad','send_date' => '2016-09-15','send_time' => '00:15:05','location_id' => '20027','blast_type' => 'Ad','blast_id' => '300175','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '296','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 00:15:05','updated_at' => '2016-09-15 00:15:06'),
          array('id' => '18001365','email_type' => 'Ad','send_date' => '2016-09-15','send_time' => '00:15:06','location_id' => '20010','blast_type' => 'Ad','blast_id' => '300176','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '854','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 00:15:06','updated_at' => '2016-09-15 00:15:07'),
          array('id' => '18001366','email_type' => 'Ad','send_date' => '2016-09-15','send_time' => '00:15:07','location_id' => '20011','blast_type' => 'Ad','blast_id' => '300177','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '1883','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 00:15:07','updated_at' => '2016-09-15 00:15:11'),
          array('id' => '18001367','email_type' => 'CustomEblast','send_date' => '2016-09-15','send_time' => '01:15:02','location_id' => '20010','blast_type' => 'CustomEblast','blast_id' => '25124','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '860','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 01:15:02','updated_at' => '2016-09-15 01:15:04'),
          array('id' => '18001368','email_type' => 'CustomEblast','send_date' => '2016-09-15','send_time' => '01:15:02','location_id' => '20011','blast_type' => 'CustomEblast','blast_id' => '25124','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '1901','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 01:15:02','updated_at' => '2016-09-15 01:15:08'),
          array('id' => '18001369','email_type' => 'CustomEblast','send_date' => '2016-09-15','send_time' => '01:15:02','location_id' => '20015','blast_type' => 'CustomEblast','blast_id' => '25124','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '767','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 01:15:02','updated_at' => '2016-09-15 01:15:10'),
          array('id' => '18001370','email_type' => 'CustomEblast','send_date' => '2016-09-15','send_time' => '01:15:02','location_id' => '20025','blast_type' => 'CustomEblast','blast_id' => '25124','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '490','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 01:15:02','updated_at' => '2016-09-15 01:15:12'),
          array('id' => '18001371','email_type' => 'CustomEblast','send_date' => '2016-09-15','send_time' => '01:15:02','location_id' => '20027','blast_type' => 'CustomEblast','blast_id' => '25124','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '303','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 01:15:02','updated_at' => '2016-09-15 01:15:14'),
          array('id' => '18001372','email_type' => 'CustomEblast','send_date' => '2016-09-15','send_time' => '01:15:02','location_id' => '20364','blast_type' => 'CustomEblast','blast_id' => '25124','production' => '0','auto_send' => '0','sent' => '1','failed' => '0','total_sent' => '1086','total_opened' => '0','total_unique_opened' => '0','total_clicked' => '0','total_unique_clicked' => '0','total_failed' => '0','created_at' => '2016-09-15 01:15:02','updated_at' => '2016-09-15 01:15:18')
        );

        foreach($blast_emails as $blast_email) {
            DB::table('blast_emails')->insert($blast_email);
        }

        $blast_email_recipients = array(
          array('id' => '1800355892','blast_email_id' => '18001362','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800355893','blast_email_id' => '18001362','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com'),
          array('id' => '1800356647','blast_email_id' => '18001363','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800356648','blast_email_id' => '18001363','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com'),
          array('id' => '1800357132','blast_email_id' => '18001364','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800357133','blast_email_id' => '18001364','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com'),
          array('id' => '1800357428','blast_email_id' => '18001365','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800357429','blast_email_id' => '18001365','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com'),
          array('id' => '1800358282','blast_email_id' => '18001366','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800358283','blast_email_id' => '18001366','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com'),
          array('id' => '1800360165','blast_email_id' => '18001367','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800360166','blast_email_id' => '18001367','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com'),
          array('id' => '1800361025','blast_email_id' => '18001368','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800361026','blast_email_id' => '18001368','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com'),
          array('id' => '1800362926','blast_email_id' => '18001369','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800362927','blast_email_id' => '18001369','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com'),
          array('id' => '1800363693','blast_email_id' => '18001370','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800363694','blast_email_id' => '18001370','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com'),
          array('id' => '1800364183','blast_email_id' => '18001371','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800364184','blast_email_id' => '18001371','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com'),
          array('id' => '1800364486','blast_email_id' => '18001372','subscriber_id' => '0','email_address' => 'chenderson@mediasolutionscorp.com'),
          array('id' => '1800364487','blast_email_id' => '18001372','subscriber_id' => '0','email_address' => 'tech@mediasolutionscorp.com')
        );

        foreach($blast_email_recipients as $blast_email_recipient) {
            DB::table('blast_email_recipients')->insert($blast_email_recipient);
        }

        $csv_filename = __DIR__ . '/data/blast_email_recipients-01.csv';
        $data = $this->loadFromCSV($csv_filename);
        DB::table('blast_email_recipients')->insert($data);

    }

    private function loadFromCSV($filename, $deliminator = ",") {
        ini_set("auto_detect_line_endings", true);
        if (!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $header = NULL;
        $data   = array();

        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $deliminator)) !== FALSE) {
                if (!$header) {
                    $header = $row;
                } else {
                    $row_assoc = array_combine($header, $row);

                    if(isset($row_assoc['location_id'])) {
                        $row_assoc['location_id'] = 40000 + $row_assoc['location_id']; // fix for owg locations
                    }

                    $data[]    = $row_assoc;
                }
            }
            fclose($handle);
        }

        return $data;
    }

}
