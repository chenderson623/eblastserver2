<?php

use Illuminate\Database\Seeder;

class EblastCouponsSeeder extends Seeder {

    public function run()
    {
        DB::table('eblast_coupons')->truncate();
        DB::table('eblast_coupon_coupons')->truncate();
        DB::table('eblast_coupon_locations_groups')->truncate();
        DB::table('testing_site_content.coupons')->truncate();

        $eblast_coupons = array(
          array('id' => '22583','title' => 'Shurfine Potato or Kettle Chips &#8226; Assorted Flavors &#8226; 8.5-10oz','blast_date' => '2016-07-04','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:06','updated_at' => '2016-07-14 11:11:06'),
          array('id' => '22586','title' => 'Fresh Avocado','blast_date' => '2016-07-04','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22588','title' => 'Shurfine Cottage Cheese &#8226; 24 oz','blast_date' => '2016-07-06','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22590','title' => 'French Onion Dip &#8226; Selected Varieties &#8226; 16 oz','blast_date' => '2016-07-07','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22585','title' => 'Shurfine Spreadable Butter &#8226; 15 oz','blast_date' => '2016-07-11','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:06','updated_at' => '2016-07-14 11:11:06'),
          array('id' => '22591','title' => 'Green Giant Carrots &#8226; Baby Cut &#8226; 1 lb','blast_date' => '2016-07-11','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22593','title' => 'Shurfine Reusable Bag','blast_date' => '2016-07-14','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22592','title' => 'Lofthouse Cookies &#8226; 10-12 ct','blast_date' => '2016-07-16','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22596','title' => 'Lofthouse Cookies &#8226; 10-12 ct','blast_date' => '2016-07-16','eblast_coupon_template_id' => '501','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22587','title' => 'At Ease Mini Sandwiches &#8226; 8.6 - 10.4 oz','blast_date' => '2016-07-18','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22597','title' => 'Mr. Dell\'s Hashbrowns &#8226; 24-32 oz','blast_date' => '2016-07-18','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22598','title' => '@Ease Skillet Meals &#8226; 21 oz','blast_date' => '2016-07-21','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22599','title' => 'Richter\'s Market Savings Coupon','blast_date' => '2016-07-21','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-18 16:24:11','updated_at' => '2016-07-18 16:24:11'),
          array('id' => '22594','title' => 'Shurfine Ice Cream &#8226; 56 oz','blast_date' => '2016-07-25','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '22602','title' => 'Betty Crocker Suddenly Pasta Salad &#8226; 5.9-8.3 oz','blast_date' => '2016-07-25','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-22 12:00:01','updated_at' => '2016-07-23 02:26:48'),
          array('id' => '22601','title' => 'Sweet Baby Ray\'s BBQ Sauce &#8226; Original Only &#8226; 18 oz','blast_date' => '2016-07-28','eblast_coupon_template_id' => '500','slug' => '','email_subject' => '','email_plain_text' => '','blast_template_values' => '','request_id' => '0','created_at' => '2016-07-20 10:00:01','updated_at' => '2016-07-20 10:00:01')
        );

        $eblast_coupon_coupons = array(
          array('id' => '3172','eblast_coupon_id' => '22583','coupon_id' => '8409','display_start_date' => '2016-07-04','display_end_date' => '2016-07-05','offer_start_date' => '2016-07-04','offer_end_date' => '2016-07-05','created_at' => '2016-07-14 11:11:06','updated_at' => '2016-07-14 11:11:06'),
          array('id' => '3173','eblast_coupon_id' => '22585','coupon_id' => '8410','display_start_date' => '2016-07-11','display_end_date' => '2016-07-12','offer_start_date' => '2016-07-11','offer_end_date' => '2016-07-12','created_at' => '2016-07-14 11:11:06','updated_at' => '2016-07-14 11:11:06'),
          array('id' => '3174','eblast_coupon_id' => '22586','coupon_id' => '8411','display_start_date' => '2016-07-04','display_end_date' => '2016-07-10','offer_start_date' => '2016-07-04','offer_end_date' => '2016-07-10','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3175','eblast_coupon_id' => '22587','coupon_id' => '8412','display_start_date' => '2016-07-18','display_end_date' => '2016-07-18','offer_start_date' => '2016-07-18','offer_end_date' => '2016-07-18','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3176','eblast_coupon_id' => '22588','coupon_id' => '8413','display_start_date' => '2016-07-06','display_end_date' => '2016-07-08','offer_start_date' => '2016-07-06','offer_end_date' => '2016-07-08','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3178','eblast_coupon_id' => '22590','coupon_id' => '8415','display_start_date' => '2016-07-07','display_end_date' => '2016-07-13','offer_start_date' => '2016-07-07','offer_end_date' => '2016-07-13','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3179','eblast_coupon_id' => '22591','coupon_id' => '8416','display_start_date' => '2016-07-11','display_end_date' => '2016-07-17','offer_start_date' => '2016-07-11','offer_end_date' => '2016-07-17','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3180','eblast_coupon_id' => '22592','coupon_id' => '8417','display_start_date' => '2016-07-16','display_end_date' => '2016-07-19','offer_start_date' => '2016-07-16','offer_end_date' => '2016-07-19','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3181','eblast_coupon_id' => '22593','coupon_id' => '8418','display_start_date' => '2016-07-14','display_end_date' => '2016-07-20','offer_start_date' => '2016-07-14','offer_end_date' => '2016-07-20','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3182','eblast_coupon_id' => '22594','coupon_id' => '8419','display_start_date' => '2016-07-25','display_end_date' => '2016-07-25','offer_start_date' => '2016-07-25','offer_end_date' => '2016-07-25','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3184','eblast_coupon_id' => '22596','coupon_id' => '8421','display_start_date' => '2016-07-16','display_end_date' => '2016-07-19','offer_start_date' => '2016-07-16','offer_end_date' => '2016-07-19','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3185','eblast_coupon_id' => '22597','coupon_id' => '8422','display_start_date' => '2016-07-18','display_end_date' => '2016-07-24','offer_start_date' => '2016-07-18','offer_end_date' => '2016-07-24','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3186','eblast_coupon_id' => '22598','coupon_id' => '8423','display_start_date' => '2016-07-21','display_end_date' => '2016-07-24','offer_start_date' => '2016-07-21','offer_end_date' => '2016-07-24','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '3187','eblast_coupon_id' => '22599','coupon_id' => '8424','display_start_date' => '2016-07-21','display_end_date' => '2016-07-27','offer_start_date' => '2016-07-21','offer_end_date' => '2016-07-27','created_at' => '2016-07-18 16:24:11','updated_at' => '2016-07-18 16:24:11'),
          array('id' => '3189','eblast_coupon_id' => '22601','coupon_id' => '8471','display_start_date' => '2016-07-28','display_end_date' => '2016-08-03','offer_start_date' => '2016-07-28','offer_end_date' => '2016-08-03','created_at' => '2016-07-20 10:00:01','updated_at' => '2016-07-20 10:00:01'),
          array('id' => '3190','eblast_coupon_id' => '22602','coupon_id' => '8475','display_start_date' => '2016-07-25','display_end_date' => '2016-07-31','offer_start_date' => '2016-07-25','offer_end_date' => '2016-07-31','created_at' => '2016-07-22 12:00:01','updated_at' => '2016-07-22 12:00:01')
        );

        $eblast_coupon_locations_groups = array(
          array('eblast_coupon_id' => '22583','location_id' => '20312','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20310','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20309','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20315','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20305','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20320','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20307','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20304','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20302','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20340','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20319','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20303','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20469','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '20510','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '21055','group_id' => NULL),
          array('eblast_coupon_id' => '22583','location_id' => '21322','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20312','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20310','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20309','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20315','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20305','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20320','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20307','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20304','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20302','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20340','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20319','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20303','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20469','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '20510','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '21055','group_id' => NULL),
          array('eblast_coupon_id' => '22585','location_id' => '21322','group_id' => NULL),
          array('eblast_coupon_id' => '22586','location_id' => '20245','group_id' => NULL),
          array('eblast_coupon_id' => '22586','location_id' => '21302','group_id' => NULL),
          array('eblast_coupon_id' => '22586','location_id' => '20413','group_id' => NULL),
          array('eblast_coupon_id' => '22586','location_id' => '20247','group_id' => NULL),
          array('eblast_coupon_id' => '22586','location_id' => '20248','group_id' => NULL),
          array('eblast_coupon_id' => '22586','location_id' => '20466','group_id' => NULL),
          array('eblast_coupon_id' => '22586','location_id' => '20249','group_id' => NULL),
          array('eblast_coupon_id' => '22586','location_id' => '20382','group_id' => NULL),
          array('eblast_coupon_id' => '22586','location_id' => '20258','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20312','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20310','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20309','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20315','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20305','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20320','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20307','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20304','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20302','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20340','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20319','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20303','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20469','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '20510','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '21055','group_id' => NULL),
          array('eblast_coupon_id' => '22587','location_id' => '21322','group_id' => NULL),
          array('eblast_coupon_id' => '22588','location_id' => '20190','group_id' => NULL),
          array('eblast_coupon_id' => '22588','location_id' => '20191','group_id' => NULL),
          array('eblast_coupon_id' => '22588','location_id' => '20192','group_id' => NULL),
          array('eblast_coupon_id' => '22588','location_id' => '20427','group_id' => NULL),
          array('eblast_coupon_id' => '22588','location_id' => '20026','group_id' => NULL),
          array('eblast_coupon_id' => '22588','location_id' => '20468','group_id' => NULL),
          array('eblast_coupon_id' => '22588','location_id' => '21161','group_id' => NULL),
          array('eblast_coupon_id' => '22590','location_id' => '21145','group_id' => NULL),
          array('eblast_coupon_id' => '22590','location_id' => '21144','group_id' => NULL),
          array('eblast_coupon_id' => '22591','location_id' => '20245','group_id' => NULL),
          array('eblast_coupon_id' => '22591','location_id' => '21302','group_id' => NULL),
          array('eblast_coupon_id' => '22591','location_id' => '20413','group_id' => NULL),
          array('eblast_coupon_id' => '22591','location_id' => '20247','group_id' => NULL),
          array('eblast_coupon_id' => '22591','location_id' => '20248','group_id' => NULL),
          array('eblast_coupon_id' => '22591','location_id' => '20466','group_id' => NULL),
          array('eblast_coupon_id' => '22591','location_id' => '20249','group_id' => NULL),
          array('eblast_coupon_id' => '22591','location_id' => '20382','group_id' => NULL),
          array('eblast_coupon_id' => '22591','location_id' => '20258','group_id' => NULL),
          array('eblast_coupon_id' => '22592','location_id' => '20190','group_id' => NULL),
          array('eblast_coupon_id' => '22592','location_id' => '20191','group_id' => NULL),
          array('eblast_coupon_id' => '22592','location_id' => '20192','group_id' => NULL),
          array('eblast_coupon_id' => '22592','location_id' => '20427','group_id' => NULL),
          array('eblast_coupon_id' => '22592','location_id' => '20026','group_id' => NULL),
          array('eblast_coupon_id' => '22592','location_id' => '20468','group_id' => NULL),
          array('eblast_coupon_id' => '22592','location_id' => '21161','group_id' => NULL),
          array('eblast_coupon_id' => '22593','location_id' => '21145','group_id' => NULL),
          array('eblast_coupon_id' => '22593','location_id' => '21144','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20312','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20310','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20309','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20315','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20305','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20320','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20307','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20304','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20302','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20340','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20319','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20303','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20469','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '20510','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '21055','group_id' => NULL),
          array('eblast_coupon_id' => '22594','location_id' => '21322','group_id' => NULL),
          array('eblast_coupon_id' => '22596','location_id' => '20015','group_id' => NULL),
          array('eblast_coupon_id' => '22596','location_id' => '20025','group_id' => NULL),
          array('eblast_coupon_id' => '22596','location_id' => '20010','group_id' => NULL),
          array('eblast_coupon_id' => '22596','location_id' => '20027','group_id' => NULL),
          array('eblast_coupon_id' => '22596','location_id' => '20011','group_id' => NULL),
          array('eblast_coupon_id' => '22596','location_id' => '20364','group_id' => NULL),
          array('eblast_coupon_id' => '22597','location_id' => '20245','group_id' => NULL),
          array('eblast_coupon_id' => '22597','location_id' => '21302','group_id' => NULL),
          array('eblast_coupon_id' => '22597','location_id' => '20413','group_id' => NULL),
          array('eblast_coupon_id' => '22597','location_id' => '20247','group_id' => NULL),
          array('eblast_coupon_id' => '22597','location_id' => '20248','group_id' => NULL),
          array('eblast_coupon_id' => '22597','location_id' => '20466','group_id' => NULL),
          array('eblast_coupon_id' => '22597','location_id' => '20249','group_id' => NULL),
          array('eblast_coupon_id' => '22597','location_id' => '20382','group_id' => NULL),
          array('eblast_coupon_id' => '22597','location_id' => '20258','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20015','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20025','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20190','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20010','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20027','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20191','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20192','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20011','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20427','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20026','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20468','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '20364','group_id' => NULL),
          array('eblast_coupon_id' => '22598','location_id' => '21161','group_id' => NULL),
          array('eblast_coupon_id' => '22599','location_id' => '21145','group_id' => NULL),
          array('eblast_coupon_id' => '22599','location_id' => '21144','group_id' => NULL),
          array('eblast_coupon_id' => '22601','location_id' => '21145','group_id' => NULL),
          array('eblast_coupon_id' => '22601','location_id' => '21144','group_id' => NULL),
          array('eblast_coupon_id' => '22602','location_id' => '20245','group_id' => NULL),
          array('eblast_coupon_id' => '22602','location_id' => '21302','group_id' => NULL),
          array('eblast_coupon_id' => '22602','location_id' => '20413','group_id' => NULL),
          array('eblast_coupon_id' => '22602','location_id' => '20247','group_id' => NULL),
          array('eblast_coupon_id' => '22602','location_id' => '20248','group_id' => NULL),
          array('eblast_coupon_id' => '22602','location_id' => '20466','group_id' => NULL),
          array('eblast_coupon_id' => '22602','location_id' => '20249','group_id' => NULL),
          array('eblast_coupon_id' => '22602','location_id' => '20382','group_id' => NULL),
          array('eblast_coupon_id' => '22602','location_id' => '20258','group_id' => NULL)
        );

        $coupons = array(
          array('id' => '8409','title' => 'Shurfine Potato or Kettle Chips &#8226; Assorted Flavors &#8226; 8.5-10oz','price' => 'FREE with $20.00 Additional Purchase','description' => '2 days ONLY! Monday July 4th & July 5th ','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"20.00","valid_storename":true,"valid_dates":true,"must_show_printed":true}','plu' => 'PLU 5878','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rPotatoChipsKettleMesqBBQ8.5oz_03172_1466788506.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rchipsanddip-53155777-2507px-3392px_1466788506.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:06','updated_at' => '2016-07-14 11:11:06'),
          array('id' => '8410','title' => 'Shurfine Spreadable Butter &#8226; 15 oz','price' => 'FREE with $20.00 Additional Purchase','description' => '2 days ONLY! July 11th & July 12th','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"20.00","valid_storename":true,"valid_dates":true,"must_show_printed":true}','plu' => 'PLU 5879','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rButterSpreadCanolaOil15oz_03323_1466789400.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rDAIRY-breadbutter-105783608-3168px-3168px_1466789400.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:06','updated_at' => '2016-07-14 11:11:06'),
          array('id' => '8411','title' => 'Fresh Avocado','price' => 'FREE with $20.00 Additional Purchase','description' => '','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"20.00","valid_storename":true,"valid_dates":true,"must_show_printed":true}','plu' => 'PLU 2045','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rVEGETABLE-avocados-ss3158489-4000px-6000px_1467128434.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rMEXICAN-guacamole&chips-103153580-4288px-2848px_1467128438.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8412','title' => 'At Ease Mini Sandwiches &#8226; 8.6 - 10.4 oz','price' => 'FREE with $25.00 Additional Purchase','description' => 'Monday, July 18th ONLY!','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"25.00","valid_storename":true,"valid_dates":true,"must_show_printed":true}','plu' => 'PLU 5775','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rAtEaseBrkfstSandEngMuffHamEggChs2ct8.4oz_41548_1467131902.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rsausageeggcheesebreakfastsandwich-91396709-2592px-3888px_1467131902.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8413','title' => 'Shurfine Cottage Cheese &#8226; 24 oz','price' => '.99&#162; with $15.00 Additional Purchase','description' => 'Advertised Price: $1.68','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"15.00","valid_storename":true,"valid_dates":true,"must_show_printed":true,"must_show_mobile":true}','plu' => 'PLU 9020','redemption_value' => 'RV .69','product_image_directory' => '00-02','product_image_filename' => 'rCottageCheese2Prcnt24oz_46228_1467132091.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rLadyShopping_126299807_1467132092.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8415','title' => 'French Onion Dip &#8226; Selected Varieties &#8226; 16 oz','price' => 'FREE with $10.00 Additional Purchase','description' => '','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"10.00","valid_storename":true,"valid_dates":true,"must_show_printed":true,"must_show_mobile":true,"additional":""}','plu' => 'PLU 840','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rDip-FrenchOnionBTS1_1467323571.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rDip-FrenchOnionBTS2_1467323571.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8416','title' => 'Green Giant Carrots &#8226; Baby Cut &#8226; 1 lb','price' => 'FREE with $20.00 Additional Purchase','description' => '','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"20.00","valid_storename":true,"valid_dates":true,"must_show_printed":true}','plu' => 'PLU 2045','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rGGBabyCarrots16oz_12404_1467325865.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rVEGETABLE-brocoli-and-carrots-98088593-3284px-2132px_1467325866.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8417','title' => 'Lofthouse Cookies &#8226; 10-12 ct','price' => '.99&#162; with $20.00 Additional Purchase','description' => 'Advertised Price 2 for $6.00','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"20.00","valid_storename":true,"valid_dates":true,"must_show_printed":true,"must_show_mobile":true}','plu' => 'PLU 9021','redemption_value' => 'RV 201','product_image_directory' => '00-02','product_image_filename' => 'rLHCookSC-PBButterfinger10.6oz_91327_1467835492.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rcookieschocolatechip106992713-4256px-2832px_1467835492.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8418','title' => 'Shurfine Reusable Bag','price' => 'FREE with $10.00 Additional Purchase','description' => '','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"10.00","valid_storename":true,"valid_dates":true,"must_show_printed":true,"must_show_mobile":true}','plu' => 'PLU 840','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rBagGroceryBYOB_1467999545.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rGROCERY-totegrocerybag-128348732-3744px-5616px_1467999545.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8419','title' => 'Shurfine Ice Cream &#8226; 56 oz','price' => 'FREE with $25.00 Additional Purchase','description' => 'Monday July 25th ONLY!','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"25.00","valid_storename":true,"valid_dates":true,"must_show_printed":true}','plu' => 'PLU 5776','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rIceCrm56ozVanilla_45101_1468000769.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rice-cream-cone-shutterstock-113614531-3096px-4687px_1468000769.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8421','title' => 'Lofthouse Cookies &#8226; 10-12 ct','price' => '.99&#162; with $20.00 Additional Purchase','description' => 'Advertised Price 2 for $6.00','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"20.00","valid_storename":true,"valid_dates":true,"must_show_printed":true,"must_show_mobile":true}','plu' => 'PLU 9021','redemption_value' => 'RV 201','product_image_directory' => '00-02','product_image_filename' => 'rrLHCookSC-PBButterfinger10.6oz_91327_1467835492_1468275519.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rrcookieschocolatechip106992713-4256px-2832px_1467835492_1468275519.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8422','title' => 'Mr. Dell\'s Hashbrowns &#8226; 24-32 oz','price' => 'FREE with $20.00 Additional Purchase','description' => '','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"20.00","valid_storename":true,"valid_dates":true,"must_show_printed":true}','plu' => 'PLU 2047','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rMrDellsHashBrownsAmer28oz_1468422783.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rbreakfastbeautybaconeggshashbrowns-66670375-2848px-4272px_1468422783.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8423','title' => '@Ease Skillet Meals &#8226; 21 oz','price' => '$1.99 with $15.00 Additional Purchase','description' => 'Advertised Price: $3.29','disclaimer' => '{"qty_limit":"2","coupons_limit":1,"additional_purchase":"15.00","valid_storename":true,"valid_dates":true,"must_show_printed":true,"must_show_mobile":true}','plu' => 'PLU 9022','redemption_value' => 'RV 2.60','product_image_directory' => '00-02','product_image_filename' => 'rAtEaseChknAlfredo21oz_42073_1468426681.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'ralfredopasta-85029172-5000px-3333px_1468426681.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-14 11:11:07','updated_at' => '2016-07-14 11:11:07'),
          array('id' => '8424','title' => 'Richter\'s Market Savings Coupon','price' => 'Save $5.00 Off Any Purchase of $35.00 with Coupon','description' => '','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"35.00","valid_storename":true,"valid_dates":true,"must_show_printed":true,"must_show_mobile":true}','plu' => 'PLU 840','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rGROCERY-grocerybag-128348732-3744px-5616px_1468594819.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rLadyShopping_126299807_1468594822.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-18 16:24:11','updated_at' => '2016-07-18 16:24:11'),
          array('id' => '8471','title' => 'Sweet Baby Ray\'s BBQ Sauce &#8226; Original Only &#8226; 18 oz','price' => 'FREE with $10.00 Additional Purchase','description' => '','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"10.00","valid_storename":true,"valid_dates":true,"must_show_printed":true,"must_show_mobile":true}','plu' => 'PLU 840','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rSBRBBQSc-18ozOrig_91810_1469024942.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rBEEF-ribs-bbq-beef-ss31259767-2600px-1646px_1469024942.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-20 10:00:01','updated_at' => '2016-07-20 10:00:01'),
          array('id' => '8475','title' => 'Betty Crocker Suddenly Pasta Salad &#8226; 5.9 - 8.3 oz','price' => 'FREE with $20.00 Additional Purchase','description' => '','disclaimer' => '{"qty_limit":"1","coupons_limit":1,"additional_purchase":"20.00","valid_storename":true,"valid_dates":true,"must_show_printed":true}','plu' => 'PLU 2048','redemption_value' => '','product_image_directory' => '00-02','product_image_filename' => 'rBCSuddenPSClassic7.75oz_50330_1469201116.jpg','beauty_image_directory' => '00-02','beauty_image_filename' => 'rchicken-pasta-90505585-3744px-3744px_1469201116.jpg','upc_image_directory' => '00-02','upc_image_filename' => '','created_at' => '2016-07-22 12:00:01','updated_at' => '2016-07-22 12:00:01')
        );

        foreach($eblast_coupons as $eblast_coupon) {
            DB::table('eblast_coupons')->insert($eblast_coupon);
        }

        foreach($eblast_coupon_coupons as $eblast_coupon_coupon) {
            DB::table('eblast_coupon_coupons')->insert($eblast_coupon_coupon);
        }

        foreach($eblast_coupon_locations_groups as $eblast_coupon_locations_group) {
            DB::table('eblast_coupon_locations_groups')->insert($eblast_coupon_locations_group);
        }

        foreach($coupons as $coupon) {
            DB::table('testing_site_content.coupons')->insert($coupon);
        }
    }

}
