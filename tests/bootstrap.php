<?php
// Here you can initialize variables that will be available to your tests

require __DIR__ . '/../bootstrap/autoload.php';
$app = require __DIR__ . '/../bootstrap/app.php';
$app->loadEnvironmentFrom('.env.testing');
$app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

$app['Illuminate\Contracts\Console\Kernel']->call('migrate:refresh', ['--env' => 'testing']);
$app['Illuminate\Contracts\Console\Kernel']->call('db:seed', ['--env' => 'testing', '--class' => 'TestDatabaseSeeder']);
