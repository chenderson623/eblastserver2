<table width="{{ $body_width }}" cellpadding="0" cellspacing="0" style="margin:30px auto 25px auto;width:{{ $body_width }}px;font-family:Arial,sans-serif;font-size:16px;text-align:left;color:{{ $default_color }};background-color:#fff">
    <tr>
        <td align="left" valign="middle" width="220" height="80">
            <a href="{{ $homepage_url }}&l=logo" style="text-decoration:none;">
                <img src="{{ $site_logo_url }}" {{ $logo_size_string }} style="border:0;max-width:220px;max-height:80px;" alt="{{ $site_loc_name }}" border="0">
            </a>
        </td>

        <td valign="middle" align="center" width="190" height="80">
            {{ $facebook_link_head_section }}
        </td>

        <td valign="middle" align="right" width="220" height="80">
            {{ $ftf_header }}
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center" valign="bottom" height="50">
            <div style="color:{{ $heading_color }};font-size:26px;padding-top:10px;letter-spacing:.05em;">
                <b><i>{{ $ad_display_name }} Specials</i></b>
            </div>
        </td>
    </tr>
    {{ $display_date_conditional }}
    <tr>
        <td width="<?php echo $body_width; ?>" colspan="3" align="center" valign="middle" style="padding:15px 0 0;line-height:18px;width:<?php echo $body_width; ?>px;color:{{ $default_color }};font-size:14px;">
            <i>As requested, here are our {{ $ad_display_name }} Specials.<br>Thanks for being our customer!</i>
        </td>
    </tr>
</table>
