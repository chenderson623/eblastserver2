<?php namespace App\Libraries\BlastContent\Ads\Templates;

use App\Libraries\BlastContent\AbstractBlastTemplateSource;

/**
 * This class should have the basic data to build content
 * Could also be used to do some basic preprocessing
 */
class WeeklyAdTemplateSource_Default extends AbstractBlastTemplateSource {

    protected $template_var_defs = array(

        'ad_dir_name'                 => array('substitute_empty' => ''),
        'ad_display_name'             => array('substitute_empty' => ''),

        'web_slug'                    => array('substitute_empty' => false),
        'image_dir'                   => array('substitute_empty' => false),
        'email_image_url'             => array('substitute_empty' => false),
        'web_image_url'               => array('substitute_empty' => "http://--|site_url|--/--|blast_type|--/--|blast_id|--/--|image_dir|--/web_image.jpg"),
    );

    public function __construct() {
        $this->template_var_defs = array_merge(parent::$default_template_var_defs, $this->template_var_defs);

        parent::__construct();

        $this->template_var_defs['display_date_conditional'] = array('substitute_empty' => '', 'substitute_value' => $this->getDateRangeSection(),'empty_check' => 'show_dates');
    }

    public function getEmailSubjectSource() {
        return '--|ad_display_name|-- Specials for --|site_loc_name|--';
    }

    public function getEmailBodyPlainTextSource() {
        return <<<TOV

As requested, here are our --|ad_display_name|-- Specials:
--|blast_url|--&l=pt

Thanks for being our customer!

Prices Effective --|offer_start_date_formatted|-- - --|offer_end_date_formatted|--

Visit our website: --|homepage_url|--&l-pt

--|site_loc_name|--
--|site_address_print_one_line|--

Unsubscribe from this mailing list: --|unsubscribe_url|--&l=pt

TOV;
    }

    protected function getDateRangeSection() {
        ob_start();
        ?>
        <tr>
            <td colspan="3" align="center" valign="middle" height="20" style="font-weight:bold;color:--|link_color|--;font-size:14px;">Prices valid --|offer_start_date_formatted|-- - --|offer_end_date_formatted|--</td>
        </tr>
        <?php
        return ob_get_clean();
    }

    public function getWebHtmlSource() {
        //Not really used. return same as email:
        return $this->getEmailBodyHtmlSource();
    }

    public function getEmailBodyHtmlSource() {
        $body_width  = 630;
        $image_width = 628;
        ob_start();
        ?>
        <html>
            <body>
<?php /*                <table width="<?php echo $body_width; ?>" cellpadding="0" cellspacing="0" style="margin:30px auto 25px auto;width:<?php echo $body_width; ?>px;font-family:Arial,sans-serif;font-size:16px;text-align:left;color:--|default_color|--;background-color:#fff">
                    <tr>
                        <td align="left" valign="middle" width="220" height="80">
                            <a href="--|homepage_url|--&l=logo" style="text-decoration:none;">
                                <img src="--|site_logo_url|--" --|logo_size_string|-- style="border:0;max-width:220px;max-height:80px;" alt="--|site_loc_name|--" border="0">
                            </a>
                        </td>

                        <td valign="middle" align="center" width="190" height="80">
                            --|facebook_link_head_section|--
                        </td>

                        <td valign="middle" align="right" width="220" height="80">
                            --|ftf_header|--
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" valign="bottom" height="50">
                            <div style="color:--|heading_color|--;font-size:26px;padding-top:10px;letter-spacing:.05em;">
                                <b><i>--|ad_display_name|-- Specials</i></b>
                            </div>
                        </td>
                    </tr>
                    --|display_date_conditional|--
                    <tr>
                        <td width="<?php echo $body_width; ?>" colspan="3" align="center" valign="middle" style="padding:15px 0 0;line-height:18px;width:<?php echo $body_width; ?>px;color:--|default_color|--;font-size:14px;">
                            <i>As requested, here are our --|ad_display_name|-- Specials.<br>Thanks for being our customer!</i>
                        </td>
                    </tr>
                </table>  */ ?>
                <table id="Table_01" width="<?php echo $body_width; ?>" border="0" cellpadding="0" cellspacing="0" align="center" style="font-family:Arial, Helvetica, sans-serif;">
                    <tr>
                        <td width="<?php echo $body_width; ?>" align="center" style="border:1px solid #999">
                            <a href="--|blast_url|--">
                                <img src="--|email_image_url|--" width="<?php echo $image_width; ?>" style="width:<?php echo $image_width; ?>px;height:auto;display:block;border:none;" alt="--|site_loc_name|-- --|ad_display_name|-- Specials" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td width="<?php echo $body_width; ?>" height="15" style="height:15px" bgcolor="#fff" align="center" >
                            <img src="--|assets_url|--/dropShadow.jpg" alt="">
                        </td>
                    </tr>
                    <tr>
                        <td width="<?php echo $body_width; ?>" bgcolor="#fff" align="center">
                            <a href="http://--|site_url|--/ads/index.php?mid=-track_code-" style="color:--|heading_color|--;text-decoration:none;font-size:28px;display:block;padding-top:10px;background-color:#fff">View All Ad Specials Online</a>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="<?php echo $body_width; ?>" cellpadding="0" cellspacing="0" style="margin:0 auto 20px;font-family:Arial,sans-serif;font-size:16px;text-align:center;color:--|default_color|--">
                    <tbody>
                        <tr>
                            <td>
                                <a style="color:--|link_color|--" href="--|homepage_url|--&l=bottom_link">Visit Our Website</a>
                                |
                                --|facebook_link_section|--
                                <a style="color:--|link_color|--" href="--|blast_url|--&l=bottom_ftf_link">
                                    <img src="--|assets_url|--/Envelope.png" height="18px;" /> Forward to a Friend
                                </a>
                                |
                                <a style="color:--|link_color|--" href="--|unsubscribe_url|--&l=bottom_link">Unsubscribe</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="margin:0 auto 20px;padding-left:30px;padding-top:20px;padding-bottom:30px;font-family:Arial,sans-serif;font-size:16px;color:#727272" valign="middle" align="center">
                                <strong>--|site_loc_name|--</strong><br />
                                --|site_address_print_one_line|-- <br />
                                --|site_phone_print|--
                            </td>
                        </tr>

                    </tbody>
                </table>
            </body>
        </html>
        <?php
        return ob_get_clean();
    }

}
