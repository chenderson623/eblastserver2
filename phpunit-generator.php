<?php

require_once "tests/bootstrap.php";

use PhpUnitTestGenerator\Application;

$configuration = \PhpUnitTestGenerator\Configuration\Configuration::getInstance();
//$configuration->setBaseClass("");

$configuration->setSourceDirectory(dirname(__FILE__) . '/EblastServer/Subscribers/Models/Eloquent');
$configuration->setTargetDirectory(dirname(__FILE__) . '/tests/Subscribers/Models/Eloquent/');

//$configuration->setSourceDirectory(dirname(__FILE__) . '/EblastServer/SubscribersTMP');
//$configuration->setTargetDirectory(dirname(__FILE__) . '/tests/SubscribersTMP/');

$configuration->setNamespaceMappings(array(
//    "Example" => "Example\\Tests",
));


$application = new Application();
$application->run();